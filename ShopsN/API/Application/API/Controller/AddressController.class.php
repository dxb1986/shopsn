<?php
namespace API\Controller;

use Common\Tool\Tool;
use API\Model\UserAddressModel;

class AddressController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        Tool::checkPost($_POST, (array)null, false, array('token')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        $this->isLogin();
    }
    
    //新增收货地址
    public function addAddressByUser()
    {
        Tool::checkPost($_POST, array(
                'is_numeric' => array('mobile', 'status', 'zipcode')
            ), true, array(
                'realname','prov','city', 'dist',
                'address', 
                'mobile', 
                'status','zipcode'
        )) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $insertId = UserAddressModel::getInitation()->add($_POST);
        
        $this->updateClient($insertId, '增加地址', true);
    }
    
    //修改地址
    public function editAddressByUser()
    {
        Tool::checkPost($_POST, array(
            'is_numeric' => array('mobile', 'status', 'id', 'zipcode')
        ), true, array(
            'realname','id','prov','city', 'dist',
            'address',
            'mobile',
            'status', 'zipcode'
        )) ? true : $this->ajaxReturnData(null, '400', '参数错误或参数为空，漏传参数');
        
        $isHaveDefault = UserAddressModel::getInitation()->getDefaultByUser();
        
        $this->alreadyInData($isHaveDefault, '已存在默认地址');
        
        $insertId = UserAddressModel::getInitation()->save($_POST);
    
        $this->updateClient($insertId, '编辑地址', true);
    }
    /**
     * 显示 列表
     */
    public function showAddressList()
    {
        $data = UserAddressModel::getInitation()->getAddressList();
        
        $this->updateClient($data, '操作');
    }
   
}