<?php
namespace API\Controller;
use Think\Controller;
use API\Model\ConfigChildrenModel;
use API\Model\SystemConfigModel;
use Common\Tool\Tool;

/**
 * 前台控制器基类 
 */
class BaseController extends Controller
{
    /**
     * 读取导航(商品大分类) 
     */
    
    public function _initialize()
    {
        //设置默认值
        Tool::isSetDefaultValue($_GET, array('page' => 1, 'page_size' => 10) );
        
        Tool::isSetDefaultValue($_POST, array('page' => 1, 'page_size' => 10) );
        //设置session保存时间
        ini_set('session.gc_maxlifetime',86400*64); 
        Tool::connect('Session');
        Tool::setSession();
    }
    /**
     * ajax 返回数据
     */
    protected function ajaxReturnData($data, $status= '200', $message = '操作成功')
    {
        if (!empty($data) && !is_numeric($data))
        {
            //判断数组的维度
            Tool::connect('Mosaic');
            $wd = Tool::array_depth($data);
            $wd === 1 ? $data = array($data) : false;
        }
        $this->ajaxReturn(array(
            'code'  => $status,
            'message' => $message,
            'data'    => empty($data) ? null : $data
        ));
        die();
    }
    /**
     * 获取系统配置
     */
    public function getConfig($key = null)
    {
        if (!S('config'))
        {
            //获取字表数据
            $children    = ConfigChildrenModel::getInitnation()->getAllConfig();
            //获取配置值
            $configValue = SystemConfigModel::getInitnation()->getAllConfig();
            //组合数据
            Tool::connect('ArrayParse', array('children' => $children, 'configValue'=> $configValue));
            $receive = array();
            $data = Tool::buildConfig()->parseConfig()->oneArray($receive);
            S('config', $receive, 100);
        }
        $arrayData = S('config');
        return $key === null ? $arrayData : $arrayData[$key];
    }
    
    protected function updateClient($insert_id, $messagePrfix = '更新', $isNull = FALSE)
    {
        $status    = empty($insert_id) ? '400' : '200';
        $message   = empty($insert_id) ? '空数据' : $messagePrfix.'成功';
        $insert_id = $isNull === true ? null : $insert_id;
        $this->ajaxReturnData($insert_id, $status, $message);
    }
    
    protected function alreadyInData($data, $message= '更新成功')
    {
        if (!empty($data)) {
            $this->ajaxReturnData(null, '400', $message);
        }
        return true;
    }
    
    protected function isLogin()
    {
        if(empty($_SESSION['userId']) ) {
            $this->ajaxReturnData(null, '400', '请登录或者登陆已过期请重新登陆');
        }
    }
    
    /**
     * 提示client
     * @param array   $data     要检测的数据
     * @param string  $checkKey 要检测的键
     * @param string  $message  信息
     */
    protected function prompt( $data, $checkKey, $message , $isValidate = true)
    {
        if (empty($data)) {
            $this->ajaxReturnData(null, '400', $message);
        } elseif(is_array($data) && empty($data[$checkKey]) && $isValidate ) {
            $this->ajaxReturnData(null, '400', $message);
        } 
        return true;
    }
    /**
     * 
     */
}