<?php
namespace API\Controller;

use API\Model\UserModel;
use API\Model\MemberModel;
use API\Model\UserAddressModel;
use Common\Tool\Tool;
use API\Model\BankCardModel;
use API\Model\UserHeaderModel;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        Tool::checkPost($_POST, (array)null, false, array('token')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        //打开
        $this->isLogin();
    }
   
    //登陆后的个人中心
    public function index()
    {
         $user = UserModel::getInistnation()->getUserInfoById();
         //获取附表信息
         $other = MemberModel::getInitnation()->getUserInfoById();
         
         $this->updateClient(array_merge($user, $other), '操作');
    }
    
    //个人信息
    public function userInfo()
    {
       
       $user =  UserModel::getInistnation()->getUserInfoById('account_balance,realname,idcard,sex,birthday,mobile', 'find');
       $this->prompt($user, null, '没有该用户', false);
       
       $address = UserAddressModel::getInitation()->getAddressList();
       // 获取头像
       $header = UserHeaderModel::getInitation()->getHeader($_SESSION['userId']);
       
       $user['user_header'] = !empty($header) ? $header['user_header'] : null;
       
       $array   = array(
           'user'    => array($user),
           'address' => empty($address) ? null : $address,
       );
       
       $this->updateClient($array, '操作');
    }
    

    //修改姓名
    public function changeName()
    {
        Tool::checkPost($_POST, (array)null, false, array('realname')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $status = UserModel::getInistnation()->save($_POST, array(
            'where' => array('id' => $_SESSION['userId'])
        ));
        $this->updateClient($status, '更新', true);
    }
    
    
    //修改身份证号
    public function  changeIdCard()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('idcard')), false, array('idcard')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        //添加验证
        Tool::connect('ParttenTool')->addPartten('idCard', '/^(\d{18,18}|\d{15,15}|\d{17,17}x)$/');
        
        $status = Tool::validateData($_POST['idcard']);
        
        $this->prompt($status,null, '身份证信息不正确');
        
        $status = UserModel::getInistnation()->save($_POST, array(
            'where' => array(
                'id' => $_SESSION['userId']
            )
        ));
        $this->updateClient($status, '更新', true);
    }
    
    
    
    //修改性别
    public function  changeSex()
    {
        Tool::checkPost($_POST, (array)null, false, array('sex')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $_POST['sex'] = $_POST['sex'] === 'm' ? '男' : '女' ;
        

        $status = UserModel::getInistnation()->save($_POST, array(
            'where' => array(
                'id' => $_SESSION['userId']
            )
        ));
        $this->updateClient($status, '更新', true);
        
    }
    
    //修改生日
    public function changeBirthday()
    {
        Tool::checkPost($_POST, (array)null, false, array('birthday')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $status = UserModel::getInistnation()->save($_POST, array(
            'where' => array(
                'id' => $_SESSION['userId']
            )
        ));
        $this->updateClient($status, '更新', true);
        
    }
    //修改手机号
    public function change_phone()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('mobile')), false, array('mobile')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        
        $status = UserModel::getInistnation()->save($_POST, array(
            'where' => array(
                'id' => $_SESSION['userId']
            )
        ));
        
        $this->prompt($status,null, '修改失败');
        $status = MemberModel::getInitnation()->save($_POST, array(
            'where' => array(
                'id' => $_SESSION['userId']
            )
        ));
        $this->updateClient($status, '更新', true);
    }    
    //修改密码
    public function  changePassword()
    {
        Tool::checkPost($_POST, (array)null, false, array('password', 'newpassword', 'newpasswordtwo')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        
        if ($_POST['newpassword'] !== $_POST['newpassword'])
        {
            $this->prompt(null, null, '两次密码不一致');
        }
       
        //先查询原密码是否正确
        $passwod = UserModel::getInistnation()->getUserInfoById('password');
        if (md5($_POST['password']) !== $passwod['password'])
        {
            $this->prompt(null, null, '与原密码不一致');
        }
        
        //修改数据库
        $_POST['password'] = md5($_POST['newpassword']);
        $status = UserModel::getInistnation()->save($_POST, array(
            'where' => array('id' => $_SESSION['userId'])
        ));
        
        $this->updateClient($status, '更新', true);
    }    
    
    
    //账户管理
    public function  personManage()
    {
        //获取账户个数
        $userCount = BankCardModel::getInitation()->count();
       
        $userInfo = UserModel::getInistnation()->getUserInfoById('account_balance,integral,add_jf_currency,add_jf_limit');
      
        $userInfo['num'] = $userCount;
        $this->updateClient($userInfo, '操作');
    
    }
    
    // 银行卡管理
    public function  personBankcard()
    {
        $data = BankCardModel::getInitation()->getUserInfoById();
        $this->updateClient($data, '操作');
    }
    
    // 添加新银行卡
    public function bankcardAdd()
    {
        Tool::checkPost($_POST, array('is_numeric' => array(
             'card_num', 'mobile'
        )), false, array('realname', 'id_card', 'type', 'card_num', 'mobile','belong')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        //验证手机号码【明天加上】
//         Tool::connect('ParttenTool');
        
//         Tool::
        $status = BankCardModel::getInitation()->add($_POST);
        
        $this->updateClient($status, '添加', true);
    }
    
    // 银行卡解绑
     public function delBankcard()
     {
         Tool::checkPost($_POST, array('is_numeric' => array(
             'id'
         )), false, array('id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
         
         $status = BankCardModel::getInitation()->delete(array(
             'where' => array('id' => $_POST['id'])
         ));
        
         $this->updateClient($status, '删除', true);
     }
     
     /**
      * 上传头像 
      */
     public function uploadUserHeader()
     {
         Tool::checkPost($_FILES, (array)null, false, array('header')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
         
         Tool::connect('File');
         $_FILES = Tool::parseFile($_FILES);
         
         //转换一维数组
         Tool::connect('Mosaic');
         $userHeader = UserHeaderModel::getInitation();
         $filePath = $userHeader->UploadFile(C('GOODS_UPLOAD'));
         
         $status   = $userHeader->add(array('user_header' => $filePath));
         
         $this->updateClient($status, '添加', true);
     }
     
     public function uploadFileToServer()
     {
         Tool::checkPost($_FILES, (array)null, false, array('header')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
          
         Tool::connect('Mosaic');
         $file = Tool::array_depth($_FILES) ===2 ? Tool::parseToArray($_FILES) : $file;
         
         Tool::connect('CURL');
         
         $header = null;
         if( $header = UserHeaderModel::getInitation()->getHeader($_SESSION['userId']))
         {
             $header = $header['user_header'];
         }
         
         $response = Tool::uploadFile($file, C('IMAGE_UPLOAD_SERVER'), $_SESSION['userId'], $header);
         $response = json_decode($response,true);
         $status = false;
         if ($response['status'] == 1)
         {
             $status   = UserHeaderModel::getInitation()->updateOrAdd($response['data'], $_SESSION['userId']);
         }
         $this->updateClient($status, '添加');
     }
     
}