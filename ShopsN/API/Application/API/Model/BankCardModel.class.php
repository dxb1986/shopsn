<?php
namespace API\Model;

use Think\Model;

/**
 * 银行卡模型 
 */
class BankCardModel extends Model
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    /**
     * 获取银行卡信息
     */
    public function getUserInfoById($field = 'id,belong,type,card_num', $default = 'select')
    {
        return $this->field($field)->where('user_id = "%s"', $_SESSION['userId'])->$default();
    }
    
    protected function _before_insert(&$data, $options)
    {
        $data['create_time'] = time();
        $data['user_id']     = $_SESSION['userId'];
        return $data;
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::add()
     */
    
    public function add($data='', $options=array(), $replace=false)
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
    
        return parent::add($data, $options, $replace);
    }
}