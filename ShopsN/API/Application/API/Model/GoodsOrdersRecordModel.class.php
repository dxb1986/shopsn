<?php
namespace API\Model;

use Think\Model;
use Common\Tool\Tool;

/**
 * 商品评论模型
 * @author Administrator
 **/

class GoodsOrdersRecordModel extends Model
{
    
    private static $obj ;
    
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    public function select( array $options, Model $model)
    {        
        if (empty($options) || !($model instanceof Model))
        {
            return array();
        }
        
        $data = parent::select($options);
        
        if (!empty($data))
        {
           $userId = null;
           foreach ($data as $key => $value)
           {
               if (!empty($value['user_id']))
               {
                    $userId .= ','.'"'.$value['user_id'].'"';
               }
           }
           $userId = substr($userId, 1);
           $userData = $model->field('username,id as user_id')->where('id in ('.$userId.')')->select();
           
           $data = Tool::lineArrayData($data, $userData);
           
        }
        
        return $data;
    }
    
    protected function _before_insert(& $data, $options)
    {
        $data['pingjia_time'] = time();
        $data['status']      = 1;
        return $data;
    }
}