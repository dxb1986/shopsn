<?php
namespace API\Model;

use Think\Model;
use Common\Tool\Tool;

class OrderWxpayModel extends Model 
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    protected function _before_insert(&$data, $options)
    {
        $data['status']  = 0;
        return $data;
    }
    
    
    
    public function add($data='', $options=array(), $replace=false)
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::add($data, $options, $replace);
    }
}