<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//财务管理
class CaiwuController extends AuthController {
    //每日销售明细表
    public function xiaoshou_d(){

    	$m = M('goods_orders_record');

    	$begin_time = strtotime(date('Y-m-d'));
    	$end_time = strtotime(date('Y-m-d')." +1 day");

    	$where['create_time'] = array('between',"$begin_time,$end_time");
    	$where['pay_status'] = 1;

    	$result = $m->where($where)->select();
    	$m_2 = M('goods_orders');
    	foreach($result as $k=>$v){   		
    		$where_2['id'] = $v['goods_orders_id'];
    		$res = $m_2->where($where_2)->find();
    		$result[$k]['orders_num'] = $res['orders_num'];
    		$result[$k]['realname'] = $res['realname'];
    		$result[$k]['yunfei'] = $res['yunfei'];
    		$result[$k]['pay_time'] = $res['pay_time'];

    		$result[$k]['pay_sum'] = $v['price_new'] * $v['goods_num'];
    		if(empty($res['pay_time'])){
    			unset($result[$k]);
    		}
    	}

		$this->assign('result',$result);
		$this->display();
    }

    //每月销售明细表
    public function xiaoshou_m(){
    	$m = M('goods_orders_record');

    	$begin_time = strtotime(date("Y-m-01"));
    	$end_time = strtotime(date('Y-m-d')." +1 month");

    	$where['create_time'] = array('between',"$begin_time,$end_time");
    	$where['pay_status'] = 1;

    	$result = $m->where($where)->select();


    	$m_2 = M('goods_orders');
    	foreach ($result as $k => $v) {
    		$where_2['id'] = $v['goods_orders_id'];
    		$res = $m_2->where($where_2)->find();
    		$result[$k]['orders_num'] = $res['orders_num'];
    		$result[$k]['realname'] = $res['realname'];
    		$result[$k]['yunfei'] = $res['yunfei'];
    		$result[$k]['pay_time'] = $res['pay_time'];

            if(empty($v['fanli_action'])){
                $result[$k]['done_time'] = '-';
            }else{
                $result[$k]['done_time'] = substr($v['fanli_action'], -19);    
            }
            

    		$result[$k]['pay_sum'] = $v['price_new'] * $v['goods_num'];
    		if(empty($res['pay_time'])){
    			unset($result[$k]);
    		}

    	}

		$this->assign('result',$result);
		$this->display();
    }

    //按区间销售明细表
    public function xiaoshou_qj(){
    	$m = M('goods_orders_record');

    	if(!empty($_GET['begin_time']) && !empty($_GET['end_time'])){
    		$begin_time = strtotime($_GET['begin_time']);
    		$end_time = strtotime($_GET['end_time'])+86400;
    	}else{
    		$begin_time = time() - 86400*7;
    		$end_time = time();
    	}

    	$where['create_time'] = array('between',"$begin_time,$end_time");
    	$where['pay_status'] = 1;

    	$result = $m->where($where)->select();

    	$m_2 = M('goods_orders');
    	foreach ($result as $k => $v) {
    		$where_2['id'] = $v['goods_orders_id'];
    		$res = $m_2->where($where_2)->find();
    		$result[$k]['orders_num'] = $res['orders_num'];
    		$result[$k]['realname'] = $res['realname'];
    		$result[$k]['yunfei'] = $res['yunfei'];
    		$result[$k]['pay_time'] = $res['pay_time'];

            if(empty($v['fanli_action'])){
                $result[$k]['done_time'] = '-';
            }else{
                $result[$k]['done_time'] = substr($v['fanli_action'], -19);    
            }


    		$result[$k]['pay_sum'] = $v['price_new'] * $v['goods_num'];
    		if(empty($res['pay_time'])){
    			unset($result[$k]);
    		}
    	}

		$this->assign('result',$result);
		$this->display();
    }
	
	/**
     * 参与采购网
     */
    public function xiaoshou_cgw(){
		$incomeOrderModel=M('IncomeOrder');
		//搜索：
		$mobile = I('mobile');
		if(!empty($mobile)){
			$where = "o.status = 1 and u.mobile = '{$mobile}'";
		}else{
			$where = "o.status = 1";
		}
		
		
		$result = $incomeOrderModel->field('o.*,u.mobile,i.id as pid,i.name,i.type,i.price,i.standard,i.income')->alias('o')
			->join('left join db_user as u on o.user_id = u.id')
			->join('left join db_income as i on o.product_id = i.id')
			->where($where)
			->select();
		
		//处理数据
		//echo '<pre>'; var_dump($result);exit;
		foreach($result as &$item){
			//处理到期时间
            if($item['standard']==1){
				$item['standard'] = '半年期';
				$item['deadtime'] = date('Y-m-d H:i:s',$item['update_time']);
				$item['deadtime'] = strtotime($item['deadtime'] . '+6months');
				$item['deadtime'] = date('Y年m月d日',$item['deadtime']);
			}
            if($item['standard']==2){
				$item['standard'] = '一年期';
				$item['deadtime'] = date('Y-m-d H:i:s',$item['update_time']);
				$item['deadtime'] = strtotime($item['deadtime'] . '+1year');
				$item['deadtime'] = date('Y年m月d日',$item['deadtime']);
			}
			$item['update_time'] = date('Y年m月d日',$item['update_time']);//处理时间数据
            $item['income']= $item['price']*$item['product_num']-0+$item['product_income'];//到期收益+本金
        }
		
		$this->assign('result',$result);
        $this->display();
    }
	
	/**
     * 参与采购网的续期和关闭
     */
	public function cgw_edit(){
		$pid = I('get.pid');
		$type = I('get.type');
		$incomeOrderModel=M('IncomeOrder');
		$row = $incomeOrderModel->field('o.product_num,o.product_income,i.price,i.income')->alias('o')->join('left join db_income as i on o.product_id = i.id')->where("o.id = '{$pid}'")->find();
		if($type=='xq'){
			$income = $row['price']*$row['product_num']*$row['income'];
			$data['update_time'] = time();
			$data['product_income'] =$row['product_income']-0+$income;
		}else if($type == 'gb'){
			$data['status'] = 2;
		}
		$rst = $incomeOrderModel->where("id = '{$pid}'")->save($data);
		if($rst){
			$this->ajaxReturn('修改成功！');
		}else{
			$this->ajaxReturn('修改失败！');
		}
	}
	

    /**
	    * 导出数据为excel表格
	    *@param $data    一个二维数组,结构如同从数据库查出来的数组
	    *@param $title   excel的第一行标题,一个数组,如果为空则没有标题
	    *@param $filename 下载的文件名
	    *@examlpe 
	    $stu = M ('User');
	    $arr = $stu -> select();
	    exportexcel($arr,array('id','账户','密码','昵称'),'文件名!');
	*/
	function exportexcel($data=array(),$title=array(),$filename='report'){
	    header("Content-type:application/octet-stream");
	    header("Accept-Ranges:bytes");
	    header("Content-type:application/vnd.ms-excel");  
	    header("Content-Disposition:attachment;filename=".$filename.".xls");
	    header("Pragma: no-cache");
	    header("Expires: 0");
	    //导出xls 开始
	    if (!empty($title)){
	        foreach ($title as $k => $v) {
	            $title[$k]=iconv("UTF-8", "GB2312",$v);
	        }
	        $title= implode("\t", $title);
	        echo "$title\n";
	    }
	    if (!empty($data)){
	        foreach($data as $key=>$val){
	            foreach ($val as $ck => $cv) {
	                $data[$key][$ck]=iconv("UTF-8", "GB2312", $cv);
	            }
	            $data[$key]=implode("\t", $data[$key]);
	            
	        }
	        echo implode("\n",$data);
	    }
	}

	public function daochu_excel_d(){
    	$m = M('goods_orders_record');

    	$begin_time = strtotime(date("Y-m-d"));
    	$end_time = strtotime(date('Y-m-d')." +1 day");

    	$where['create_time'] = array('between',"$begin_time,$end_time");
    	$where['pay_status'] = 1;

    	$result = $m->where($where)->select();
    	if(empty($result)){
    		$this->error('没有数据可以导出');
    	}
    	$m_2 = M('goods_orders');
    	foreach($result as $k=>$v){		
    		$where_2['id'] = $v['goods_orders_id'];
    		$res = $m_2->where($where_2)->find();
    		$arr[$k]['pay_time'] = date('Y-m-d H:i:s',$res['pay_time']);
    		$arr[$k]['realname'] = $res['realname'];
    		$arr[$k]['goods_title'] = $v['goods_title'];
    		$arr[$k]['taocan_name'] = empty($v['taocan_name'])?'无':$v['taocan_name'];
    		$arr[$k]['goods_num'] = $v['goods_num'];
    		$arr[$k]['yunfei'] = $res['yunfei'];
    		$arr[$k]['pay_sum'] = $v['price_new'] * $v['goods_num'];
    		$arr[$k]['price_new'] = $v['price_new'];
    		$arr[$k]['fanli_jifen'] = $v['fanli_jifen'];
    		$arr[$k]['order_remarks'] = empty($res['order_remarks'])?'无':$res['order_remarks'];
    		if(empty($res['pay_time'])){
    			unset($result[$k]);
    		}
    	}
    	$title[] = '交易日期';
    	$title[] = '付款单位';
    	$title[] = '商品名称';
    	$title[] = '套餐';
    	$title[] = '数量';
    	$title[] = '邮费';
    	$title[] = '付款合计';
    	$title[] = '售价';
    	$title[] = '积分';
    	$title[] = '备注';
    	$filename = '('.date("Y-m-d").')销售明细表';
    	$this->exportexcel($arr,$title,$filename);
	}

	public function daochu_excel_m(){
    	$m = M('goods_orders_record');

    	$begin_time = strtotime(date("Y-m-01"));
    	$end_time = strtotime(date('Y-m-01')." +1 month");

    	$where['create_time'] = array('between',"$begin_time,$end_time");
    	$where['pay_status'] = 1;

    	$result = $m->where($where)->select();
    	if(empty($result)){
    		$this->error('没有数据可以导出');
    	}
    	$m_2 = M('goods_orders');
    	foreach($result as $k=>$v){		
    		$where_2['id'] = $v['goods_orders_id'];
    		$res = $m_2->where($where_2)->find();
    		$arr[$k]['pay_time'] = empty($res['pay_time'])?'':date('Y-m-d H:i:s',$res['pay_time']);

            if(empty($v['fanli_action'])){
                $arr[$k]['done_time'] = '-';
            }else{
                $arr[$k]['done_time'] = substr($v['fanli_action'], -19);    
            }

    		$arr[$k]['realname'] = empty($res['realname'])?'-':$res['realname'];
    		$arr[$k]['goods_title'] = $v['goods_title'];
    		$arr[$k]['taocan_name'] = empty($v['taocan_name'])?'无':$v['taocan_name'];
    		$arr[$k]['goods_num'] = $v['goods_num'];
    		$arr[$k]['yunfei'] = $res['yunfei'];
    		$arr[$k]['pay_sum'] = $v['price_new'] * $v['goods_num'];
    		$arr[$k]['price_new'] = $v['price_new'];
    		$arr[$k]['fanli_jifen'] = $v['fanli_jifen'];
    		$arr[$k]['order_remarks'] = empty($res['order_remarks'])?'无':$res['order_remarks'];
    		if(empty($res['pay_time'])){
    			unset($result[$k]);
    		}
    	}
    	$title[] = '交易日期';
        $title[] = '完成日期';
    	$title[] = '付款单位';
    	$title[] = '商品名称';
    	$title[] = '套餐';
    	$title[] = '数量';
    	$title[] = '邮费';
    	$title[] = '付款合计';
    	$title[] = '售价';
    	$title[] = '积分';
    	$title[] = '备注';
    	$filename = '('.date("Y-m-01").'到'.date("Y-m-d",$end_time).')销售明细表';
    	$this->exportexcel($arr,$title,$filename);
	}	
   
	public function daochu_excel_qj(){
    	$m = M('goods_orders_record');

    	if(!empty($_GET['begin_time']) && !empty($_GET['end_time'])){
    		$begin_time = strtotime($_GET['begin_time']);
    		$end_time = strtotime($_GET['end_time'])+86400;
    	}else{
    		$this->error('请先选择查询时间范围');
    	}

    	$where['create_time'] = array('between',"$begin_time,$end_time");
    	$where['pay_status'] = 1;

    	$result = $m->where($where)->select();

    	if(empty($result)){
    		$this->error('没有数据可以导出');
    	}

    	$m_2 = M('goods_orders');
    	foreach($result as $k=>$v){		
    		$where_2['id'] = $v['goods_orders_id'];
    		$res = $m_2->where($where_2)->find();
    		$arr[$k]['pay_time'] = empty($res['pay_time'])?'':date('Y-m-d H:i:s',$res['pay_time']);
            
            if(empty($v['fanli_action'])){
                $arr[$k]['done_time'] = '-';
            }else{
                $arr[$k]['done_time'] = substr($v['fanli_action'], -19);    
            }

    		$arr[$k]['realname'] = empty($res['realname'])?'-':$res['realname'];
    		$arr[$k]['goods_title'] = $v['goods_title'];
    		$arr[$k]['taocan_name'] = empty($v['taocan_name'])?'无':$v['taocan_name'];
    		$arr[$k]['goods_num'] = $v['goods_num'];
    		$arr[$k]['yunfei'] = $res['yunfei'];
    		$arr[$k]['pay_sum'] = $v['price_new'] * $v['goods_num'];
    		$arr[$k]['price_new'] = $v['price_new'];
    		$arr[$k]['fanli_jifen'] = $v['fanli_jifen'];
    		$arr[$k]['order_remarks'] = empty($res['order_remarks'])?'无':$res['order_remarks'];
    		if(empty($res['pay_time'])){
    			unset($arr[$k]);
    		}
    	}

    	$title[] = '交易日期';
        $title[] = '完成日期';
    	$title[] = '付款单位';
    	$title[] = '商品名称';
    	$title[] = '套餐';
    	$title[] = '数量';
    	$title[] = '邮费';
    	$title[] = '付款合计';
    	$title[] = '售价';
    	$title[] = '积分';
    	$title[] = '备注';
    	$filename = '('.date("Y-m-d",$begin_time).'到'.date("Y-m-d",$end_time-86400).')销售明细表';
    	$this->exportexcel($arr,$title,$filename);
	}   
}




