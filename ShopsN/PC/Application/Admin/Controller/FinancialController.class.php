<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//理财专区
class FinancialController extends AuthController{
	
     public function setting(){
		 $financial_setting=M('financial_setting');

		 $data =$financial_setting->select();
		 $this->assign('rows',$data);
		 $this->display();
	 }
	 public function order(){
		 $this->display();
	 }
	 public function edit(){
		 $financial_setting=M('financial_setting');
		 if(IS_POST){
			 $id=I('post.id');
			 $map['grade']=I('post.grade');
			 $map['annual_rate']=I('post.annual_rate')/100;
			 $map['half_rate']=I('post.half_rate')/100;
			 $map['max_buy']=I('post.max_buy');
						 $rst=$financial_setting->where(array('id'=>$id))->save($map);
			 if($rst){
				 $this->success('修改成功!');
			 }else{
				 $this->error('修改失败!');
			 }
			  
		 }else{
			 $id=I('get.id');
			 $row =$financial_setting->where(array('id'=>$id))->find();
			 $this->assign('row',$row);
			 $this->display();
		 }
		 
	 }
	 
}