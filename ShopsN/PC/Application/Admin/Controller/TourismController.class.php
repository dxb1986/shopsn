<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//旅游管理
class TourismController extends AuthController {

    //旅游一级分类列表
    public function class_list(){
    	if(!empty($_POST)){
    		$upload = new \Think\Upload();// 实例化上传类
    		$upload->maxSize = 3145728;// 设置附件上传大小
    		$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
    		$upload->rootPath = './Public/Uploads/class/';      //设置文件根目录
    		//上传文件
    		$info = $upload->upload();
    		$_POST['pic_url'] = $info['pic_url']['savepath'].$info['pic_url']['savename'];  //上传文件的路径
    		//var_dump($_POST['pic_url']);die;
    		if(!$info) {        // 上传错误提示错误信息
    			$this->error($upload->getError());
    		}else{
    			$m = M('goods_class');
    			$_POST['creatte_time'] = time();
    			$_POST['type'] = 2;
    			$result = $m->add($_POST);
    			if($result){
    				$this->success('保存成功');
    			}else{
    				$this->error('保存失败');
    			}
    		}
    	}else{
    		$m = M('goods_class');
    		$where['fid'] = 0;
    		$where['type']  = 2;
    		$result = $m->where($where)->order('sort_num ASC')->select();
    		foreach ($result as $k=>$v){
    			$result[$k]['create_time'] = date('Y-m-d H:i:s',$result[$k]['create_time']);
    		}
    		foreach($result as $n=>$val){
    			$result[$n]['vo'] = $m->where('fid='.$val['id'])->select();
    		}
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    
    
    //旅游一级分类更新
    public function class_edit(){
    	$m = M('goods_class');
    	$_POST['update_time'] = time();
    	$result = $m->save($_POST);
    	if($result){
    		$this->ajaxReturn(1);	//删除成功
    	}else{
    		$this->ajaxReturn(0);	//删除失败
    	}
    }
    
    //旅游一级分类删除
    public function class_del(){
    	$m = M('goods_class');
    	$where['id'] = $_POST['id'];
    	$checkone = M('goods')->where('class_id='.$where['id'])->find();
    	if(!empty($checkone)){
    		$this->ajaxReturn(2);//分类下有商品
    	}else{
    		$result = $m->where($where)->delete();
    		if($result){
    			$this->ajaxReturn(1);	//删除成功
    		}else{
    			$this->ajaxReturn(0);	//删除失败
    		}
    	}
    }
    
    //添加二级分类
    public function tourism_addpage(){
    	if(IS_POST){
    		$data = I('post.');
    		$data['fid'] = $_GET['id'];
    		$data['type'] = 2;
    		$goods_class = M('goods_class');
    		if($goods_class->create($data)){
    			if($goods_class->add($data)){
    				$this->success('子类添加成功',U('Goods/goods_addpage'));
    			}else{
    				$this->error('添加失败');
    			}
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$resone = M('goods_class')->where('fid=0')->select();
    		$this->assign('resone',$resone);
    		$this->display();
    	}
    }
    
    //添加旅游商品
    public function tourisms_add(){
    
		if(!empty($_POST)){
			$upload = new \Think\Upload();// 实例化上传类
			$upload->maxSize = 3145728;// 设置附件上传大小
			$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
			$upload->rootPath = './Public/Uploads/goods/';		//设置文件根目录
			//上传文件
			$info = $upload->upload();
			$_POST['pic_url'] = $info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
			foreach ($info as $k=>$v){
				if($k >= 1){
					$arr[] = $info[$k]['savepath'].$info[$k]['savename'];
				}
			}
			

			foreach($_POST['chufa_date'] as $k=>$v){
				if(!empty($v)){
					$chufa_arr[$k]['chufa_date'] = $v;
					$chufa_arr[$k]['chufa_price'] = trim($_POST['chufa_price'][$k]);
                    $chufa_arr[$k]['chufa_price_et'] = trim($_POST['chufa_price_et'][$k]);
				}				
			}
			
			$_POST['chufa_taocan'] = serialize($chufa_arr);
			
			$_POST['pic_tuji'] = serialize($arr);
			if(!$info) {		// 上传错误提示错误信息
				$this->error($upload->getError());
			}else{		// 上传成功
				$m = M('goods');
				$_POST['create_time'] = time();
				$_POST['type'] = 2;
				
				if($m->add($_POST)){
					$this->success('添加成功');
				}else{
					$this->success('添加失败');
				}
			}
		}else{
			$m = M('goods_class');
			$result_class = $m->where('type=2 AND fid=0')->order('sort_num ASC')->select();
			$this->assign('result_class',$result_class);
			$this->display();
		}
    }
    
     //旅游产品列表
    public function tourisms_list(){
        $goods_class = M('goods_class');
        $result_class = $goods_class->where('type=2 and fid=0')->order('sort_num ASC')->select();

        $this->assign('result_class',$result_class);
        $m = M('goods');
        $nowPage = isset($_GET['p'])?$_GET['p']:1;
        if(!empty($_GET['title'])){
            $where['title'] = array('like','%'.$_GET['title'].'%');
        }
        if(!empty($_GET['keyword'])){
            $where['keyword'] = array('like','%'.$_GET['keyword'].'%');
        }
        if(!empty($_GET['class_id'])){
            $where['class_id'] = array('eq',$_GET['class_id']);
        }
        if($_GET['class_fid'] != null){
            $where['class_fid'] = array('eq',$_GET['class_fid']);
        }
        $where['type'] = 2;
        // page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
        $result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();
       // echo $m->getlastsql();
        $nid = count($result);
        foreach ($result as $k=>$v){
            $result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
            $result[$k]['nid'] = $nid--;
            $class_name = $goods_class->field('class_name,id')->where('id='.$v['class_id'])->find();
            $result[$k]['class_name'] = $class_name['class_name'];
        }
    
        //分页
        $count = $m->where($where)->count(id);      // 查询满足要求的总记录数
        $page = new \Think\Page($count,PAGE_SIZE);      // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $page->show();      // 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('result',$result);
    
        $this->display();
    }
    
    
    public function ajaxtourisms(){
        $id = I('post.fid');
        $info = M('goods_class')->where(array('fid'=>$id))->select();
        if(empty($info)){
            $this->ajaxReturn(0);
        }else{
            $this->ajaxReturn($info);
        }
    }
    
    
    //隐藏或者打开
    public function class_hide(){
    	$m = M('GoodsClass');
    	$_POST['update_time'] = time();
    	$result = $m->save($_POST);
    	//$this->ajaxReturn(1);	//操作成功
    	if($result){
    		$this->ajaxReturn(1);	//操作成功
    	}else{
    		$this->ajaxReturn(0);	//操作失败
    	}
    }
    
    //旅游产品编辑
    public function tourisms_edit(){
		if(!empty($_POST)){
    		$m = M('goods');
    		$where['id'] = $_POST['id'];	//活动ID
    		$upload = new \Think\Upload();// 实例化上传类
    		$upload->maxSize = 3145728;// 设置附件上传大小
    		$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
    		$upload->rootPath = './Public/Uploads/goods/';		//设置文件根目录
    		//上传文件
    		$info = $upload->upload();
    		$_POST['pic_url'] = $info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
    		foreach ($info as $k=>$v){
    			if($k >= 1){
    				$arr[] = $info[$k]['savepath'].$info[$k]['savename'];
    			}
    		}

			foreach($_POST['chufa_date'] as $k=>$v){
				if(!empty($v)){
					$chufa_arr[$k]['chufa_date'] = $v;
                    $chufa_arr[$k]['chufa_price'] = trim($_POST['chufa_price'][$k]);
                    $chufa_arr[$k]['chufa_price_et'] = trim($_POST['chufa_price_et'][$k]);
				}				
			}
			
			$_POST['chufa_taocan'] = serialize($chufa_arr);

			if(empty($_POST['class_fid'])){
				$this->error('一级商品分类不能为空');
			}
			if(empty($_POST['class_id'])){
				$this->error('二级商品分类不能为空');
			}
			
    		$_POST['pic_tuji'] = serialize($arr);
    		if(empty($_POST['pic_url'])){
    			unset($_POST['pic_url']);
    		}
    		if($_POST['pic_tuji'] == 'N;'){
    			unset($_POST['pic_tuji']);
    		}
    		$_POST['update_time'] = time();		//更新时间
    		$result = $m->where($where)->save($_POST);
    		if($result){
    			$this->success('修改成功');
    		}else{
    			$this->error('修改失败');
    		}
    	}else{
    		$goods_class = M('goods_class');
    		$result_class = $goods_class->where('type=2 AND fid=0')->order('sort_num ASC')->select();
    		$this->assign('result_class',$result_class);
			
    		$m = M('goods');
    		$where['id'] = $_GET['id'];	//活动ID
    		$result = $m->where($where)->find();
			
			$chufa_taocan = unserialize($result['chufa_taocan']);
			$this->assign('chufa_taocan',$chufa_taocan);
			
    		$where2['id'] = $result['class_id'];
    		$class_name = $goods_class->where($where2)->find();
    		$result['class_name'] = $class_name['class_name'];
			
			$class_fid_name = $goods_class->where('id='.$result['class_fid'])->find();
			$this->assign('class_fid_name',$class_fid_name);
			
			
			//dump($result);
			
    		$this->assign('result',$result);
    		$result_tuji = unserialize($result['pic_tuji']);
    		$this->assign('result_tuji',$result_tuji);
    		$this->display();
    	}
    }
    
    //旅游产品删除
    public function tourisms_del(){
    	$where['id'] = $_POST['id'];	//活动ID
    	$m = M('goods');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    		$this->ajaxReturn($data);
    	}else{
    		$data['code'] = '0';	//删除失败
    		$this->ajaxReturn($data);
    	}
    }
    
    
    //属性管理
    public function goods_shuxing_add(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['create_time'] = time();
    		$result = $m->add($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$this->display();
    	}
    }
    
    public function goods_shuxing_edit(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['update_time'] = time();
    		$result = $m->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$m = M('goods_shuxing');
    		$where['id'] = $_GET['id'];
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //属性列表
    public function goods_shuxing_list(){
    	$m = M('goods_shuxing');
    	$result = $m->where($where)->select();
    	foreach ($result as $k=>$v){
    		$result[$k]['shuxing_content'] = explode('|', $v['shuxing_content']);
    	}
    	$this->assign('result',$result);
    	$this->display();
    }
    
    public function goods_shuxing_del(){
    	$where['id'] = $_POST['id'];
    	$m = M('goods_shuxing');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    	}else{
    		$data['code'] = '0';	//删除失败
    	}
    	$this->ajaxReturn($data);
    }
    
    //商品属性内容添加
    public function goods_shuxing_content_add(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['update_time'] = time();
    		$_POST['shuxing_content'] = implode('|', $_POST['shuxing_content']);
    		$result = $m->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$m = M('goods_shuxing');
    		$where['id'] = $_GET['id'];
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
	
	//一级分类是否首页推荐
	public function shoutui(){
		if(IS_POST){
			$id = $_POST['id'];
			$findone = M('goods_class')->where('id='.$id)->find();
			$count = M('goods_class')->where('shoutui=1')->count();
				if($findone['shoutui'] == 0){
					if($count>9){
						$this->ajaxReturn(3);
					}else{
						$data['shoutui'] = 1;
						$resone = M('goods_class')->where('id='.$id)->save($data);
						$this->ajaxReturn(1);
					}
				}else if($findone['shoutui'] == 1){
						$data['shoutui'] = 0;
						$resone = M('goods_class')->where('id='.$id)->save($data);
						$this->ajaxReturn(2);
				}
		}
	}
	
    //订单列表
	public function orders_list(){
		$orders_num = I('get.orders_num');
		if(!empty($orders_num)){
			$where['orders_num'] = $orders_num;
		}
		$mobile = I('get.mobile');
		if(!empty($mobile)){
			$where['mobile'] = $mobile;
		}
		$pay_status = I('get.pay_status');
        if($pay_status == 1){
            $where['pay_status'] = array('eq',1);
        }elseif($pay_status == -1){
            $where['pay_status'] = array('eq',0);
        }else{
            //$where['pay_status'] = array('eq',1);
        }
        $orders_status = I('get.orders_status');
        if(!empty($orders_status)){
            $where['orders_status'] = $orders_status;
        }
            
        $this->assign('pay_status',$pay_status);

        //时间查询
        if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
            $where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
        }

        $where['order_type'] = 1;
		$goods_orders = M('Goods_orders');
		$count = $goods_orders->where($where)->count();
		$Page  = new \Think\Page($count,10);
		$show   = $Page->show();
		$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
        //dump($res);

		$this->assign('page',$show);// 赋值分页输出
		$this->assign('res',$res);
		$this->display();
    }
    
    //订单列表 - 待支付
    public function wait_pay(){
    	$orders_num = I('get.orders_num');
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['pay_status'] = 0;    
    	$where['order_type'] = 1;
    	
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,10);
    	$show   = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }

    //订单列表 - 已支付
    public function done_pay(){
    	$orders_num = I('get.orders_num');
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['pay_status'] = 1;    
    	$where['order_type'] = 1;
    	
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,10);
    	$show   = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }
    
    //订单详情
    public  function order_lst_detail(){
        $id = I('get.id');
        $goods_orders = M('Goods_orders');
        $res = $goods_orders->where('id='.$id)->find();

        $g = M('Goods_orders_record');

        $good_id = $res['id'];
        $res['result'] = $g->where('goods_orders_id='.$good_id)->select();
        $this->assign('list',$res);
        //dump($res);
		$this->assign('jfa',$res['use_jf_limit']);
		$this->assign('jfb',$res['use_jf_currency']);
        $this->display();

    }

	//标记订单为完成状态
	public function done(){
		if(IS_AJAX){
			$order_id=I('order_id');
			$password=I('password');
			$goods_orders=M('goods_orders');
			$pwd=M('pwd')->where(array('id'=>1))->getField('password');
			if($pwd!=md5($password)){
				$this->ajaxReturn(2);//权限密码错误
				exit;
			}
			$account=session('account');
			$biaoji=$goods_orders->where(array('id'=>$order_id))->save(array('orders_status'=>5,'fanli_action'=>$account.'触发，时间：'.date('Y-m-d H:m:s',NOW_TIME)));
		    if($biaoji){
				//调用提成逻辑
				$this->ajaxReturn(1);
			}else{
				$this->ajaxReturn(0);
			}
		}
	}


    //已退货
    public function tuidan_ed(){
        $orders_num = I('get.orders_num');
        $where = array();
        if(!empty($orders_num)){
            $where['orders_num'] = $orders_num;
        }
        $mobile = I('get.mobile');
        if(!empty($mobile)){
            $where['mobile'] = $mobile;
        }
        $where['pay_status'] = 1;
        $where['tuihuo_chuli_status'] = 1;  //已允许退货
        $where['order_type'] = 1;
    
        $realname = I('get.realname');
        if(!empty($realname)){
            $where['realname'] = array('like',"%$realname%");
        }
    
        //时间查询
        if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
            $where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
        }
    
        $goods_orders = M('Goods_orders');
        $count = $goods_orders->where($where)->count();
        $Page  = new \Think\Page($count,20);
        $show  = $Page->show();
        $res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
         
        $this->assign('page',$show); //赋值分页输出
        $this->assign('res',$res);
        $this->display();
    }   

    //订单列表 - 已申请退货
    public function shenqing_tuidan(){
        $orders_num = I('get.orders_num');
        $where = array();
        if(!empty($orders_num)){
            $where['orders_num'] = $orders_num;
        }
        $mobile = I('get.mobile');
        if(!empty($mobile)){
            $where['mobile'] = $mobile;
        }
        $where['order_type'] = 1;
        $where['pay_status'] = 1;
        $where['orders_status'] = 4;    //已退货状态
        $where['tuihuo_chuli_status'] = 0;
        $where['order_type'] = 1;
         
        $realname = I('get.realname');
        if(!empty($realname)){
            $where['realname'] = array('like',"%$realname%");
        }
         
        $goods_orders = M('Goods_orders');
        $count = $goods_orders->where($where)->count();
        $Page  = new \Think\Page($count,20);
        $show  = $Page->show();
        $res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
        $this->assign('page',$show); //赋值分页输出
        $this->assign('res',$res);
        $this->display();
    }

    //订单列表  - 已完成
    public function done_jifen(){
        $orders_num = I('get.orders_num');
        $where = array();
        if(!empty($orders_num)){
            $where['orders_num'] = $orders_num;
        }
        $mobile = I('get.mobile');
        if(!empty($mobile)){
            $where['mobile'] = $mobile;
        }
        $where['pay_status'] = 1;
        $where['order_type'] = 1;
        $where['orders_status'] = 5;    //已完成状态
         
        $realname = I('get.realname');
        if(!empty($realname)){
            $where['realname'] = array('like',"%$realname%");
        }
        
        //时间查询
        if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
            $where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
        }
         
        $goods_orders = M('Goods_orders');
        $count = $goods_orders->where($where)->count();
        $Page  = new \Think\Page($count,20);
        $show  = $Page->show();
        $res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
        $this->assign('page',$show); //赋值分页输出
        $this->assign('res',$res);
        $this->display();
    }
}


