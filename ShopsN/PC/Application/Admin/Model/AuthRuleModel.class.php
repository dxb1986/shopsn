<?php
namespace Admin\Model;

use Think\Model;
use Common\Tool\Tool;

class AuthRuleModel extends Model 
{
    private static  $obj;
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    
    /**
     * 重写添加操作
     */
    public function add($data = '', $options = array(),  $replace = false)
    { 
        if (empty($data) || !is_array($data))
        {
            return array();
        }
       
        $addData  = $this->create($data);
        return parent::add($addData, $options, $replace);
    }
    
    /**
     * 添加前操作
     */
    protected function _before_insert(&$data, $options)
    {
        $data['create_time'] = time();
        $data['update_time'] = time();
        $data['status']      = 1;
        $data['type']        = 1;
        return $data;
    }
    
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    
    /**
    * {@inheritDoc}
    * @see \Think\Model::save()
    */
    
    public function save($data='', $options=array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
    
        return parent::save($data, $options);
    }
    /**
     * 获取权限列表 
     */
    public function getAuthGroupById( $field, $where = null, $fun = 'select', $order = 'sort DESC')
    {
        if (empty($field))
        {
            return array();
        }
        
        return $this->field($field)->where($where)->order($order)->$fun();
    }
    
    /**
     * 获取二级分类 
     * @param array $fauther 父级分类
     */
    public function getTwoChildren(array $fauther,  $field)
    {
        if (empty($fauther))
        {
            return array();
        }
        $ids = Tool::characterJoin($fauther, 'id');
        if (empty($ids))
        {
            return array();
        }
        $children = $this->getAuthGroupById($field, 'pid in('.$ids.')');
        return $children;
    }
    /**
     * 根据子类编号获取爷爷类编号 
     */
    public function getGrandFather(array $children)
    {
        if (empty($children))
        {
            return array();
        }
        //获取父级编号
        $pId = array_shift(Tool::compressArray($children,'pid'));
        
        $grandFatherId = self::topId($pId);
        
        foreach ($children as $key => &$value)
        {
            $value['pid'] = $grandFatherId;
        }
        
        return $children;
    }
    /**
     * 获取顶级编号 
     */
    private  function topId($topId)
    {
        if (!is_numeric($topId))
        {
            return false;
        }
        $id = self::getPk();
        $pId = self::find(array(
            'field' => id.',pid',
            'where' => array('id' => $topId),
        ));
        
        return (empty($pId)) ? false : ($pId['pid'] == 0 ? $pId[$id] : self::topId($pId['pid']));
    }
}