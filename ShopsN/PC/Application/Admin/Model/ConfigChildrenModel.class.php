<?php
namespace Admin\Model;

use Think\Model;

class ConfigChildrenModel extends Model
{
    private static  $obj;
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    /**
     * 添加前操作
     */
    protected function _before_insert(&$data, $options)
    {
        $data['create_time'] = time();
        $data['update_time'] = time();
        return $data;
    }
    /**
     * 更新前操作
     */
    protected function _before_update(&$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    /**
     * 重写添加操作
     */
    public function add($data = '', $options = array(),  $replace = false)
    { 
        if (empty($data) || !is_array($data))
        {
            return array();
        }
       
        $addData  = $this->create($data);
        return parent::add($addData, $options, $replace);
    }
    
    /**
     * 重写更新操作
     */
    public function save($data = '', $options = array() )
    {
        if (empty($data) || !is_array($data))
        {
            return array();
        }
       
        $data = $this->create($data);
        $id   = parent::save($data, $options);
        return $id;
    }
    
    public function delete($options = array())
    {
        if (empty($options['where'])  || !is_array($options) )
        {
            return false;
        }
   
        $id = parent::delete($options);
        
        return $id;
    }
    /**
     * 获取全部数据 
     */
    public function getAll(array $options = NULL)
    {
        return $this->field('create_time,update_time', true)->where($options)->select();
    }
}