<?php
namespace Admin\Model;

use Think\Model;

/**
 * 系统配置模型 
 */
class ConfigClassModel extends Model
{
    private static $obj;
    
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    
    /**
     * 添加前操作 
     */
    protected function _before_insert(&$data, $options)
    {
        $data['create_time'] = time();
        $data['update_time'] = time();
        return $data;
    }
    /**
     * 更新前操作 
     */
    protected function _before_update(&$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    
    /**
     * 重写添加操作 
     */
    public function add($data = '', Model $model, $options = array(),  $replace = false)
    {
        if (empty($data) || !is_array($data) || !($model instanceof Model))
        {
            return false;
        }
        $addData  = $this->create($data);
        $insertChildrenId = 0;
        if ( ($insertId  = parent::add($addData, $options, $replace)) != false && isset($data['p_id']))
        {
            $data['config_class_id'] = $insertId;
            $insertChildrenId = $model->add($data);
            return $insertChildrenId && $insertId;
        }
        return $insertId ;
    }
    
    /**
     * 重写更新操作
     */
    public function save($data = '',Model $model, $options = array() )
    {
        if (empty($data) || !is_array($data) || !($model instanceof Model))
        {
            return array();
        }
        $updateData = $this->create($data);
        $updateChildrenId = 0;
        if ( ($updateId  = parent::save($updateData, $options)) != false)
        {   
            $data['id'] = $data['children_id'];
            $updateChildrenId = $model->save($data);
            return $updateChildrenId && $updateId;
        }
        return $updateChildrenId ;
    }
    /**
     * 获取全部分类 
     */
    public function getAllClass(array $options)
    {
        if (empty($options))
        {
            return $options;
        }
        return parent::select($options);
    }
    /**
     * 获取全部子集分类【树形结构】
     * @param array $where 查询条件
     * @param array $field 查询的字段
     * @return string
     */
    public function getChildren(array $where = null, array $field = null)
    {
        // 根据地区编号  查询  该地区的所有信息
        $video_data   = parent::select(array(
            'where' => $where,
            'field' => $field,
        ));
        showData($video_data);
        if (empty($video_data))
        {
            return;
        }
        $pk    = $this->getPk();
        static $children = array();
      
        foreach ($video_data as $key => &$value)
        {
            if(!empty($value['id']))
            {
                $where['p_id'] = $value['id'];
                $child = $this->getChildren($where, $field);
                $children[$key] = $value;
                if (!empty($child))
                {
                    $children[$key]['children'] = $child;
                }
                unset($value, $child);
            }
        }
        return $children;
    }
    
  
    /**
     * 获取全部子集分类编号
     * @param array $where 查询条件
     * @param array $field 查询的字段
     * @return string
     */
    public function getChildrenId(array $where = null, array $field = null)
    {
        // 根据地区编号  查询  该地区的所有信息
        $video_data   = parent::select(array(
            'where' => $where,
            'field' => $field,
        ));
        static $data ;
        $pk    = $this->getPk();
        foreach ($video_data as $key => &$value)
        {
            if(!empty($value[$pk]))
            {
                $data .= ','. $value[$pk];
                $where['p_id'] = $value[$pk];
                $child = $this->getChildrenId($where, $field);
                if (!empty($child))
                {
                    foreach ($child as $key_value => $value_key)
                    {
                        if (!empty($value_key[$pk]))
                        {
                            $data.=','.$value_key[$pk];
                        }
                    }
                }
                unset($value, $child);
            }
        }
        return !empty($data) ? substr($data , 1) : null;
    }
    
    /**
     *  
     */
    public function getFind(array $options = array(), Model $model)
    {
        if (empty($options) || !is_array($options) || !($model instanceof Model))
        {
            return array();
        }
        
        $data = parent::find($options);
        if (!empty($data))
        {
            $children = $model->field('id as children_id,type_name,type,show_type')->where('config_class_id = "'.$data['id'].'"')->find();
            $data     = array_merge($data , (array)$children);
        }
        return $data;
    }
    
    /**
     * 是否还有下级分类 
     */
    public function isHaveClass(array $optionss)
    {
        if (empty($optionss))
        {
            return array();
        }
        $this->where($optionss)->count();
        return $this->where($optionss)->count();
    }
    /**
     * c重写删除 
     */
    public function delete($options = array(), Model $model)
    {
        if (empty($options['where']) || empty($options['field']) || empty($options['where']['id']) || !is_array($options) || !($model instanceof Model))
        {
            return array();
        }
        
        //是否有下级
        $have = $this->getChildrenId($options['where'], $options['field']);
      
        $deld = 0;
        if (false !== strpos($have, ','))
        {
            $sumId = $options['where']['id'].','.$have;
            $deld  = $model->delete(array(
                'where' => array('config_class_id' => array('in', $have))
            ));
            $options['where'] = array('id' => array('in',$sumId));
            
        }
        else 
        {
            $deld = $model->delete(array(
                'where' => array('config_class_id' => $options['where']['id'])
            ));
        }
        return  parent::delete($options) && $deld;
    }
    //是否存在
    public function isHaveName(array $options)
    {
        if (empty($options))
        {
            return array();
        }
        $data = $this->where($options)->find();
        return empty($data) ? false : true;
    }
    
    /**
     * 查找子集和自己 
     */
    public function getChildrenAndMe(array $options)
    {
        if (empty($options))
        {
            return array();
        }
        
        $data = $this->where($options)->select();
        
        return $data;
    }
}