<?php
namespace Common\Tool\Extend;

use Common\Tool\Tool;

/**
 * 数组操作类 
 */
class ArrayParse extends Tool implements \ArrayAccess
{
    private  $array = array();
    protected $children = array();
    
    protected $parseChildren = array();
    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetExists()
     */
    
    public function __construct(array $array)
    {
        $this->array = $array;
    }
    
    public function __set($name, $value)
    {
        if (!isset($this->array[$name]))
        {
            $this->array[$name] = $value;
        }
    }
    
    public function __get($name = null)
    {
        return isset($this->array[$name]) ? $this->array[$name] : $this->array;
    }
    
    public function offsetExists($offset)
    {
        // TODO Auto-generated method stub
        return isset($this->array[$offset]);
    }

    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset)
    {
        // TODO Auto-generated method stub
        return $this->array[$offset];
    }

    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value)
    {
        // TODO Auto-generated method stub
        $this->array[$offset] = $value;
    }

    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset)
    {
        // TODO Auto-generated method stub
        unset($this->array[$index]);
    }
    
    /**
     * 组合数据 
     */
    public function buildData(array $data = null)
    {
        $data  = empty($data) ? $this->array : $data;
        
        //阵列变量
        extract($data);
        foreach ($children as $key => &$value)
        {
            
            foreach ($configValue as $config => $nameValue)
            {
                if (array_key_exists($value['type_name'], $nameValue))
                {
                    $value['value'] = $nameValue[$value['type_name']];
                }
            }
        }
        
        foreach ($pData as $key => &$value)
        {
            if ($value['p_id'] == 0)
            {
                continue;
            }
            foreach ($children as $type => $name)
            {
                if ($value['id'] == $name['config_class_id'])
                {
                    $value['type_name'] = $name['type_name'];
                    $value['show_type'] = $name['show_type'];
                    $value['type']      = $name['type'];
                    $value['value']     = $name['value'];
                    unset($children[$type]);
                }
            }
        }
        return $pData;
    }
    
    public function buildConfig(array $data = null)
    {
        $data  = empty($data) ? $this->array : $data;
        //阵列变量
        extract($data);
        
        foreach ($children as $key => &$value)
        {
            foreach ($configValue as $config => $nameValue)
            {
                if (array_key_exists($value['type_name'], $nameValue))
                {
                    $value['value'] = $nameValue[$value['type_name']];
                }
            }
        }
        $this->children = $children;
        return $this;
    }
    
    public function parseConfig()
    {
        if (empty($this->children))
        {
            return array();
        }
        $data = $this->children;
        foreach ($data as $key => &$value)
        {
            $value[$value['type_name']] = $value['value'];
            
            unset($data[$key]['value'], $data[$key]['type_name']);
        }
        $this->parseChildren = $data;
        return $this;
    }
    
    public function oneArray( array & $receive, array $data = null)
    {
        $data =  (empty($data)) ? $this->parseChildren : $data;
            
        foreach ($data as $key => $value)
        {
            if (is_array($value))
            {
                $this->oneArray($receive, $value);
            }
            else
            {
                $receive[$key] = $value;
            }
        }
        return $receive;
    }
    /**
     * 添加增删改查权限 
     */
    public function addPromissona(array $masterPromission, array $otherPromission)
    {
        if (empty($masterPromission) || empty($otherPromission))
        {
            return $masterPromission;
        }
      
        foreach ($masterPromission as $key => &$value)
        {
            if (empty($value['children']))
            {
                continue;
            }
            foreach ($otherPromission as $other => $otherName)
            {
                $value['children'][] = $otherName;
            }
        }
        unset($otherPromission);
        return $masterPromission;
    }
    
    /**
     * 压缩数组 
     */
    public function compressArray(array $array, $key ='code')
    {
        if (empty($array))
        {
            return array();
        }
        
        foreach ($array as $name => &$value)
        {
            if (!array_key_exists($key, $value))
            {
                continue;
            }
            $value = $value[$key];
        }
        
        return $array;
    }
    /**
     * 计算返利积分
     * @param array $data 请求的数据
     * @return int
     */
    public function sumInteragel(array $data)
    {
        if (!is_array($data)) {
            return 0;
        }
        $interagel = 0;
        foreach ($data as $key => &$value)
        {
            $interagel += $value;
        }
        
        return $interagel;
    }
    
    /**
     * 数据重组 
     * @param array  $array      要重组的数据
     * @param string $recKey     重组对应的keykey的对应值
     * @param string $combineKey 要合并的key的对应值
     */
    public function recombination(array $array,  $recKey = 'goods_id', $combineKey = 'goods_num')
    {
        if (!is_array($array) || empty($array)) {
            return array();
        }
        $keyArray = array($recKey, $combineKey);
       
        /**
         * 检测键是否存在 
         */
        $i = 0;
        foreach ($array as $key => $value) 
        {
            if (in_array($key, $keyArray))
            {
                $i = 1;
            }
        }
        if ($i === 0) {
            return array();
        }
       
        $data = $flag = array();
        /**重组数据*/
        foreach ($array[$recKey] as $key => &$value)
        {
            if (array_key_exists($key, $array[$combineKey]))
            {
                $data[$key][$recKey]       = $value;
                $data[$key][$combineKey]   = $array[$combineKey][$key];
                $data[$key]['order_id']    = $array['order_id'];
                $data[$key]['goods_price'] = $array['goods_price'][$key];
                $flag[] = $data[$key];
            }
        }
        unset($data, $array);
        
        return $flag;
    }
}