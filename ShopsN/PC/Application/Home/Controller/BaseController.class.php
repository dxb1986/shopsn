<?php
namespace Home\Controller;
use Think\Controller;
use Home\Model\HotWordsModel;
use Common\Tool\Tool;
use Common\Controller\CommonController;
use Think\Hook;
use Common\TraitClass\WhoIsShe;
use Home\Model\GoodsClassModel;

/**
 * 前台控制器基类 
 */
class BaseController extends Controller
{
    /**
     * 读取导航(商品大分类) 
     */
    use CommonController;
    use WhoIsShe;
    public function _initialize($param = null)
    {
        $data = self::YouMastToBeWhatHowerveMe(array());
        
        empty($data) ? Hook::listen(ASDKLJHKJHJKHKUH, $param) : false;
        
        if (!S('nav_data')) {
            $model = GoodsClassModel::getInition();
            //查找导航 0为显示
            $data = $model->field('class_name,id')->where('fid =0 and hide_status = 0 and is_show_nav= 0')->select();
            
            S('nav_data', $data, 600);
        }
        $data = self::YouMastToBeWhatHowerve();
        if (!S('result_class'))
        {
            $goods_model = new \Home\Model\GoodsClassModel();
            //全部分类
            $resul_class = $goods_model->getProductClass(array(
                'where' => array('fid' => 0, 'type' => 1 , 'hide_status' => 0),
                'limit' => 12, 
                'order' => 'sort_num DESC'
            ));
            S('result_class', $resul_class, 600);
        }
        //获取购物车数量
        if (!empty($_SESSION['user_id'])) 
        {
            if (!S('cart_count')) {
                $cart_model = new \Home\Model\GoodsCartModel();
                
                $count      = $cart_model->getCartCount(array('user_id' => $_SESSION['user_id']));

                //获取最新添加购物车的商品信息
                $cartNewGoods = $cart_model->getNewCartForGood(array(
                    'where' => array('user_id' => $_SESSION['user_id']),
                    'field' => array('goods_id,id as cart_id,price_new,goods_num'),
                    'order' => array('create_time DESC')
                ), new \Think\Model('goods'));
                
                if (!empty($cartNewGoods)) {
                    $cartNewGoods['title'] = strlen($cartNewGoods['title']) > 20 ? \Common\Tool\Tool::cut_str($cartNewGoods['title'], 10) : $cartNewGoods['title'];
                }
                S('cart_count', $count, 30);
                S('cart_new_goods', $cartNewGoods, 30);
            }
        }
        else 
        {
            S('cart_count', 0);
        }
        $this->intnetConfig  =  $this->getConfig();
        $this->key_words     =  $this->keyWord();
        $this->result_class  = S('result_class');
        $this->nav_data      = S('nav_data');
        $this->cart_count    = S('cart_count');
        $this->cart_new_goods = S('cart_new_goods');
        $this->intnetTitle    = $this->getConfig('intnet_title');
        $this->shd            = $data;
    }
    /**
     * 关键词搜索
     */
    protected  function keyWord()
    {
        // 获取关键词 及其分类
        $data = HotWordsModel::getInitnation()->select(array(
            'where' => array('is_hide' => '0'),
            'field' => array('id', 'hot_words', 'goods_class_id'),
        ));
        return $data;
    }
    /**
     * ajax 返回数据
     */
    protected function ajaxReturnData($data, $status= 1, $message = '操作成功')
    {
        $this->ajaxReturn(array(
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ));
        die();
    }
    protected function isLogin()
    {
        if(empty($_SESSION['user_id']) ) {
          
            $this->redirect('Public/login');
        }
    }
    
    /**
     * 提示client
     * @param array   $data     要检测的数据
     * @param string  $checkKey 要检测的键
     * @param string  $message  信息
     */
    protected function prompt( $data, $checkKey, $message , $isValidate = true)
    {
        if (empty($data)) {
            $this->error($message);
        } elseif(is_array($data) && empty($data[$checkKey]) && $isValidate ) {
            $this->error($message);
        }
        return true;
    }
    
    
    protected function alreadyInData($data, $message= '更新成功')
    {
        if (!empty($data)) {
            $this->error($message);
        }
        return true;
    }
    
    
    
}