<?php
namespace Home\Controller;
use Home\Model\OrderModel;
use Common\Tool\Tool;
use Common\Model\OrderGoodsModel;
use Home\Model\GoodsModel;
use Common\Model\UserAddressModel;

class OrderController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        //检查是否登录
        $this->isLogin();
    }
    
    /**
     * 订单列表 【商品图片，名称，套餐，数量，单价，总额，收货地址，收货人下单时间】【全部订单】
     */
    public function index()
    {
        //订单模型
        $orderData = $this->getOrder(array($_SESSION['user_id']));
        
        $this->res = $orderData;
        $this->display();
    }
    
    /**
     * 待付款，
     * 【// -1:取消订单,0 未支付，1已支付，2，发货中，3已发货，4已收货，5退货审核中，6审核失败，7审核成功，8退款中，9退款成功, 10：代发货，11待收货】
     */
    
    public function paymentForlist()
    {
        $orderData = $this->getOrder(array($_SESSION['user_id'], OrderModel::NotPaid), ' and order_status ="%s" ');
        
        $this->res = $orderData;
        $this->display('index');
    }
    
    /**
     *  待收货
     */
    public function receiptOfGoods()
    {
        $status = OrderModel::YesPaid.','.OrderModel::InDelivery;
        $orderData = $this->getOrder(array($_SESSION['user_id']), ' and order_status in('.$status.') ');
        
        $this->res = $orderData;
        $this->display('index');
    }
    
    /**
     * 待评价 
     */
    public function paymentsWaite()
    {
        $orderData = $this->getOrder(array($_SESSION['user_id'], 0, OrderModel::YesPaid), ' and comment_status ="%s" and order_status ="%s" ');
        $this->res = $orderData;
        $this->display('index');
    }
    
    /**
     * 待完成  [已发货，还没收到]
     */
    public function orderEnd()
    {
        $orderData = $this->getOrder(array($_SESSION['user_id'], OrderModel::AlreadyShipped), ' and order_status ="%s" ');
        $this->res = $orderData;
        $this->display('index');
    }
    /**
     * 辅助方法
     */
    private  function getOrder(array $value, $where = null)
    {
        $order = OrderModel::getInitation()->getOrderByUser($value, $where);
        Tool::connect('parseString');
       //获取收货地址【传递收货地址模型】
        $data = UserAddressModel::getInitation()->goodsAdressByOrder($order);
        
        $this->prompt($data, null, '暂无订单', false);
        //获取订单商品信息
       
        $goodsData = OrderGoodsModel::getInitnation()->getGoodsInfoByOrder($data);
       
        //传递商品模型
        //传递给商品表
        $goods  = GoodsModel::getInitation()->getGoodsByChildrenOrderData($goodsData);
      
        $this->prompt($goods, null, '意外错误，很抱歉', false);
        //组合数据
        
        $orderData = Tool::parseTwoArray($data, $goods, 'order_id', array('goods'));
        
        return $orderData;
    }
    
}