<?php
namespace Home\Controller;

use Home\Model\HotWordsModel;
use Home\Model\GoodsModel;

class SearchController extends BaseController
{
    public function index()
    {
        \Common\Tool\Tool::checkPost($_GET, array('is_numeric' => array('id')), true , array('id')) ? true : $this->error('灌水机制已打开');
        
        //连接驱动
        \Common\Tool\Tool::connect('Mosaic');
        //获取关键词
        $data = HotWordsModel::getInitnation()->parseData(array(
            'where' => array('is_hide' => '0'),
            'field' => array('id', 'hot_words', 'goods_class_id')
        ),new \Think\Model('goods_class'));
        
        //Uploads/goods/2016-08-26/58097a19bb50a.jpg
        //传递给商品模型
        $receiveGoods = GoodsModel::getInitation()->getGoodsData($data);
        $this->result = $receiveGoods;
        $this->display('Index/search');
    }
}