<?php
namespace Home\Model;
use Think\Model;

/**
 * @author Administrator
 * 商品重量模型（用于运费计算） 
 */

class FreightModel extends Model
{
    public function getFreight(array $options, Model $model)
    {
        if (empty($options) || !is_array($options) || !($model instanceof Model)) {
            return array();
        }
        $areaData = $model->find($options);
        $data      = array();
        if (!empty($areaData))
        {
            $data = $this->find(array(
                'where' => array('province' => $areaData['areaid']),
                'field' => array('ykg,money,onemoney')
            ));
        }
        return $data;
    }
}