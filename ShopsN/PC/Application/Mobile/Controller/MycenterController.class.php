<?php
namespace Mobile\Controller;
use Think\Controller;

//前台会员中心模块
class MycenterController extends Controller{
	
	//短信验证码
	public function send_newsms($mobile,$content){
		header("Content-Type: text/html; charset=UTF-8");
		$flag = 0;
		$params='';//要post的数据
		$argv = array(
				'name'=>'dxwzzy',     //必填参数。用户账号
				'pwd'=>'2E80700AF2D325872D9E11726763',     //必填参数。（web平台：基本资料中的接口密码）
				'content'=>$content,
				'mobile'=>$mobile,   //必填参数。手机号码。多个以英文逗号隔开
				'stime'=>'',   //可选参数。发送时间，填写时已填写的时间发送，不填时为当前时间发送
				'sign'=>'【掌中宝】',    //必填参数。用户签名。
				'type'=>'pt',  //必填参数。固定值 pt
				'extno'=>''    //可选参数，扩展码，用户定义扩展码，只能为数字
		);
		foreach ($argv as $key=>$value) {
			if ($flag!=0) {
				$params .= "&";
				$flag = 1;
			}
			$params.= $key."="; $params.= urlencode($value);// urlencode($value);
			$flag = 1;
		}
		$url = "http://web.duanxinwang.cc/asmx/smsservice.aspx?".$params; //提交的url地址
		//echo $url;
		$con= substr( file_get_contents($url), 0, 1 );  //获取信息发送后的状态
		if($con == '0'){
			return true;
		}else{
			return false;
		}
	}
	
	//用户进入会员中心   判断是否已登录    状态不同进入页面不同
	public function person_common(){
		$userid = $_COOKIE['userid'];
		if(empty($userid)){
			$this->redirect('Mycenter/nologin_person_center');
		}else{
			$this->redirect('Mycenter/person_center');
		}
	}
	
	
	//未登录的用户中心页面 
	public function nologin_person_center(){
		$this->display();
	}
	
	//已经是登录状态的会员中心页面
	public function person_center(){
		$userid = $_COOKIE['userid'];
		if(empty($userid)){
			$this->redirect('User/login');//判断是否登录   未登录跳转到登录页面
		}else{
			$where['id'] = $_COOKIE['userid'];
			$findone = M('user')->where($where)->find();
			$data['memberid'] = sprintf("%05d",$findone['id']);
			$this->assign('data',$data);
			$this->assign('find',$findone);
			$this->assign('zhanghu',$_SESSION);
			//统计优惠卷

			$resone = M('user')->where($where)->find();

			$where2['mobile'] = $_SESSION['mobile'];
			$coupon['count'] = M('user_coupons')->where($where2)->count();

			//统计用户积分
			$member_profit=D('Profit');
			$where_m_id['member_id'] = session('user_id');
			$jf=$member_profit->where($where_m_id)->find();
			$this->assign('coupon',$coupon);
			$this->assign('grade_name',$grade_name);
			$this->assign('jf',$jf);

			$m = M('member','vip_');
			$where_m['id'] = $id;
			$res_m = $m->where(array('id'=>$userid))->find();

			if($res_m['grade_name'] != '会员' && $res_m['grade_name'] != '合伙人'){
				$this->redirect('Mycenter/become_hui');
			}

			$this->assign('res_m',$res_m);
			$this->display();
		}
	}
	
	
	//个人信息页面
	public function person_information(){
		$userid = $_COOKIE['userid'];
		if(empty($userid)){
			$this->redirect('User/login');//判断是否登录   未登录跳转到登录页面
		}else{
			$findone = M('user')->where('id='.$userid)->find();
			$this->assign('find',$findone);
			$this->display();
		}
	}
	
	//修改生日
	public function change_birthday(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}
		if(IS_POST){
			$userid = $_COOKIE['userid'];
			$data['birthday'] = date("Y-m-d",$_POST['timesmonth']);
			var_dump($data);
			$resone = M('user')->where('id='.$userid)->save($data);
			if($resone){
				$date['isok'] = 'ok';
				exit(json_encode($date));
			}else{
				$date['isok'] = 'off';
				exit(json_encode($date));
			}
		}
	}
	
	//修改昵称
	public function change_name(){
		$userid = $_COOKIE['userid'];
		if(empty($userid)){
			$this->redirect('User/login');//判断是否登录   未登录跳转到登录页面
		}else{
			if(IS_POST){
				$userid = $_COOKIE['userid'];
				$data['username'] = $_POST['username'];
				$resone = M('user')->where('id='.$userid)->save($data);
				if($resone){
					$date['isok'] = 'ok';
					exit(json_encode($date));
				}else{
					$date['isok'] = 'off';
					exit(json_encode($date));
				}
			}else{
				$findone = M('user')->where('id='.$userid)->find();
				$this->assign('find',$findone);
				$this->display();
			}
		}
	}
	
	//修改身份证
	public function change_idcard(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(IS_POST){
				$userid = $_COOKIE['userid'];
				$data['idcard'] = $_POST['idcard'];
				$user = M('user');
				$resone = $user->where('id='.$userid)->save($data);
				if($resone){
					$this->ajaxReturn('ok');
				}else{
					$this->ajaxReturn('off');
				}
			}else{
				$userid = $_COOKIE['userid'];
				$result = M('user')->where('id='.$userid)->find();
				$this->assign('vv',$result);
				$this->display();
			}
		}
	}
	
	
	//修改手机号需要获取新手机号的验证码
	public function change_newphonecod(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(IS_POST){
				$userid = $_COOKIE['userid'];
				$mobile = $_POST['phone'];
				$resone = M('user')->where('id='.$userid)->find();
				$result = M('user')->where('mobile='.$mobile)->find();
				if($mobile == $resone['mobile'] && !empty($result)){
					$data['isok'] = 'nochange';
					exit(json_encode($data));
				}else if($mobile != $resone['mobile'] && !empty($result)){
					$data['isok'] = 'have';
					exit(json_encode($data));
				}else{
					$mobile = $_POST['phone'];
					$code = rand(100000,999999);
					session('Mobile_change_newphonecod',$code);
					$content='短信验证码为:'.$code.',请勿将验证码提供给他人.';
					$ret = $this->send_newsms($mobile,$content);
					if($ret){
						$data['isok'] = 'ok';
						$data['code'] = $code;
						exit(json_encode($data));
					}
				}
			}
		}
	}
	
	//修改手机号码
	public function change_phone(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(IS_AJAX){
				$userid = $_COOKIE['userid'];
				$data['mobile'] = $_POST['phone'];
				$yzm= $_POST['yzm'];
				if(!session('?name')){
					$this->ajaxReturn('e');//未点击发送验证码
					exit;
				}
				if(session('Mobile_change_newphonecod')!=$yzm){
					$this->ajaxReturn('e');
					exit;
				}
				session('Mobile_change_newphonecod',null); 
				$user = M('user');
				$admin = M('admin','vip_');
				$member = M('member','vip_');
				$user->startTrans();
				$old_phone=$user->where('id='.$userid)->getField('mobile');
				$rst1=$admin->where(array('account'=>$old_phone))->save(array('account'=>$data['mobile']));
				$rst2=$member->where(array('id'=>$userid))->save(array('mobile'=>$data['mobile']));
				$resone = $user->where('id='.$userid)->save($data);
				if($resone && $rst1 && $rst2){
					$user->commit();
					$this->ajaxReturn('ok');
				}else{
					$user->rollback();
					$this->ajaxReturn('off');
				}
			}else{
				$userid = $_COOKIE['userid'];
				$result = M('user')->where('id='.$userid)->find();
				$this->assign('vv',$result);
				$this->display();
			}
		}
	}
	
	//修改密码-判断原密码输入是否正确
	public function check_password(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(IS_POST){
				$userid = $_COOKIE['userid'];
				$oldpassword = md5($_POST['password']);
				$resone = M('user')->where('id='.$userid)->find();
				if($oldpassword != $resone['password']){
					$date['isok']= 'ok';
					exit(json_encode($date));
				}else{
					$date['isok']= 'off';
					exit(json_encode($date));
				}
			}
		}
	}
	
	//修改密码
	public function change_password(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(IS_POST){
				$userid = $_COOKIE['userid'];
				$data['password'] = md5($_POST['newpasswordtwo']);
				$resone = M('user')->where('id='.$userid)->save($data);
				if($resone){
					$date['isok'] = 'ok';
					exit(json_encode($date));
					unset($_COOKIE);
				}else{
					$date['isok'] = 'off';
					exit(json_encode($date));
				}
			}else{
				$userid = $_COOKIE['userid'];
				$checkone = M('user')->where('id='.$userid)->find();
				$this->assign('vo',$checkone);
				$this->display();
			}
		}	
	}
	
	//个人信息-地址列表
	public function address_list(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$userid = $_COOKIE['userid'];
			$result = M('user_address')->where('user_id='.$userid)->select();
			$this->assign('result',$result);
			$this->display();
		}
	}
	
	//个人信息-修改地址
	public function change_address(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(!empty($_POST)){
				$m = M('user_address');
				$addressid = $_GET['addressid'];
				$_POST['create_time'] = time();
				if(empty($_POST['status'])){
					$_POST['status'] = 0;
				}else {
					//把默认地址全部设置为非默认
					$data['status'] = 0;
					$m->where('user_id='.$_COOKIE['userid'])->save($data);
				}
				if($m->where('id='.$addressid )->save($_POST)){
					$this->redirect('Mycenter/address_list');
				}
			}else{
				$addressid = $_GET['addressid'];
				$userid = $_COOKIE['userid'];
				$where['user_id'] = $userid;
				$where['id'] = $addressid;
				$result = M('user_address')->where($where)->find();
				$this->assign('vo',$result);
				$this->display();
			}
		}
	}
	
	//个人信息-添加地址i
	public function address_add(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(!empty($_POST)){
				$m = M('user_address');
				$_POST['user_id'] = $_COOKIE['userid'];
				$_POST['create_time'] = time();
				if(!empty($_POST['status'])){
					$_POST['status'] = 1;
				}else {
					//把默认地址全部设置为非默认
					$data['status'] = 0;
					$m->where('user_id='.$_COOKIE['userid'])->save($data);
				}
				if($m->add($_POST)){
					$this->redirect('Mycenter/address_list');
				}else{
					$this->error('添加失败');
				}
			}else{
				$this->display();
			}
		}
	}
	
	//个人信息-修改性别
	public function change_sex(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			if(IS_POST){
				$data['sex'] = $_POST['sexs'];
				$userid = $_COOKIE['userid'];
				$result = M('user')->where('id='.$userid)->save($data);
				if($result){
					$this->ajaxReturn(1);
				}else{
					$this->ajaxReturn(0);
				}
			}else{
				$userid = $_COOKIE['userid'];
				$resone = M('user')->where('id='.$userid)->find();
				$this->assign('data',$resone);
				$this->display();
			}
		}
	}
	
	//我的足迹列表
	public function person_footprint(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$uid = $_COOKIE['userid'];
			$info = M('foot_print');
			$result = $info->where('uid='.$uid)->order('create_time desc')->select();
			$this->assign('result',$result);
		
			$this->display();
		}
	}
	
	//点击足迹进入商品详细页
	public function footprint_goods(){
		if(IS_POST){
			$gid = $_POST['gid'];
			$resone = M('goods')->where('id='.$gid)->find();
			$this->ajaxReturn($resone['type']);
		}
	}
	
	//清空足迹
	public function empty_footer(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$uid = $_COOKIE['userid'];
			$result = M('foot_print')->where('uid='.$uid)->delete();
			$this->redirect('Mycenter/person_footprint');
		}
	}
	
	//删除单个足迹
	public function del_footer(){
		$id = I('post.id');
		$info = M('foot_print')->where('id='.$id)->delete();
		if(!$info){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn(1);
		}
	}

	//足迹商品添加到购物车
	public function foot_cart(){
		if(empty($_COOKIE['userid'])){
			//要回调的url
			$this->ajaxReturn(2);	//请先登录
		}else{
			$goods_id = I('post.goods_id');
			$where['user_id'] = $_COOKIE['userid'];
			$where['goods_id'] = $_POST['goods_id'];
			$goods = M('goods_cart')->where($where)->find();
			if(empty($goods)){
				$_POST['user_id'] = $_COOKIE['userid'];
				$_POST['create_time'] = time();
				$result = M('goods_cart')->add($_POST);
			}else{
				$_POST['user_id'] = $_COOKIE['userid'];
				$_POST['update_time'] = time();
				$_POST['goods_num'] = $goods['goods_num'] + $_POST['goods_num'];
				$result = M('goods_cart')->where('id='.$goods['id'])->save($_POST);
			}
			if($result){
				$this->ajaxReturn(1);   //成功
			}else{
				$this->ajaxReturn(0);	//失败
			}
		}
	}
	
	//我的收藏列表
	public function person_collect(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$goods_shoucang = M('goods_shoucang');
			$where['user_id'] = $_COOKIE['userid'];
			$result = $goods_shoucang->where($where)->order('id DESC')->select();
			$this->assign('result',$result);
		
			$this->display();
		}
	}
	
	//点击收藏进入商品详细页
	public function collect_goods(){
		if(IS_POST){
			$gid = $_POST['gid'];
			$resone = M('goods')->where('id='.$gid)->find();
			$this->ajaxReturn($resone['type']);
		}
	}
	
	//清空收藏
	public function empty_collect(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$uid = $_COOKIE['userid'];
			$result = M('goods_shoucang')->where('user_id='.$uid)->delete();
			$this->redirect('Mycenter/person_collect');
		}
	}
	
	//删除单个收藏
	public function del_collect(){
		$id = I('post.id');
		$info = M('goods_shoucang')->where('id='.$id)->delete();
		if(!$info){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn(1);
		}
	}
	

	//足迹商品添加到购物车
	public function collect_cart(){
		if(empty($_COOKIE['userid'])){
			//要回调的url
			$this->ajaxReturn(2);	//请先登录
		}else{
			$goods_id = I('post.goods_id');
			$where['user_id'] = $_COOKIE['userid'];
			$where['goods_id'] = $_POST['goods_id'];
			$goods = M('goods_cart')->where($where)->find();
			if(empty($goods)){
				$_POST['user_id'] = $_COOKIE['userid'];
				$_POST['create_time'] = time();
				$result = M('goods_cart')->add($_POST);
			}else{
				$_POST['user_id'] = $_COOKIE['userid'];
				$_POST['update_time'] = time();
				$_POST['goods_num'] = $goods['goods_num'] + $_POST['goods_num'];
				$result = M('goods_cart')->where('id='.$goods['id'])->save($_POST);
			}
			if($result){
				$this->ajaxReturn(1);   //成功
			}else{
				$this->ajaxReturn(0);	//失败
			}
		}
	}

	//我的优惠券列表
	public function person_coupon(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}
		$userid = $_COOKIE['userid'];
		$resone = M('user')->where('id='.$userid)->find();
		$list = M('user_coupons')->where('mobile='.$resone['mobile'])->select();
		$this->assign('list',$list);
		$this->display();
	}
	
	//清空优惠卷
	public function empty_coupon(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$userid = $_COOKIE['userid'];
			$resone = M('user')->where('id='.$userid)->find();
			$result = M('user_coupons')->where('mobile='.$resone['mobile'])->delete();
			$this->redirect('Mycenter/person_coupon');
		}
	}
	
	//删除单个优惠卷
	public function coupon(){
		$id = I('post.id');
		$info = M('user_coupons')->where('id='.$id)->delete();
		if(!$info){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn(1);
		}
	}
	
	//消息列表
	public function person_news(){
		$list = M('news')->select();
		$news['count'] = M('news')->count();
		$this->assign('news',$news);
		$this->assign('list',$list);
		$this->display();
	}
	
	//清空消息
	/* public function empty_coupon(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}else{
			$userid = $_COOKIE['userid'];
			$resone = M('user')->where('id='.$userid)->find();
			$result = M('user_coupons')->where('mobile='.$resone['mobile'])->delete();
			$this->redirect('Mycenter/person_coupon');
		}
	} */
	
	//删除单个优惠卷
	/* public function coupon(){
		$id = I('post.id');
		$info = M('user_coupons')->where('id='.$id)->delete();
		if(!$info){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn(1);
		}
	} */
	
	//消息内页
	public function person_newslist(){
		$id = $_GET['id'];
		$list = M('news')->where('id='.$id)->find();
		$this->assign('list',$list);
		$this->display();
	}
	
	//消息详细页
	public function person_newsdetail(){
		$id = $_GET['id'];
		$list = M('news')->where('id='.$id)->find();
		$this->assign('list',$list);
		$this->display();
	}
	/*
	 *会员资格充钱
	 */
	public function menb_push_money(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/reg');
		}
		$user_id = $_COOKIE['userid'];
		$hf_money = I('get.hf_money');
		$member=D('Myzzy');
		$my=$member->where(array('user_id'=>$user_id))->find();
		$pid=$my['pid'];
		$grade_name=$my['grade_name'];
		if($grade_name=="合伙人" || $grade_name=="会员"){
			$this->error("您已经拥有会员资格,无需再次购买!");
			exit;
		}
		if($member->where(array('id'=>$pid))->getField('grade_name')!="合伙人"){
			if($hf_money!=365){
				$this->error("对不起,您不能购买此类型的会员等级");
				exit;
			}
		}
		$orders_num = 'hui'.time().rand(111111,999999);
		$res = strpos($orders_num,'u');
		$user_huifei = M('User_huifei');
		$info = $user_huifei->add(array(
			'user_id'=>$user_id,
			'hf_money'=>$hf_money,
			'orders_num'=>$orders_num,
		));
		if($info){
			/*session('orders_num',$orders_num);
			$this->redirect('Mobile/Wxpay/new_pay');*/
			//判断是是微信浏览器还是其他浏览器
			$user_agent = $_SERVER['HTTP_USER_AGENT'];
			if (strpos($user_agent, 'MicroMessenger') === false) {
				// 非微信浏览器禁止浏览
				$this->redirect('Mobile/Tourism/mobile_pay_money',array('orders_num'=>$orders_num));

			} else {
				// 微信浏览器，允许访问
				session('orders_num',$orders_num);
				$this->redirect('Mobile/Wxpay/new_pay');
			}
			
		}

	}
	
	/*
	 * 银行卡列表
	 */
	public function person_bankcard(){
		$info = M('BankCard');
		$list = $info->where(array('user_id' => cookie('userid')))->select();
		$this->assign('list',$list);
		
		$this->display();
	}
	
	
	/*  
	 * 解除银行卡绑定
	 */
	public function bankcard_del(){
		$info = M('BankCard');
		$id = I('post.id');
		$result = $info->where(array('id'=>$id))->delete();
		if(!$result){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn(1);
		}
	}
	
	
	/*
	 * 绑定银行卡
	 */
	public function bankcard_add(){
		if(IS_POST){
			$info = M('BankCard');
			$data['realname'] = I('post.realname');
			$data['id_card'] = I('post.id_card');
			$data['type'] = I('post.type');
			$data['belong'] = I('post.belong');
			$data['card_num'] = I('post.card_num');
			$data['mobile'] = I('post.mobile');
			$data['create_time'] = time();
			$data['user_id'] = cookie('userid');
			$result = $info->where(array('card_num'=>$data[card_num]))->limit(1)->find();
			if($result){
				$this->ajaxReturn(2);
			}else{
				$result = $info->add($data);
				if(!$result){
					$this->ajaxReturn(0);
				}else{
					$this->ajaxReturn(1);
				}
			}
		}else{
			$this->display();
		}
	}
	
	
	/*
	 * 账户管理
	 */
	public function person_manage(){
		$info = M('BankCard');
		$count = $info->where(array('user_id' => cookie('userid')))->count();
		$info = M('User');
		$result = $info->field('account_balance,integral')->where(array('id'=>cookie('userid')))->find();
		$result['count'] = $count;
		$con = M('UserCoupons');
		$res = $con->where(array('mobile'=>cookie('phone')))->count();
	
		$result['yh_count'] = $res;
		$this->assign('result',$result);
	
		$this->display();
	}
	
	
	/* 
	 * 账户明细
	 *  */
	public function person_balance(){
		
		$info = M('Account');
		$list = $info->where(array('user_id'=> cookie('userid'),'pay_status'=>1))->select();
		$this->assign('list',$list);
		$result['account_balance'] = $_GET['account_balance'];
		$this->assign('result',$result);
		$this->display();
	}
	
	
	/*
	 * 积分明细
	 *  */
	public function person_integral(){
		/**
		$info = M('Integral');
		$list = $info->where(array('uid'=>cookie(userid)))->select();
		$this->assign('list',$list);
		$result['integral'] = $_GET['integral'];
		**/
		$user=M('user');
		$id=cookie('userid');
		$grade=M('member','vip_')->where(array('id'=>$id))->getfield('grade_name');
		$jf=$user->where(array('id'=>$id))->find();
		$this->assign('jfa',$jf['add_jf_limit']);
		$this->assign('jfb',$jf['add_jf_currency']);
		$this->assign('grade',$grade);
		$this->display();
	}


	/*
	 * 余额充值
	 */
	public function payment_balance(){
		if(empty($_COOKIE['userid'])){
			$this->redirect('User/login');
		}
		if(!empty($_POST)){
			//当u在第二个位置时位冲余额
			$orders_num = $orders_num = 'haui'.time().rand(111111,999999);
			$account = M('Account');
			$info = $account->add(array(
				'user_id'=>cookie('userid'),
				'quota'=>I('post.quota'),
				'type'=>1,
				'create_time'=>time(),
				'orders_num'=>$orders_num
			));
			if($info){
				/*session('orders_num',$orders_num);
				$this->redirect('Wxpay/new_pay');*/
				//判断是是微信浏览器还是其他浏览器
				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				if (strpos($user_agent, 'MicroMessenger') === false) {
					// 非微信浏览器禁止浏览
					$this->redirect('Mobile/Tourism/mobile_pay_money',array('orders_num'=>$orders_num));

				} else {
					// 微信浏览器，允许访问
					session('orders_num',$orders_num);
					$this->redirect('Mobile/Wxpay/new_pay');
				}
			}
		}else{
			$this->display();
		}
	}
//分享亿速网络
	public function person_share(){
		$id=session('id');
		if(!$id){
			$id=$_COOKIE['userid'];
		}
		if(!$id){
			$this->redirect('User/login');//判断是否登录   未登录跳转到登录页面
		}
		if(!file_exists("./Public/code/".$id.".png")) {
			$content = "http://" . $_SERVER['HTTP_HOST'] . "/index.php/Mobile/Myzzy/web/id/" . $id;
			vendor("phpqrcode.phpqrcode");
			$QRcode = new \QRcode();
			$QRcode::png($content, "./Public/code/" . $id . ".png", '', 6);
		}
		$this->assign('id',$id);
		$this->display();
	}


	public function person_identity(){

		$member = M('member','vip_');
		$where_m['id'] = $_COOKIE['user_id'];
		$res_member = $member->where($where_m)->find();
		$this->assign('res_member',$res_member);

		$this->display();
	}	

/*
	 *成为会员和事业合伙人页面
	 */

	public function become_sh_hui(){

		$member = M('member','vip_');
		$where_m['id'] = $_COOKIE['user_id'];
		$res_member = $member->where($where_m)->find();
		$this->assign('res_member',$res_member);

		$this->display();
	}


	/*
	 *成为会员
	 */
	public function become_hui(){
		$this->display();
	}

	/*
	 *成为合伙人
	 */
	public function become_hehuoren(){
		$this->display();
	}






}
