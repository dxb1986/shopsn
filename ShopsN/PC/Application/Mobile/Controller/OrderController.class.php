<?php
namespace Mobile\Controller;
use Think\Controller;

//前台订单模块
class OrderController extends Controller{
	/* 
	 * 我的订单 
	 */
	public function order_list(){
		$goods_orders = M('Goods_orders');
		$where['user_id'] = cookie('userid');
		$res = $goods_orders->order('id DESC')->where($where)->select();
		$goods_orders_record = M('Goods_orders_record');
		foreach($res as $k=>$v){
			$id = $v['id'];
			$result = $goods_orders_record->where('goods_orders_id='.$id)->select();
			$res[$k]['result'] = $result;
		}
		
		$this->assign('page',$show);// 赋值分页输出
		$this->assign('res',$res);
		
		$this->display();
	}
	

	//退货申请
	public function tuihuo(){
		if(!empty($_POST)){
			$m = M('goods_orders');
			$where['id'] = $_POST['orders_id'];
			$data['orders_status'] = 4;	//退货状态
			$data['tuihuo_time'] = time();
			$data['tuihuo_case'] = $_POST['tuihuo_case'];
			$result = $m->where($where)->save($data);
			if($result){
				$this->success('退货申请已提交');
			}else{
				$this->error('提交失败,系统异常');
			}
		}else{
			$m = M('goods_orders');
			$where['id'] = $_GET['orders_id'];
			$result = $m->where($where)->find();
			$this->assign('result',$result);

			$this->assign('orders_id',$_GET['orders_id']);
			$this->display();
		}
	}	



	//确认订单
	public function queren_shouhuo(){
		if(!empty($_POST)){
			$m = M('user');
			$where['id'] = $_COOKIE['user_id'];
			$result = $m->where($where)->find();
			if($result['password'] == md5($_POST['password'])){
				$m = M('Goods_orders');
				$where2['id'] = $_POST['orders_id'];
				$data['orders_status'] = 3;
				$data['shouhuo_time'] = time();
				$m->where($where2)->save($data);
				$this->success('确认收货成功',U('Order/order_list'));
			}else{
				$this->error('密码错误,确认失败');
			}
		}else{
			$m = M('goods_orders');
			$where['id'] = $_GET['orders_id'];
			$result = $m->where($where)->find();
			$this->assign('result',$result);
			$this->display();
		}
	}
	

	//评价
	public function pingjia(){
		if(!empty($_POST)){
			$m = M('goods_orders_record');
			foreach($_POST['record_id'] as $k=>$v){
				$data['pingjia_status'] = $_POST['pingjia_status'][$v];
				$data['pingjia_content'] = $_POST['pingjia_content'][$v];
				$data['pingjia_time'] = NOW_TIME;
				$where['id'] = $v;
				$m->where($where)->save($data);
			}
			$this->success('评论成功!');
		}else{
			$m = M('goods_orders_record');
			$where['goods_orders_id'] = $_GET['orders_id'];
			$where['user_id'] = $_COOKIE['user_id'];
			$result = $m->where($where)->select();
			//echo $m->getLastSql();
			//exit;
			$this->assign('result',$result);
			$this->display();
		}
	}

	/* 
	 * 取消订单
	 */
	public function cancel_order(){
		$id = I('post.id');
		$goods_orders = M('Goods_orders');
		$goods_orders_record = M('Goods_orders_record');
		$message = $goods_orders->where(array('id'=>$id))->find();//删除之前先保存到内存中才能获取到订单积分抵扣信息
		if($message['pay_status']==1){
			$this->ajaxReturn(0);
			exit;//已经支付的订单不允许取消
		}
		$info = $goods_orders->where('id='.$id)->delete();

		if($info){
			$info1 = $goods_orders_record->where('goods_orders_id='.$id)->delete();
			/************************释放用户积分*****************************/
			$user=M('user');
			if($message['use_jf_currency']>0){
				$user->where(array('id'=>$message['user_id']))->setInc('add_jf_currency',$message['use_jf_currency']);		        
			}
			if($message['use_jf_limit']>0){
				$user->where(array('id'=>$message['user_id']))->setInc('add_jf_limit',$message['use_jf_limit']);
			}
			if($message['use_jf_limit']+$message['use_jf_currency']>0){
				$user->where(array('id'=>$message['user_id']))->setInc('integral',$message['use_jf_limit']+$message['use_jf_currency']);
			}
			/*****************************************************/
			if(!$info1){
				$this->ajaxReturn(0);
			}else{
				$this->ajaxReturn(1);
			}
		}else{
			$this->ajaxReturn(0);
		}

	}
	
	/*
	 *订单 待付款
	 */
	public function payment_list(){
		$goods_orders = M('GoodsOrders');
		$where['orders_status'] = 0;
		$where['pay_status'] = 0;
		$where['user_id'] = cookie('userid');
		$count = $goods_orders->where($where)->count();
		$Page  = new \Think\Page($count,10);
		$show   = $Page->show();
		$res = $goods_orders->order('id DESC')->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
		$goods_orders_record = M('Goods_orders_record');
		foreach($res as $k=>$v){
			$id = $v['id'];
			$result = $goods_orders_record->where('goods_orders_id='.$id)->select();
			$res[$k]['result'] = $result;
		}
		//dump($res);
		$this->assign('page',$show);// 赋值分页输出
		$this->assign('res',$res);
		$this->display();
		
	}
	
	/* 
	 * 待收货 
	 */
	public  function  receipt_goods(){
		$goods_orders = M('GoodsOrders');
	
		$where = array();
		$where['orders_status'] = 2;
		$where['pay_status'] = 1;
		
		$where['user_id'] = cookie('userid');
		
		$count = $goods_orders->where($where)->count();
		$Page  = new \Think\Page($count,10);
		$show   = $Page->show();
		$res = $goods_orders->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->where($where)->select();
		//dump($res);
		$goods_orders_record = M('Goods_orders_record');
		foreach($res as $k=>$v){
			$id = $v['id'];
			$result = $goods_orders_record->where('goods_orders_id='.$id)->select();
			$res[$k]['result'] = $result;
		}
		
		$this->assign('page',$show);// 赋值分页输出
		$this->assign('res',$res);
		$this->display();
	}
	
	
	/* 
	 * 待评价
	 */
	public function payments_waite(){
		$goods_orders = M('Goods_orders');
		$where = array();
		
		$where['pay_status'] = 1;
		$where['orders_status'] = 3;
		//$where['pingjia_time'] = NULL;
		$where['user_id'] = cookie('userid');
		$count = $goods_orders->where($where)->count();
		$Page  = new \Think\Page($count,10);
		$show   = $Page->show();
		$res = $goods_orders->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->where($where)->select();
	
		$goods_orders_record = M('Goods_orders_record');
		foreach($res as $k=>$v){
			$id = $v['id'];
			$result = $goods_orders_record->where('goods_orders_id='.$id)->select();
			$res[$k]['result'] = $result;
		}
		$this->assign('page',$show);// 赋值分页输出
		$this->assign('res',$res);
		$this->display();
		
	}
	public function test(){
		dump(session());
		dump(cookie());
	}
	public function order_end(){
		if(!session("user_id") && !cookie('userid')){
			$this->redirect('User/login');
			exit;
		}
		$goods_orders = M('goods_orders');
		$user_id=cookie('userid');
		if(IS_AJAX){
			$orders_num=I('post.orders_num');
			$is_use=$goods_orders->where(array('orders_num'=>$orders_num,'is_used'=>1))->count();
			if($is_use){
				$this->ajaxReturn(0);
				exit;
			}
			$map['orders_status']=5;
			$map['fanli_action']=$user_id.'触发，时间：'.date("Y-m-d H:m:s",NOW_TIME);
			$map['shouhuo_time']=NOW_TIME;
			$order_id=$goods_orders->where(array('orders_num'=>$orders_num))->getField('id');
			$rst=$goods_orders->where(array('orders_num'=>$orders_num))->save($map);
			//$rst=1;
			if($rst){
				R("Home/Shop/buy",array($order_id));
				$this->ajaxReturn(1);
				exit;
			}else{
				$this->ajaxReturn(0);
				exit;
			}
			
		}else{
			$count = $goods_orders->where(array('pay_status'=>1,'user_id'=>$user_id,'fahuo_time'=>array('gt',1462032000),'is_used'=>0))->count();
			$Page  = new \Think\Page($count,10);
			$show   = $Page->show();
			$res = $goods_orders->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->where(array('pay_status'=>1,'user_id'=>$user_id,'fahuo_time'=>array('gt',1462032000),'is_used'=>0))->select();
		
			$goods_orders_record = M('Goods_orders_record');
			foreach($res as $k=>$v){
				$id = $v['id'];
				$result = $goods_orders_record->where('goods_orders_id='.$id)->select();
				$res[$k]['result'] = $result;
			}
			$this->assign('page',$show);// 赋值分页输出
			//dump($res);exit;
			$this->assign('res',$res);
			$this->display();
		}
		
		
	}
	//评价
	/* public function pingjia(){
		if(!empty($_POST)){
			$m = M('goods_orders_record');
			foreach($_POST['record_id'] as $k=>$v){
				$data['pingjia_status'] = $_POST['pingjia_status'][$v];
				$data['pingjia_content'] = $_POST['pingjia_content'][$v];
				$data['pingjia_time'] = time();
				$where['id'] = $v;
				$m->where($where)->save($data);
			}
			$this->success('评论成功');
		}else{
			$m = M('goods_orders_record');
			$where['goods_orders_id'] = $_GET['orders_id'];
			$where['user_id'] = $_SESSION['user_id'];
			$result = $m->where($where)->select();
			$this->assign('result',$result);
			$this->display();
		}
	} */
}