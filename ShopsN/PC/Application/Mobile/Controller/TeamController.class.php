<?php
/**
 * Created by PhpStorm.
 * User: jiangqingfeng
 * Date: 2016/7/10
 * Time: 22:22
 */

namespace Mobile\Controller;


use Think\Controller;

class TeamController extends Controller
{
    public function _initialize()
    {
        $this->usermodel=M('user');
        $this->membermodel = M('member');
        $this->huifeimodel = M('user_huifei');
        $this->profitmodel=M('member_profit');
        $this->proportionmodel=M('member_proportion');
    }

    //团队会员详情
    public function team_details($id=null){
        if($id){
            //我的会员id
            //$id;
            //事业合伙人总人数
            $data=$this->membermodel->where(array('id'=>$id))->find();
            $wheres['path']=array('like',$data['path'].'%');
            $rows=$this->membermodel->where($wheres)->select();
            $zonghehuoren=0;      //下级合伙人总人数
            foreach($rows as $row){
                if($row['path']!=$data['path']&&$row['grade_name']!='会员'){
                    ++$zonghehuoren;
                }
            }
            foreach($rows as &$result){
                $result['path']=str_ireplace($data['path'],'',$result['path']);     //将下家所有的path字段中本身的path进行替换
                if(substr_count( $result['path'],'-')!=0&&$result['grade_name']!='会员'){
                    if(substr_count( $result['path'],'-')==1){
                        $firsts[]=$result;       //下一级所有合伙人
                    }elseif(substr_count( $result['path'],'-')==2){
                        $seconds[]=$result;      //下2级所有合伙人
                    }elseif(substr_count( $result['path'],'-')==3){
                        $thirds[]=$result;        //下三级所有合伙人
                    }else{
                        $others[]=$result;
                    }
                }
            }
            foreach($firsts as &$first){
                $first['hehuoren']=$this->jisuan($first['id']);
            }
            foreach($seconds as &$first){
                $second['hehuoren']=$this->jisuan($first['id']);
            }
            foreach($thirds as &$first){
                $third['hehuoren']=$this->jisuan($first['id']);
            }
            $this->assign('data',$data);            //用户Id
            $this->assign('zonghehuoren',$zonghehuoren);            //合伙人总人数
            $this->assign('renshu_1',count($firsts));                  //一级合伙人人数
            $this->assign('renshu_2',count($seconds));
            $this->assign('renshu_3',count($thirds));
            $this->assign('firsts',$firsts);                            //一级合伙人信息
            $this->assign('seconds',$seconds);
            $this->assign('thirds',$thirds);
            //我的推荐人
            $tuijianren=$this->membermodel->where(array('id'=>$data['pid']))->find();   //推荐人相关系信息

            $this->assign('tuijianren',$tuijianren);
        }else{
            $this->error('Sorry,没有查询到相关信息');
        }


        $this->display();
    }

    public function tuan_zan($id=null){
        if($id){
            $data=$this->membermodel->where(array('id'=>$id))->find();
            $wheres['path']=array('like',$data['path'].'%');
            $rows=$this->membermodel->where($wheres)->select();
            $zonghehuoren=0;      //下级合伙人总人数
            $zonghuiyuan=0;
            foreach($rows as $row){
                if($row['path']!=$data['path']&&$row['grade_name']!='会员'){
                    ++$zonghehuoren;
                }else{
                    ++$zonghuiyuan;
                }
            }
            foreach($rows as &$result){
                $result['path']=str_ireplace($data['path'],'',$result['path']);     //将下家所有的path字段中本身的path进行替换
                if(substr_count( $result['path'],'-')!=0&&$result['grade_name']!='会员'){
                    if(substr_count( $result['path'],'-')==1){
                        $firsts[]=$result;       //下一级所有合伙人
                    }elseif(substr_count( $result['path'],'-')==2){
                        $seconds[]=$result;      //下2级所有合伙人
                    }elseif(substr_count( $result['path'],'-')==3){
                        $thirds[]=$result;        //下三级所有合伙人
                    }else{
                        $others[]=$result;
                    }
                }
            }
        }
        $data['hehuoren']=$this->jisuan($data['id']);
        $this->assign('data',$data);
        $this->assign('zonghehuoren',$zonghehuoren);
        $this->assign('zonghuiyuan',$zonghuiyuan);
        $this->assign('hehuoren1',count($firsts));
        $this->assign('hehuoren2',count($seconds));
        $this->assign('hehuoren3',count($thirds));
        $this->assign('other',count($others));
        $this->display();
    }
    //计算下家
    public function jisuan($id){
        $data=$this->membermodel->where(array('id'=>$id))->find();
        $wheres['path']=array('like',$data['path'].'%');
        $rows=$this->membermodel->where($wheres)->select();
        $zonghehuoren=0;      //下级合伙人总人数
        foreach($rows as $row){
            if($row['path']!=$data['path']&&$row['grade_name']!='会员'){
                ++$zonghehuoren;
            }
        }
        return $zonghehuoren;
    }
}