/**
 * 弹出层js
 */
(function(){
	
	function newAlert(){}
	
	newAlert.prototype = 
	{
		//弹窗高度
		height : 0,
		//弹窗宽度
		width  : 0,
		type   : 'post',
		//回调数据类型
		dataType : 'json',
	    /**
	     * 是否是数字
	     * @param number 要检测的值
	     */
		isNumer   : function(number) {
			return isNaN(number) ? false : true;
		},
		/**
		 * 弹窗页面 
		 */
		alertEdit : function(url, title, width, height)
		{
			width = typeof width === 'undefined' ? 500 : width;
			
			height = typeof height === 'undefined' ? 400 : height;
			
			console.log(width+"==="+height);
			
			if(!url)
			{
				layer.msg('来自网页的消息,未知错误');
				return false;
			}
			title = title ? title : '添加关键词';
			parent.layer.open({
				type: 2,
				shadeClose: true,
				shade: 0.5,
				area: [width+'px', height+'px'],
				title: title,
				content: url,
				name   : name,
			});
		},
		
		
		submit : function(event, url)
		{
			var data = this.getForm(event);
			if(!data)
			{
				alert('数据有误');
				return false;
			}
			
			this.ajax(url, data);
		},
		
		//获取form数据
		getForm :function(event)
		{
			var formData = {};
			
			$('select').each(function() {
				if( typeof $(this).attr('name') != 'undefined' && !$(this).attr('disabled')) {
					
					formData[$(this).attr('name')] = $(this).find('option:selected').val();
				}
			});
			
			$('input').each(function(){
				formData[$(this).attr('name')] = $(this).val();
			});
			var flag = 0;
			for(var i in formData)
			{
				if(!formData[i])
				{
					flag++;
				}
			}
			return flag === 0 ? formData : false;
		},
		
		/**
		 * @param string url  
		 */
		ajax : function(url , data, callBack)
		{
			var type = this.type;
			$.ajax({
				url  		: url,
				type 		: type,
				data		: data,
				dataType	: 'json',
				success		: function(res)
				{
					if(res.status)
					{
						return typeof callBack ==='undefined' ? Tool.closeWindow() : callBack(res);
					}
					else
					{
						layer.msg(res.message);
						return false;
					}
				}
			})
		},
		
		/**
		 * 关闭窗口 
		 */
		closeWindow : function()
		{
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			index ? parent.layer.close(index) : false;
			return window.parent.iframe.location.reload();
		},
		
		
		/**
		 * 编辑
		 * @param int id	   属性编号
		 * @param string url   网址连接
		 * @param string title 页面标题 
		 */
		editData : function(id, url, title)
		{
			if(!this.isNumer(id) || !title)
			{
				layer.msg("参数错误");
				return false;
			}
			return this.alertEdit(url, title);
		},
		
		/**
		 * 删除属性 
		 */
		deleteData : function(id, url) {
			if(!this.isNumer(id))
			{
				layer.msg("参数错误");
				return false;
			}
			return this.ajax(url, {id : id});
		},
		/**
		 * 属性赋值 
		 */
		inputValue : function(id, value)
		{
			return document.getElementById(id).value = value;
		},
		
		uploadify :function (url)
		{	
			
			var iframe_str='<iframe frameborder="0" name="uploadframe" ';
			iframe_str=iframe_str+'id=uploadify ';   		
			iframe_str=iframe_str+' src='+url;
			iframe_str=iframe_str+' allowtransparency="true" class="uploadframe" scrolling="no"> ';
			iframe_str=iframe_str+'</iframe>';    	    		
			$("body").append(iframe_str);	
			$("iframe.uploadframe").css("height",$(document).height()).css("width","600px").css("position","absolute").css("left","200px").css("top","0px").css("z-index","999999").show();
			$(window).resize(function(){
				$("iframe.uploadframe").css("height",$(document).height()).show();
			});
		},
		/**
		 * 检测url的合法性 
		 */
		checkURL : function (URL) {
			var str=URL;
			//判断URL地址的正则表达式为:http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?
			//下面的代码中应用了转义字符"\"输出一个字符"/"
			var Expression=/http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
			var objExp=new RegExp(Expression);
			
			return (objExp.test(str)==true) ? true : false;
		},
		
		post : function(url, data, callBack) {
			var fun = callBack === 'undefined' ? this.parseResponse : callBack;
			if(/*!this.checkURL(url)||*/ !data) {
				layer.msg('数据为空');
				return false;
			}
			return $.post(url, data, fun, this.dataType);
		},
		
		/**
		 * 解析商品分类 
		 */
		parseResponse : function(res) {
			console.log(res);
			if(!res.hasOwnProperty('data') && res.data == null ) {
				return false;
			}
			var arr = res.data;
			var optionBuild = "<option>请选择商品分类</option>";
			for(var i in arr) {
				
				optionBuild += '<option value="'+ arr[i].id+'">'+arr[i].class_name+'</option>';
			}
			
            return $('#parent_id_2').empty().html(optionBuild);
		},
		
		/**
		 * 分类选择 
		 */
		change : function (event,url) {
			var req = {};
			var selectId = event.value;
			req[event.getAttribute('name')] = selectId;
			
			if(!this.isNumer(selectId))
			{
				layer.msg('参数错误');
				return false;
			}
			return this.post(url, req);
		}
	}
	window.Tool = new newAlert();
	return window.Tool;
})(window);