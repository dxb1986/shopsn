/**
 * 
 */
$(function(){
		$('#home_slider').flexslider({
			animation : 'slide',
			controlNav : true,
			directionNav : true,
			animationLoop : true,
			slideshow : false,
			useCSS : false
		});
		
		
		var pic = $('#goods_pic').val();
	    var gid = $('#goods_id').val();
	    var price = $('#price_new').val();

	    var name = $('#goods_title').val();
	    var type = $('#goods_type').val(); 
	    $.ajax({
	        type: "POST",
	        url: SPX,
	        data: "gid="+gid+"&goods_pic="+pic+"&goods_price="+price+"&goods_name="+name+"&type="+type,
	        success: function(msg){
	         	console.log(msg);
	         }
	    });
		
	});
	
	//添加到购物车
    function add_cart(id){
    	var goods_pic = $('#goods_pic').val();
    	var price_new = $('#price_new').val();
	    var goods_id = $('#goods_id').val();
	    var goods_title = $('#goods_title').val();
	    var goods_type = $('#goods_type').val();
	    var taocan_name = $('#taocan_name').val();
	    var fanli_jifen = $('#fanli_jifen').val();
      	var num = 1;
        $.post(JC, {goods_id:goods_id,goods_title:goods_title,pic_url:goods_pic,goods_num:num,goods_type:goods_type,taocan_name:taocan_name,fanli_jifen:fanli_jifen,price_new:price_new },function(data){
            if(data == 1){
                layer.alert('成功加入购物车',6);
            }else if(data == 2){
            	layer.alert('请先登录');
                window.location.href="{:U('User/login')}";
            }else{
            	layer.alert('加入购物车失败');
            }
        },"json");
    }
    function select_taocan(id){
    	  $('.taocan_span').attr("style","border: 2px solid #E3E3E3;margin-right: 10px; display: block;float: left;padding: 3px 10px;");
    	  $('#taocan_span'+id).attr("style","border:2px solid #BE0106;"); 
    	  var guige_title = $('#guige_title'+id).val();
    	  var guige_price_new = $('#guige_price_new'+id).val();
    	  var guige_zhongliang = $('#guige_zhongliang'+id).val();

    	  $('#taocan_price').html(guige_price_new);
    	  $('#price_new').val(guige_price_new);

    	  $('#price_sum').val(guige_price_new);
    	  $('#taocan_name').val(guige_title);

    	}