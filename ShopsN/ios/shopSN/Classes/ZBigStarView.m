//
//  ZBigStarView.m
//  shopSN
//
//  Created by yisu on 16/8/3.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZBigStarView.h"

@interface ZBigStarView ()

@property(nonatomic,strong)NSMutableArray *starIVs;

@end

@implementation ZBigStarView

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        _starIVs=[NSMutableArray array];
        
        
        for (NSInteger i=0; i<5; i++) {
            
            _starIV=[[UIImageView alloc]initWithFrame:CGRectMake(20*i,0, 15, 15)];
            
            _starIV.contentMode = UIViewContentModeScaleAspectFit;
            //_starIV.image=MImage(@"xx_h.png");
            _starIV.image = _starImage;
            _starIV.tag=i;
            [self addSubview:_starIV];
            [_starIVs addObject:_starIV];
        }
    }
    return self;
}
-(void)setStar:(NSInteger)star{
    for (UIImageView *starIV in _starIVs) {
        if (starIV.tag<star) {
            starIV.image=MImage(@"xx_l.png");
        }else{
            starIV.image=MImage(@"xx_h.png");
        }
    }
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
