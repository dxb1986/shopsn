//
//  ZCAccountBundleBankCardViewController.m
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCAccountBundleBankCardViewController.h"


@interface ZCAccountBundleBankCardViewController ()<UITextFieldDelegate>
{
    UITableView *_tableView;
    UIView *mainView;
    UITextField *_currentTextField;
}


/** 标题数组 */
@property (nonatomic, strong) NSArray *titleArray;

/** 内容数组 */
@property (nonatomic, strong) NSArray *detailPlaceholdArray;

/** 填写的信息数组 */
@property (nonatomic, strong) NSMutableArray *detailInfoArray;


@end

@implementation ZCAccountBundleBankCardViewController
#pragma mark - ==== 懒加载 =====

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [NSArray array];
        //开户姓名 身份证号 卡类型 所属银行 银行卡号 预留手机
        _titleArray = @[@"开户姓名", @"身份证号", @"卡类型", @"所属银行", @"银行卡号", @"预留手机"];
    }
    return _titleArray;
}

- (NSArray *)detailPlaceholdArray {
    if (!_detailPlaceholdArray) {
        _detailPlaceholdArray = [NSArray array];
        //请输入持卡人姓名 请输入持卡人身份证号 储蓄卡 请输入银行卡所属银行
        //请输入银行卡卡号	请输入银行卡预留手机号
        _detailPlaceholdArray = @[@"请输入持卡人姓名", @"请输入持卡人身份证号", @"储蓄卡", @"请输入银行卡所属银行", @"请输入银行卡卡号", @"请输入银行卡预留手机号"];
    }
    return _detailPlaceholdArray;
}

- (NSMutableArray *)detailInfoArray {
    if (!_detailInfoArray) {
        _detailInfoArray = [@[@"",@"",@"",@"",@"",@""] mutableCopy];
    }
    return _detailInfoArray;
}

#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"绑定银行卡";
    self.view.backgroundColor = __AccountBGColor;
    //视图 初始化
    [self initViews];
    
}

//视图 初始化
- (void)initViews {
    
    //mainView
    mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 240)];
    mainView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:mainView];
    
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(registeFirstRespond:)];
//    [mainView addGestureRecognizer:tap];
    
    for (int i=0; i<self.titleArray.count; i++) {
        //1 subView
        UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 40*i, __kWidth, 40)];
        [mainView addSubview:subView];
        
        //title
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, 39)];
        [subView addSubview:title];
//        title.backgroundColor = __TestOColor;
        title.font = MFont(15);
        title.textColor = HEXCOLOR(0x333333);
        title.text = self.titleArray[i];
        
        //detail
        if (i == 2) {
            UILabel *detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(title), 0, CGRectW(subView)-CGRectW(title)-20, 39)];
            [subView addSubview:detailLb];
//            detailLb.backgroundColor = __TestOColor;
            detailLb.font = MFont(15);
            detailLb.text = @"";
        }
        UITextField *detailTF = [[UITextField alloc]initWithFrame:CGRectMake(CGRectXW(title), 0, CGRectW(subView)-CGRectW(title)-20, 39)];
        [subView addSubview:detailTF];
        detailTF.font = MFont(15);
        detailTF.placeholder = self.detailPlaceholdArray[i];
        detailTF.tag = 110+i;
        detailTF.delegate = self;
        
        //lineIV
        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(subView)-1, CGRectW(subView), 1)];
        [subView addSubview:lineIV];
        lineIV.backgroundColor = HEXCOLOR(0xf0f0f0);
        
    }
    
    //绑定按钮 color(215 30 33) (0xd71e21)
    UIButton *bundleBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectYH(mainView)+30, __kWidth-20, 40)];
    [self.view addSubview:bundleBtn];
    bundleBtn.backgroundColor = HEXCOLOR(0xd71e21);
    bundleBtn.layer.cornerRadius = 3.0f;
    bundleBtn.titleLabel.font = MFont(15);
    [bundleBtn setTitle:@"绑定银行卡" forState:BtnNormal];
    [bundleBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [bundleBtn addTarget:self action:@selector(bundleBtnAction) forControlEvents:BtnTouchUpInside];
    
   
    
}
#pragma mark - ==== 按钮触发方法 =====
- (void)bundleBtnAction {
    [self textFieldDidEndEditing:_currentTextField];
    NSLog(@"绑定银行卡");
    NSLog(@"%@", self.detailInfoArray);

    WK(weakSelf)
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid],
                            @"realname":self.detailInfoArray[0],
                            @"id_card":self.detailInfoArray[1],
                            @"type":self.detailInfoArray[2],
                            @"belong":self.detailInfoArray[3],
                            @"card_num":self.detailInfoArray[4],
                            @"mobile":self.detailInfoArray[5]};
    
    [ZHttpRequestService POST:@"User/bankcardAdd" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSString *message = jsonDic[@"message"];
            [SXLoadingView showAlertHUD:message duration:1];

            NSLog(@"jsondic---%@",jsonDic[@"data"]);
            [weakSelf.navigationController popViewControllerAnimated:YES];
            
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:0.5];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:0.5];
        
    } animated:true withView:nil];
}

#pragma mark - 使用积分 textField Delegate
//键盘弹出时 监听键盘高度
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _currentTextField = textField;
    if (textField.tag-110 == 4 || textField.tag-110 == 5) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardDidHideNotification object:nil];
    }
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSInteger index = textField.tag -110;
    self.detailInfoArray[index] = textField.text;
    
    
//    switch (index) {
//        case 0:
//        {
//            NSLog(@"1:%@",textField.text);
//            _detailInfoArray[index] = textField;
//        }
//            break;
//        case 1:
//        {
//            NSLog(@"2:%@===",textField.text);
//        }
//            break;
//        case 2:
//        {
//            NSLog(@"3:%@===",textField.text);
//        }
//            break;
//        case 3:
//        {
//            NSLog(@"4:%@===",textField.text);
//        }
//            break;
//        case 4:
//        {
//            NSLog(@"5:%@===",textField.text);
//        }
//            break;
//        case 5:
//        {
//            NSLog(@"6:%@===",textField.text);
//        }
//            break;
//        default:
//            break;
//    }
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}


//弹出键盘
- (void)keyboardShow:(NSNotification *)notification {
    //1 获取当前最大Y值
    CGFloat maxY = CGRectGetMaxY(mainView.frame) + 45;
    //2 获取键盘Y值
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey]CGRectValue];
    CGFloat keybordY = keyboardFrame.origin.y;
    //    NSLog(@"当前Y:%lf===键盘Y:%lf",maxY,keybordY);
    //3 计算距离
    CGFloat y = keybordY - maxY;
    if (y < 0) {
        [UIView animateWithDuration:0.25 animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, y);
        }];
    }
    
}

//键盘消失
- (void)keyboardHidden:(NSNotification *)notification {
    [UIView animateWithDuration:0.1 animations:^{
        self.view.transform = CGAffineTransformIdentity;
    }];
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
