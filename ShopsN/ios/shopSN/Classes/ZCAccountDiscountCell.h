//
//  ZCAccountDiscountCell.h
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 账户管理 子页面
 *
 *   优惠券页面 cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZCAccountDiscountCell : BaseTableViewCell

/** 优惠券 金额label */
@property (nonatomic, strong) UILabel *ca_discountMoneyLb;

/** 优惠券 使用范围label */
@property (nonatomic, strong) UILabel *ca_discountReference;

/** 优惠券 有效期label */
@property (nonatomic, strong) UILabel *ca_discountTime;


@end
