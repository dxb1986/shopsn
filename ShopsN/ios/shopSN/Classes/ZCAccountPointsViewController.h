//
//  ZCAccountPointsViewController.h
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 账户管理 子页面
 *
 *   积分 视图控制器
 *
 */
#import "ZBaseViewController.h"

@interface ZCAccountPointsViewController : ZBaseViewController

/** 我的积分 */
@property (nonatomic, copy) NSString *myPointsStr;

@end
