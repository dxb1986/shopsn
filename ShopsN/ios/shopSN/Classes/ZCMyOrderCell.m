//
//  ZCMyOrderCell.m
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMyOrderCell.h"

@implementation ZCMyOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = HEXCOLOR(0xf0f0f0);
        
        [self initSubViews];
        
    }
    return self;
}

- (void)initSubViews {
    //1 背景
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 60)];
    [self addSubview:bgView];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    
    //2 商品图片

    _goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
    [bgView addSubview:_goodsIV];
    _goodsIV.layer.borderWidth = 1.0f;
    _goodsIV.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    _goodsIV.contentMode = UIViewContentModeScaleAspectFill;
    _goodsIV.image = MImage(@"pic12");
    
    
    //3 商品描述 w 160
    // COSME DECORTE 黛珂 AQ珍萃精颜 悦活洁肤霜 150克
    _descLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_goodsIV)+5, 10, 160, 35)];
    //    _wp_descLb .backgroundColor = __TestGColor;
    [bgView addSubview:_descLb];
    _descLb.numberOfLines = 0;
    _descLb.font = MFont(11);
    _descLb.textColor = HEXCOLOR(0x333333);
    _descLb.text = @"COSME DECORTE 黛珂 AQ珍萃精颜 悦活洁肤霜 150克";
    
    
    //4 商品现价
    _nowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10-90, 10, 90, 20)];
    [bgView addSubview:_nowPriceLb];
    //    _wp_nowPriceLb.backgroundColor = __TestOColor;
    _nowPriceLb.font = MFont(11);
    _nowPriceLb.textColor = HEXCOLOR(0x333333);
    _nowPriceLb.textAlignment = NSTextAlignmentRight;
    _nowPriceLb.text = @"￥589";
    
    //5 商品原价
//    _originalPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10-50, CGRectYH(_nowPriceLb), 50, 15)];
//    [bgView addSubview:_originalPriceLb];
//    _originalPriceLb.font = MFont(11);
//    _originalPriceLb.textColor = HEXCOLOR(0x999999);
//    _originalPriceLb.textAlignment = NSTextAlignmentRight;
//    _originalPriceLb.text = @"889";
//    [self setLabeltextAttributes:_originalPriceLb.text];
    
    
    //6 商品数量
    _countLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-15-50, CGRectYH(_nowPriceLb), 50, 15)];
    [bgView addSubview:_countLb];
    _countLb.font = MFont(10);
    _countLb.textColor = HEXCOLOR(0x999999);
    _countLb.textAlignment = NSTextAlignmentRight;
    _countLb.text = @"x1";
    
    //7 边线line2
    UIImageView *lineIV2 = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(bgView)-1, CGRectW(bgView)-10, 1)];
    [bgView addSubview:lineIV2];
    lineIV2.backgroundColor = HEXCOLOR(0xdedede);
    
    
    
}

- (void)setLabeltextAttributes:(NSString *)text {
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //设置删除线
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
    
    [_originalPriceLb setAttributedText:attri];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
