//
//  ZCMyOrderHeaderView.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 我的订单页面
 *
 *  header
 *
 */
#import "BaseView.h"

@interface ZCMyOrderHeaderView : BaseView

/** 待付款订单 订单日期 */
@property (nonatomic, strong) UILabel *orderDateLb;

/** 待付款订单 订单类型 */
@property (nonatomic, strong) UILabel *orderTypeLb;


@end
