//
//  ZCMyOrderSuccessPayFooter.m
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMyOrderSuccessPayFooter.h"

@implementation ZCMyOrderSuccessPayFooter

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        _sp_shopCount = 1;
        
        
        //1 背景
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:bgView];
        
        
        //2 小计 价格 color (254 65 0) (fe4100)
        _sp_settleNowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-125, 10, 120, 15)];
        [bgView addSubview:_sp_settleNowPriceLb];
        //    _wp_settleNowPriceLb.backgroundColor = __TestGColor;
        _sp_settleNowPriceLb.font = MFont(11);
        _sp_settleNowPriceLb.textColor = __MoneyColor;
        _sp_settleNowPriceLb.textAlignment = NSTextAlignmentRight;
        _sp_settleNowPriceLb.text = @"应付金额:￥278.00";
        [self setSettlePriceLabeltextAttributes:_sp_settleNowPriceLb.text];
        
        //12 小计 商品件数
        _sp_settleGoodsCountLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_sp_settleNowPriceLb)-40, 10, 40, 15)];
        [bgView addSubview:_sp_settleGoodsCountLb];
        //_wp_settleGoodsCountLb.backgroundColor = __TestOColor;
        _sp_settleGoodsCountLb.font = MFont(11);
        _sp_settleGoodsCountLb.textColor = HEXCOLOR(0x333333);
        _sp_settleGoodsCountLb.textAlignment = NSTextAlignmentRight;
        _sp_settleGoodsCountLb.text = [NSString stringWithFormat:@"共%d件",_sp_shopCount];
        
        //13 按钮
        //评价按钮
        _sp_judgeBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10-50, CGRectYH(_sp_settleNowPriceLb)+10, 50, 25)];
        [bgView addSubview:_sp_judgeBtn];
        _sp_judgeBtn.titleLabel.font = MFont(11);
        _sp_judgeBtn.backgroundColor = __DefaultColor;
        _sp_judgeBtn.layer.cornerRadius = 3.0f;
        [_sp_judgeBtn setTitle:@"评价" forState:BtnNormal];
        [_sp_judgeBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        
        //删除订单按钮 边框color(169 169 169) (0xa9a9a9)
        _sp_deleteOrderBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10*2-75*1-50, CGRectYH(_sp_settleNowPriceLb)+10, 75, 25)];
        [bgView addSubview:_sp_deleteOrderBtn];
        _sp_deleteOrderBtn.titleLabel.font = MFont(11);
        //    _sp_deleteOrderBtn.backgroundColor = __TestOColor;
        _sp_deleteOrderBtn.layer.borderWidth = 1.0f;
        _sp_deleteOrderBtn.layer.borderColor = HEXCOLOR(0xa9a9a9).CGColor;
        _sp_deleteOrderBtn.layer.cornerRadius = 3.0f;
        [_sp_deleteOrderBtn setTitle:@"删除订单" forState:BtnNormal];
        [_sp_deleteOrderBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        
        
        //查看物流按钮 边框color(169 169 169) (0xa9a9a9)
        _sp_logistSituationBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10*3-75*2-50, CGRectYH(_sp_settleNowPriceLb)+10, 75, 25)];
        [bgView addSubview:_sp_logistSituationBtn];
        _sp_logistSituationBtn.titleLabel.font = MFont(11);
        //    _sp_deleteOrderBtn.backgroundColor = __TestOColor;
        _sp_logistSituationBtn.layer.borderWidth = 1.0f;
        _sp_logistSituationBtn.layer.borderColor = HEXCOLOR(0xa9a9a9).CGColor;
        _sp_logistSituationBtn.layer.cornerRadius = 3.0f;
        [_sp_logistSituationBtn setTitle:@"查看物流" forState:BtnNormal];
        [_sp_logistSituationBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        _sp_logistSituationBtn.hidden = YES;
    }
    return self;
    
}

- (void)setSettlePriceLabeltextAttributes:(NSString *)text {
    //NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //修改前5为颜色
    [attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 5)];
    
    [_sp_settleNowPriceLb setAttributedText:attri];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
