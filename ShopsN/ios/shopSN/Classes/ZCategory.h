//
//  ZCategory.h
//  shopSN
//
//  Created by yisu on 16/9/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 分类页面
 *
 *  一级分类模型
 *
 */

#import <Foundation/Foundation.h>

@interface ZCategory : NSObject

/** 一级分类 id */
@property (copy, nonatomic) NSString *categoryID;

/** 一级分类 名称 */
@property (copy, nonatomic) NSString *categoryName;

/** 二级分类 数组*/
@property (nonatomic, strong) NSMutableArray *subCategory;

@end
