//
//  ZCategoryMainCell.h
//  亿速
//
//  Created by chang on 16/6/23.
//  Copyright © 2016年 wshan. All rights reserved.
//

/* 提供 分类页面 中间分类视图
 *
 *  左侧 主分类 cell
 *
 */

#import "BaseTableViewCell.h"

@interface ZCategoryMainCell : BaseTableViewCell


/** 主分类 背景视图 */
@property (nonatomic, strong) UIView *categoryMainCellBGView;

/** 主分类 名称 */
@property (nonatomic, strong) UILabel *categoryMainCellTitleLb;

/** 主分类 左侧IV */
@property (nonatomic, strong) UIImageView *categoryMainCellSideIV;


@end
