//
//  ZCenterAccountCell.m
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterAccountCell.h"

@implementation ZCenterAccountCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    
    return self;
}




- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 44)];
    [self addSubview:bgView];
    //bgView.backgroundColor = [UIColor orangeColor];
    
    //1 图片
    _ca_iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
    [bgView addSubview:_ca_iconIV];
//    _ca_iconIV.backgroundColor = __DefaultColor;//等待切图图片
    _ca_iconIV.contentMode = UIViewContentModeScaleAspectFit;
    
    //2 title
    _ca_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_ca_iconIV)+5, 14, 100, 15)];
    [bgView addSubview:_ca_titleLb];
    //    _cs_titleLb.backgroundColor = __TestOColor;
    _ca_titleLb.font = MFont(12);
    _ca_titleLb.textColor = HEXCOLOR(0x999999);
    
    
    //3 detail
    _ca_detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-100-30, 14, 100, 15)];
    [bgView addSubview:_ca_detailLb];
    //    _cs_detailLb.backgroundColor = __TestOColor;
    _ca_detailLb.font = MFont(12);
    _ca_detailLb.textColor = HEXCOLOR(0x999999);
    _ca_detailLb.textAlignment = NSTextAlignmentRight;
    
    
    //4 line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(bgView)-1, CGRectW(bgView)-10, 1)];
    [bgView addSubview:lineIV];
    //** ok
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
