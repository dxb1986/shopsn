//
//  ZCenterAccountViewController.h
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块
 *
 *  账户管理 视图控制器
 *
 */
#import "BaseViewController.h"

@interface ZCenterAccountViewController : BaseViewController

/** 用户 积分 */
@property (nonatomic, copy) NSString *ca_points;

/** 用户 优惠券 */
@property (nonatomic, copy) NSString *ca_discount;

/** 用户 余额 */
@property (nonatomic, copy) NSString *ca_balance;

/** 用户 银行卡 */
@property (nonatomic, copy) NSString *ca_bankCard;


@end
