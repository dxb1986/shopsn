//
//  ZCenterMessageViewController.h
//  shopSN
//
//  Created by chang on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块
 *
 *  消息 视图控制器
 *
 */
#import "BaseViewController.h"

@interface ZCenterMessageViewController : BaseViewController

/** 消息页面 消息数组 */
@property (nonatomic, strong) NSArray *messageArray;


@end
