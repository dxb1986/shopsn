//
//  ZCenterMyCollectionViewController.m
//  shopSN
//
//  Created by yisu on 16/7/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterMyCollectionViewController.h"

#import "ZCommonGoodsInfoViewController.h"//普通商品 信息页面

//子视图
#import "ZCenterMyPrintsCell.h"     //旅游类 cell
#import "ZCenterMyPrintsGoodsCell.h"//商品类 cell

@interface ZCenterMyCollectionViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSArray *_dataSource;
}
/**清除足迹按钮按钮*/
@property (nonatomic,strong) UIButton *clearBtn;

@end

@implementation ZCenterMyCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的收藏";
    
    //视图 初始化
    [self initViews];
    
    [self getData];
}

#pragma mark - 获取数据
- (void)getData {
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid]};
    [ZHttpRequestService POST:@"Collection/personCollect" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            _dataSource = jsonDic[@"data"];
            [_tableView reloadData];
            
        }else{
            _dataSource = @[];
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];

}
#pragma mark - 页面初始化
//视图 初始化
- (void)initViews {
    
    //顶部边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 1)];
    [self.view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, __kWidth, __kHeight-65)];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
    [self.view addSubview:self.clearBtn];
    
}


#pragma mark - ===== tableView DataSource and Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = _dataSource[indexPath.row];
    
        //普通商品
        ZCenterMyPrintsGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPrintsGoodsCell"];
        if (!cell) {
            cell = [[ZCenterMyPrintsGoodsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyPrintsGoodsCell"];
        }
        
//        [cell.cp_shoppingButton addTarget:self action:@selector(shoppingButtonAciton) forControlEvents:BtnTouchUpInside];
    if (!IsNilString(dic[@"pic_url"])) {
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",GoodsImageUrl,dic[@"pic_url"]];
        [cell.cp_iconIV sd_setImageWithURL:[NSURL URLWithString:urlStr]];
    }
    if (!IsNilString(dic[@"price_new"])) {
        cell.cp_priceLb.text = dic[@"price_new"];
    }
    if (!IsNilString(dic[@"detail_title"])) {
        cell.cp_titleLb.text = dic[@"detail_title"];
    }
    
    
        cell.cp_shoppingButton.hidden = true;
    
    //删除按钮
    UIButton *delBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(cell.cp_iconIV)+3, CGRectYH(cell.cp_priceLb), 40, 20)];
    [delBtn setTitle:@"删除" forState:0];
    delBtn.titleLabel.font = MFont(13);
    [delBtn setTitleColor:__DefaultColor forState:0];
    delBtn.layer.borderColor = __DefaultColor.CGColor;
    delBtn.layer.borderWidth = 1;
    [cell addSubview:delBtn];
    delBtn.tag = indexPath.row;
    [delBtn addTarget:self action:@selector(respondsToCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 102;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"点击行数:%ld",(long)indexPath.row);
    
    NSDictionary *dic = _dataSource[indexPath.row];
    
    NSString *type = dic[@"is_type"];
    NSString *goodID = dic[@"goods_id"];
    if ([type isEqualToString:@"1"]) {
        ZCommonGoodsInfoViewController *vc = [[ZCommonGoodsInfoViewController alloc] init];
        vc.comGoodsID = goodID;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
        
}


- (void)shoppingButtonAciton {
    NSLog(@"加入购物车");
}

//删除某一个
-(void)respondsToCancelBtn:(UIButton *)sender{
    WK(weakSelf)
    [ZHttpRequestService POST:@"Collection/delCollect" withParameters:@{@"id":_dataSource[sender.tag][@"id"],@"token":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [weakSelf getData];
        }
    } failure:^(NSError *error) {
        
    } animated:YES withView:nil];
}
-(void)respondsToClearBtn{
    WK(weakSelf)
    [ZHttpRequestService POST:@"Collection/emptyCollect" withParameters:@{@"token":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [weakSelf getData];
        }
    } failure:^(NSError *error) {
        
    } animated:YES withView:nil];
    
}

#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIButton *)clearBtn{
    if (!_clearBtn) {
        _clearBtn = [[UIButton alloc] initWithFrame:CGRectMake(__kWidth-60, 20, 44, 44)];
        _clearBtn.titleLabel.font = MFont(14);
        [_clearBtn setTitle:@"清除" forState:0];
        [_clearBtn setTitleColor:__DefaultColor forState:0];
        [_clearBtn addTarget:self action:@selector(respondsToClearBtn) forControlEvents:UIControlEventTouchUpInside];
        
        //标题
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 100, 44)];
        titleLabel.text = @"我的收藏";
        titleLabel.font = MFont(17);
        titleLabel.center = CGPointMake(self.view.center.x, titleLabel.center.y);
        titleLabel.textAlignment = 1;
        [self.view addSubview:titleLabel];
    }
    return _clearBtn;
}

@end
