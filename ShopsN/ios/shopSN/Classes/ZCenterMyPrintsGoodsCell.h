//
//  ZCenterMyPrintsGoodsCell.h
//  shopSN
//
//  Created by yisu on 16/7/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的足迹 页面
 *
 *   商品类 cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZCenterMyPrintsGoodsCell : BaseTableViewCell

/** cell 图片 */
@property (nonatomic, strong) UIImageView *cp_iconIV;

/** cell 标题 */
@property (nonatomic, strong) UILabel *cp_titleLb;

/** cell 详情 */
@property (nonatomic, strong) UILabel *cp_detailLb;

/** cell 价格 */
@property (nonatomic, strong) UILabel *cp_priceLb;

/** cell 购物车 */
@property (nonatomic, strong) UIButton *cp_shoppingButton;
/**足迹id*/
@property (nonatomic,copy) NSString *printFootId;
/**商品id*/
@property (nonatomic,copy) NSString *printGoodsId;
@end
