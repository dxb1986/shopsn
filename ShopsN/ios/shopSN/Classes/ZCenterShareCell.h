//
//  ZCenterShareCell.h
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 shopSN分享 页面
 *
 *   cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZCenterShareCell : BaseTableViewCell



/** 图片 */
@property (nonatomic, strong) UIImageView *cs_iconIV;

/** 标题 */
@property (nonatomic, strong) UILabel *cs_titleLb;

/** 详情 */
@property (nonatomic, strong) UILabel *cs_detailLb;




@end
