//
//  ZCenterShareCell.m
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterShareCell.h"

@interface ZCenterShareCell ()


@end

@implementation ZCenterShareCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    
    return self;
}




- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 44)];
    
    [self addSubview:bgView];
    
    //bgView.backgroundColor = [UIColor orangeColor];
    
    

    //1 图片
    _cs_iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
    [bgView addSubview:_cs_iconIV];
    _cs_iconIV.contentMode = UIViewContentModeScaleAspectFit;
//    _cs_iconIV.backgroundColor = __DefaultColor;//
    
    //2 title
    _cs_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cs_iconIV)+5, 14, 100, 15)];
    [bgView addSubview:_cs_titleLb];
//    _cs_titleLb.backgroundColor = __TestOColor;
    _cs_titleLb.font = MFont(12);
    _cs_titleLb.textColor = HEXCOLOR(0x999999);
    

    //3 detail
    _cs_detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-130-15, 14, 130, 15)];
    [bgView addSubview:_cs_detailLb];
//    _cs_detailLb.backgroundColor = __TestOColor;
    _cs_detailLb.font = MFont(12);
    _cs_detailLb.textColor = HEXCOLOR(0x999999);
    _cs_detailLb.textAlignment = NSTextAlignmentRight;
    
    
    
    
    

    //4 line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(bgView)-1, CGRectW(bgView)-10, 1)];
    [self addSubview:lineIV];
    //** ok
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
