//
//  ZCenterShareDomainCell.m
//  shopSN
//
//  Created by yisu on 16/9/18.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterShareDomainCell.h"

@implementation ZCenterShareDomainCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    
    return self;
}




- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 154)];
    
    [self addSubview:bgView];
    
    //bgView.backgroundColor = [UIColor orangeColor];
    
    //1 图片
    _cs_iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
    [bgView addSubview:_cs_iconIV];
    _cs_iconIV.contentMode = UIViewContentModeScaleAspectFit;
    //    _cs_iconIV.backgroundColor = __DefaultColor;//
    
    //2 title
    _cs_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cs_iconIV)+5, 14, 100, 15)];
    [bgView addSubview:_cs_titleLb];
    //    _cs_titleLb.backgroundColor = __TestOColor;
    _cs_titleLb.font = MFont(12);
    _cs_titleLb.textColor = HEXCOLOR(0x999999);
    
    
    
    
    //3 shareUrlView

    _cs_shareUrlLabel= [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cs_iconIV)+5, CGRectYH(_cs_iconIV)+5, 200, 105)];
    //_cs_shareUrlLabel.backgroundColor = __TestGColor;
    _cs_shareUrlLabel.font = MFont(17);
    _cs_shareUrlLabel.numberOfLines = 0;
    [bgView addSubview:_cs_shareUrlLabel];
    //_cs_shareUrlView.hidden = YES;
    
    
    
    //4 line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(bgView)-1, CGRectW(bgView)-10, 1)];
    [self addSubview:lineIV];
    //** ok
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
    //5 分享按钮
    _cs_shareButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-50-10, (CGRectH(bgView)-25)/2, 50, 25)];
    [bgView addSubview:_cs_shareButton];
    _cs_shareButton.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //[_cs_shareButton setImage:MImage(@"ic_share.png") forState:BtnNormal];
    _cs_shareButton.titleLabel.font = MFont(12);
    _cs_shareButton.layer.borderWidth = 1.0;
    _cs_shareButton.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    [_cs_shareButton setTitle:@"分享" forState:BtnNormal];
    [_cs_shareButton setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
