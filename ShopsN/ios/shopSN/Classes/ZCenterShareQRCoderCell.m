//
//  ZCenterShareQRCoderCell.m
//  shopSN
//
//  Created by yisu on 16/9/18.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterShareQRCoderCell.h"
#import "ZCenterMyQRCodeViewController.h"//我的二维码名片页面

@interface ZCenterShareQRCoderCell ()

/**二维码图*/
@property (nonatomic,strong) UIImage *myImage;

@end


@implementation ZCenterShareQRCoderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    
    return self;
}




- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 174)];
    
    [self addSubview:bgView];
    
    //bgView.backgroundColor = [UIColor orangeColor];
    
    
    
    //1 图片
    _cs_iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 12, 20, 20)];
    [bgView addSubview:_cs_iconIV];
    _cs_iconIV.contentMode = UIViewContentModeScaleAspectFit;
    //    _cs_iconIV.backgroundColor = __DefaultColor;//
    
    //2 title
    _cs_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cs_iconIV)+5, 14, 100, 15)];
    [bgView addSubview:_cs_titleLb];
    //    _cs_titleLb.backgroundColor = __TestOColor;
    _cs_titleLb.font = MFont(12);
    _cs_titleLb.textColor = HEXCOLOR(0x999999);
    
    
    //3 ewIV
    _cs_ewIV = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectW(bgView)-120)/2, CGRectYH(_cs_iconIV)+5, 120, 120)];
    
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    // 3.给过滤器添加数据(正则表达式/账号和密码)
    NSString *dataString = [NSString stringWithFormat:@"%@",ShareURL];
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKeyPath:@"inputMessage"];
    
    // 4.获取输出的二维码
    CIImage *outputImage = [filter outputImage];
    
    _cs_ewIV.image = [self createNonInterpolatedUIImageFormCIImage:outputImage withSize:120];
    self.myImage = _cs_ewIV.image;
    [bgView addSubview:_cs_ewIV];
    
    
    
    
    
    
    //4 line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(bgView)-1, CGRectW(bgView)-10, 1)];
    [self addSubview:lineIV];
    //** ok
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
}

- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
