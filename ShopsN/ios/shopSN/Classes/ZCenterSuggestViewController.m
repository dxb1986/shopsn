//
//  ZCenterSuggestViewController.m
//  shopSN
//
//  Created by yisu on 16/10/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterSuggestViewController.h"

@interface ZCenterSuggestViewController ()<UITextFieldDelegate, UITextViewDelegate>
{
    
    UIView *bgView;
    UITextView *suggestTV;
    UITextField *phoneTF;
    
    UILabel *_label1;
    UILabel *_label2;
}

@end

@implementation ZCenterSuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"意见反馈";
    
    [self initSubViews:self.mainMiddleView];//初始化中间视图
}


#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    
    //1 背景
    bgView = [[UIView alloc] initWithFrame:view.frame];
    
    [view addSubview:bgView];
    bgView.backgroundColor = __AccountBGColor;
    
    //2 建议内容
    UIView *suggestView = [[UIView alloc] initWithFrame:CGRectMake(10, 15, CGRectW(bgView)-20, 150)];
    [bgView addSubview:suggestView];
    suggestView.backgroundColor = HEXCOLOR(0xffffff);
    
    _label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 180, 20)];
    [suggestView addSubview:_label1];
    _label1.font = MFont(13);
    _label1.textColor = HEXCOLOR(0x999999);
    _label1.text = @"请输入遇到的问题或建议...";
    
    
    _label2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(suggestView)-120, CGRectH(suggestView)-20, 120, 20)];
    [suggestView addSubview:_label2];
    _label2.font = MFont(13);
    _label2.textColor = HEXCOLOR(0x999999);
    _label2.text = @"您还可以输入500字";
    
    suggestTV = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(suggestView), 150)];
    [suggestView addSubview:suggestTV];
    suggestTV.backgroundColor = [UIColor clearColor];
    suggestTV.textColor = HEXCOLOR(0x333333);
    suggestTV.font = MFont(13);
    suggestTV.delegate = self;
    
    
    
    //3 联系电话
    UIView *phoneView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectYH(suggestTV)+30, CGRectW(bgView)-20, 30)];
    [bgView addSubview:phoneView];
    phoneView.backgroundColor = HEXCOLOR(0xffffff);
    
    UILabel *noteLb = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 30)];
    [phoneView addSubview:noteLb];
    noteLb.font = MFont(13);
    noteLb.textColor = HEXCOLOR(0x333333);
    noteLb.text = @"联系电话：";
    
    phoneTF = [[UITextField alloc] initWithFrame:CGRectMake(CGRectXW(noteLb), 0, CGRectW(phoneView)-CGRectW(noteLb)-10, 30)];
    [phoneView addSubview:phoneTF];
    phoneTF.font = MFont(13);
    phoneTF.textColor = HEXCOLOR(0X333333);
    phoneTF.delegate = self;
    phoneTF.placeholder = @"选填，便于我们联系您";
    
    
    //4 提交
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectYH(phoneView)+20, CGRectW(bgView)-40, 40)];
    [bgView addSubview:submitButton];
    submitButton.backgroundColor = __DefaultColor;
    submitButton.titleLabel.font = MFont(14);
    submitButton.layer.cornerRadius = 4.0f;
    [submitButton setTitle:@"提交" forState:BtnNormal];
    [submitButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [submitButton addTarget:self action:@selector(submitButtonAction) forControlEvents:BtnTouchUpInside];
    
}


- (void)submitButtonAction {
    if (IsNilString(suggestTV.text)) {
        [SXLoadingView showAlertHUD:@"请填写反馈信息" duration:2];
        return;
    }
    
    NSLog(@"联系方式:%@ ===反馈建议内容:%@", phoneTF.text,suggestTV.text);
    
    NSDictionary *params =@{@"content":suggestTV.text,
                            @"mobile":phoneTF.text,
                            };
    
    [ZHttpRequestService POST:@"liuyan" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [SXLoadingView showAlertHUD:@"提交成功" duration:2];
            [self backAciton];
        }else {
            NSString *message = jsonDic[@"message"];
            [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
        }
    } failure:^(NSError *error) {
        //
    } animated:NO withView:nil];
    
}




#pragma mark - textView Delegate

-(void)textViewDidChange:(UITextView *)textView {
    if ([textView.text isEqualToString:@""] || IsNilString(textView.text)) {
        _label1.text = @"请输入遇到的问题或建议...";
        _label2.text = @"您还可以输入500字";
    }else{
        _label1.text = @"";
        int labelLength = 500 - (int)textView.text.length;
        _label2.text = [NSString stringWithFormat:@"您还可以输入%d字",labelLength];
    }
}




#pragma mark -UITextfiled Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    NSLog(@"x:%lf===y:%lf===w:%lf===h:%lf",bgView.frame.origin.x,bgView.frame.origin.y, bgView.frame.size.width, bgView.frame.size.height);
    if (__kHeight<__k5Height) {
        
        
        CGRect frame = bgView.frame;
        CGFloat h = bgView.frame.origin.y - 100;
        frame.origin.y = h;
        
        [UIView animateWithDuration:0.4 animations:^{
            
            bgView.frame = frame;
        }];
    }
    
    
    return YES;
}



-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (__kHeight<__k5Height) {
        CGRect frame = bgView.frame;
        CGFloat h = bgView.frame.origin.y + 100;
        frame.origin.y = h;
        [UIView animateWithDuration:0.4 animations:^{
            bgView.frame = frame;
        }];
    }

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (phoneTF || suggestTV) {
        [self.view endEditing:YES];
    }
}


#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
