//
//  ZChangeAddressTableViewOtherCell.h
//  亿速
//
//  Created by chang on 16/7/20.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ZChangeAddressTableViewOtherCell : BaseTableViewCell

/**
 *  类型
 */
@property (strong,nonatomic) UILabel *titleLb;

/**
 *  详细地址
 */
@property (strong,nonatomic) UITextView *addressTF;

@end
