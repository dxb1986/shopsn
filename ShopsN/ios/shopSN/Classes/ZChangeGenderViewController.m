//
//  ZChangeGenderViewController.m
//  shopSN
//
//  Created by imac on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangeGenderViewController.h"
#import "ZCenterPersonalInfoCell.h"

@interface ZChangeGenderViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
}

//@property (strong,nonatomic) UITableView *tableView;

@end

@implementation ZChangeGenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.titleLb.text = @"修改性别";
    self.title = @"修改性别";
    
    [self initView];
}

-(void)initView{
    
    //添加 headerView
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 15)];
    //headerView.backgroundColor = HEXCOLOR(0xfcfcfc);
    headerView.backgroundColor = __AccountBGColor;
    [self.view addSubview:headerView];
    //headerView 底部线条
    UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(headerView)-1, CGRectW(headerView), 1)];
    linIV.backgroundColor = HEXCOLOR(0xdedede);
    [headerView addSubview:linIV];
    
    
    //添加 tableView
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = headerView;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZCenterPersonalInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"genderCell"];
    if (!cell) {
        cell = [[ZCenterPersonalInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"genderCell"];
        //cell.tintColor = HEXCOLOR(0xff9632);
        cell.tintColor = __DefaultColor;
    }
    switch (indexPath.row) {
        case 0:
        {
        cell.titleLb.text = @"男";
        }
            break;
            
        default:{
        cell.titleLb.text = @"女";
        }
            break;
    }
    //判断性别
    if ([cell.titleLb.text isEqualToString:_genderStr]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
        _genderStr = @"男";
        }
            break;
            
        default:
        {
        _genderStr = @"女";
        }
            break;
    }
    [_tableView reloadData];
    [self save];
}

/**
 *  上传到服务器
 */
-(void)save{
    
    [SXLoadingView showAlertHUD:@"选项修改成功" duration:SXLoadingTime];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSDictionary *params =@{@"sex":[_genderStr isEqualToString:@"男"]?@"m":@"w",@"token":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"User/changeSex" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            NSLog(@"xiugai--%@",jsonDic);

            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
