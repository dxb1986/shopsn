//
//  ZCommonGoodsCountCell.m
//  shopSN
//
//  Created by yisu on 16/10/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsCountCell.h"

@interface ZCommonGoodsCountCell ()
{
    NSString *_goodsCountsStr;//商品数量
    UIView *_choosePriceView;
}

@end

@implementation ZCommonGoodsCountCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //点击cell不变色
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        _goodsCountsStr = @"1";
        
        
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    //bgView
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 54)];
    [self addSubview:bgView];
//    bgView.backgroundColor = [UIColor orangeColor];
    
    //choosePriceView
    _choosePriceView = [[UIView alloc] initWithFrame:CGRectMake(10, 2, CGRectW(bgView)-20, 40)];
    [bgView addSubview:_choosePriceView];
    //_choosePriceView.backgroundColor = __TestOColor;
    [self initChoosePriceSubViews:_choosePriceView];
    
    
    
    //lineIV
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-10, CGRectW(bgView), 10)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    
    
    
}



#pragma mark - 价格选择视图
- (void)initChoosePriceSubViews:(UIView *)view {
    
    //1 noteLb
    UILabel *noteLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 40, 20)];
    [view addSubview:noteLb];
    //noteLb.backgroundColor = __TestGColor;
    noteLb.font = MFont(13);
    noteLb.textColor = HEXCOLOR(0x333333);
    noteLb.text = @"数量：";
    
    //2 goodsSubBtn
    UIButton *goodsSubBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(noteLb), 10, 20, 20)];
    [view addSubview:goodsSubBtn];
    //goodsSubBtn.backgroundColor = __TestGColor;
    goodsSubBtn.layer.borderWidth = 1.0;
    goodsSubBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    goodsSubBtn.tag = 330;
    goodsSubBtn.titleLabel.font = MFont(14);
    [goodsSubBtn setTitle:@"-" forState:BtnNormal];
    [goodsSubBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [goodsSubBtn addTarget:self action:@selector(goodsChangeButtonAciton:) forControlEvents:BtnTouchUpInside];
    
    //3 _goodsCountsLb
    _goodsCountsLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(goodsSubBtn)+5, 10, 40, 20)];
    [view addSubview:_goodsCountsLb];
    //_goodsCountsLb.backgroundColor = __TestOColor;
    _goodsCountsLb.layer.borderWidth = 1.0;
    _goodsCountsLb.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    _goodsCountsLb.font = MFont(14);
    _goodsCountsLb.textAlignment = NSTextAlignmentCenter;
    _goodsCountsLb.text = _goodsCountsStr;
    
    
    //4 goodsAddBtn
    UIButton *goodsAddBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(_goodsCountsLb)+5, 10, 20, 20)];
    [view addSubview:goodsAddBtn];
    //goodsAddBtn.backgroundColor = __TestGColor;
    goodsAddBtn.layer.borderWidth = 1.0;
    goodsAddBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    goodsAddBtn.tag = 331;
    goodsAddBtn.titleLabel.font = MFont(14);
    [goodsAddBtn setTitle:@"+" forState:BtnNormal];
    [goodsAddBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [goodsAddBtn addTarget:self action:@selector(goodsChangeButtonAciton:) forControlEvents:BtnTouchUpInside];
    
}


- (void)goodsChangeButtonAciton:(UIButton *)sender {
    NSInteger staffCount = [_goodsCountsLb.text integerValue];
    
    //    NSLog(@"%@", _sg_countLb.text);
    if (sender.tag - 330) {
        staffCount += 1;
    }else{
        
        if (staffCount<1) {
            return;
        }
        staffCount -= 1;
    }
    NSLog(@"adultCounts -- %ld", (long)staffCount);
    _goodsCountsLb.text = [NSString stringWithFormat:@"%ld",(long)staffCount];
    
    [self.delegate changeGoodsCounts:staffCount];

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
