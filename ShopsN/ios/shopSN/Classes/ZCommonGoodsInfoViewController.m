//
//  ZCommonGoodsInfoViewController.m
//  shopSN
//
//  Created by yisu on 16/8/2.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsInfoViewController.h"
#import "ZShoppingCarViewController.h"  //购物车页面
#import "ZPayViewController.h"//支付页面
#import "ZCenterMemberCertificateViewController.h"//会员资格页面

#import "ZYSLoginViewController.h"//亿速登录页面

#import "ZCommonGoodsIntroductionView.h"//普通商品简介页面
#import "ZCommonGoodsDetailView.h"      //普通商品 详情页面
#import "ZCommonGoodsJudgeView.h"       //普通商品 评价页面


#import "UMsocial.h"



@interface ZCommonGoodsInfoViewController ()<ZCommonGoodsIntroductionDelegate, UMSocialUIDelegate>
{
    UIView *_naviView;
    //UIScrollView *_contentScrollView;
    UIView *_btnBackView;
    UIView *bottomView;
    
    
    //普通商品 三个子视图
    ZCommonGoodsIntroductionView *commonGoodsIntroductionView;
    ZCommonGoodsDetailView *commonGoodsDetailView;
    ZCommonGoodsJudgeView *commonGoodsJudgeView;
    
    NSMutableArray *dealArray;
    
    
    //跳转添加购物车相关数据
    NSString *_goodsCounts;
    
}

/**栏目选择按钮 */
@property (nonatomic, strong) NSMutableArray *btnArray;

/** 会员商品 数组 */
@property (nonatomic, strong) NSMutableArray *goodsInfoArrays;

@end

@implementation ZCommonGoodsInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _goodsInfoArrays = [NSMutableArray array];
    
    [self getData];
    
    //self.mainMiddleView.backgroundColor = __TestGColor;
    
    
    
    //[self initSubViews:self.mainMiddleView];
    
    //初始化数据
    _goodsCounts = @"1";
    
}


#pragma mark - 自定义中间子控件
- (void)addNaviSubControllers:(UIView *)middleView {
    
    
    
    
    //初始化导航scrollView
    _naviView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, CGRectW(middleView), CGRectH(middleView))];
    [middleView addSubview:_naviView];
    
    //[self setupTitle];
    
    
    _btnArray = [NSMutableArray array];
    
    
    //添加按钮
//    NSArray *titleArray = @[@"商品", @"详情", @"评价"];
    NSArray *titleArray = @[@"商品", @"详情"];
    for (int i= 0; i<titleArray.count; i++) {
        CGRect frame = CGRectMake((i * (CGRectW(_naviView)- titleArray.count*50-20)/2+ 10 + i*50), 0, 50, 30);
        [self initScreenButtonWith:frame withIndex:i withTitle:titleArray[i]];
    }
    
    
    //添加按钮底部view
    _btnBackView = [[UIView alloc] initWithFrame:CGRectMake(10+11, CGRectH(_naviView)/2+13, 28, 2)];
    _btnBackView.backgroundColor = __DefaultColor;
    [_naviView addSubview:_btnBackView];
    
}


- (void)initScreenButtonWith:(CGRect)frame withIndex:(NSInteger)index withTitle:(NSString *)title {
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [_naviView addSubview:button];
    [_btnArray addObject:button];
    
    
    
    if (!index) {
        button.selected = YES;
    }
    
    button.titleLabel.font = MFont(15);
    [button setTitle:title forState:BtnNormal];
    [button setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [button setTitleColor:__DefaultColor forState:BtnStateSelected];
    
    button.tag = 120 + index;
    [button addTarget:self action:@selector(naviViewButtonAciton:) forControlEvents:BtnTouchUpInside];
    
    
    
    
}


#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    ZGoodsDeal *goodsDeal = _goodsInfoArrays[0];
    
    //商品 简介页面
    commonGoodsIntroductionView = [[ZCommonGoodsIntroductionView alloc] initWithFrame:self.mainMiddleView.frame];
    [self.mainMiddleView addSubview:commonGoodsIntroductionView];
    commonGoodsIntroductionView.delegate = self;
    commonGoodsIntroductionView.goodsDeal = goodsDeal;
    
    
    
    //商品 详情页面
    commonGoodsDetailView = [[ZCommonGoodsDetailView alloc] initWithFrame:self.mainMiddleView.frame];
    [self.mainMiddleView addSubview:commonGoodsDetailView];
    commonGoodsDetailView.hidden = YES;
    
    //[commonGoodsDetailView loadCommonGoodsImage:goodDeal.goodsDetailPicArray];
    [commonGoodsDetailView loadCommonGoodsID:goodsDeal.goodsID];
    
    
    
    
    //商品 评价页面
    commonGoodsJudgeView = [[ZCommonGoodsJudgeView alloc] initWithFrame:self.mainMiddleView.frame];
    [self.mainMiddleView addSubview:commonGoodsJudgeView];
    commonGoodsJudgeView.hidden = YES;
    [commonGoodsJudgeView loadCommonJudgeData:_goodsInfoArrays];
    
    
    //底部视图
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(self.mainMiddleView)-40, CGRectW(self.mainMiddleView), 40)];
    [self.mainMiddleView addSubview:bottomView];
    bottomView.backgroundColor = HEXCOLOR(0xdedede);
    [self addCommonBottomViewSubViews:bottomView];
    
    
}


//加载底部视图 子控件
- (void)addCommonBottomViewSubViews:(UIView *)view {
    
    
    
    
    //首页
    UIButton *homeButton = [[UIButton alloc] initWithFrame:CGRectMake(1, 1, 50, CGRectH(view)-2)];
    [view addSubview:homeButton];
    homeButton.tag = 240;
    homeButton.backgroundColor = HEXCOLOR(0xffffff);
    //homeButton.layer.cornerRadius = 3.0f;
    homeButton.titleLabel.font = MFont(14);
    [homeButton setTitle:@"首页" forState:BtnNormal];
    [homeButton setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [homeButton addTarget:self action:@selector(bottmBtnAction:) forControlEvents:BtnTouchUpInside];
    
    //购物车
    
    
    UIButton *shoppingCartButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(homeButton)+1, 1, 50, CGRectH(view)-2)];
    [view addSubview:shoppingCartButton];
    shoppingCartButton.tag = 241;
    shoppingCartButton.backgroundColor = HEXCOLOR(0xffffff);
    //shoppingCartButton.layer.cornerRadius = 3.0f;
    shoppingCartButton.titleLabel.font = MFont(14);
    [shoppingCartButton setTitle:@"购物车" forState:BtnNormal];
    [shoppingCartButton setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [shoppingCartButton addTarget:self action:@selector(bottmBtnAction:) forControlEvents:BtnTouchUpInside];
    
    //NSLog(@"%@",[UdStorage getObjectforKey:UserType]);
//    if ([[UdStorage getObjectforKey:UserType] isEqualToString:@"游客"]||IsNilString([UdStorage getObjectforKey:UserType])){
//        
//        //会员说明 color(102,102,102) (0x666666)
//        UIButton *noteButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(shoppingCartButton)+1, 1, CGRectW(view)-50*2-4, CGRectH(view)-2)];
//        [view addSubview:noteButton];
//        noteButton.tag = 242;
//        noteButton.backgroundColor = HEXCOLOR(0x666666);
//        noteButton.titleLabel.font = MFont(14);
//        [noteButton setTitle:@"请晋升为会员，继续购物" forState:BtnNormal];
//        [noteButton setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
//        [noteButton addTarget:self action:@selector(bottmBtnAction:) forControlEvents:BtnTouchUpInside];
//    }else{
//        
        //加入购物车
        UIButton *addShoppingCartBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(shoppingCartButton)+1, 1, (CGRectW(view)-105)/2, CGRectH(view)-2)];
        [view addSubview:addShoppingCartBtn];
        addShoppingCartBtn.tag = 243;
        addShoppingCartBtn.backgroundColor = __TestOColor;
        addShoppingCartBtn.titleLabel.font = MFont(14);
        [addShoppingCartBtn setTitle:@"加入购物车" forState:BtnNormal];
        [addShoppingCartBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        [addShoppingCartBtn addTarget:self action:@selector(bottmBtnAction:) forControlEvents:BtnTouchUpInside];
        
        //立即购买
        UIButton *shoppingBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(addShoppingCartBtn)+1, 1, (CGRectW(view)-105)/2, CGRectH(view)-2)];
        [view addSubview:shoppingBtn];
        shoppingBtn.tag = 244;
        shoppingBtn.backgroundColor = HEXCOLOR(0xff5000);//(255 80 10)
        shoppingBtn.titleLabel.font = MFont(14);
        [shoppingBtn setTitle:@"立即购买" forState:BtnNormal];
        [shoppingBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        [shoppingBtn addTarget:self action:@selector(bottmBtnAction:) forControlEvents:BtnTouchUpInside];
//    }

    
}




#pragma mark - ==== 按钮触发方法 =====
//导航选择按钮 方法
- (void)naviViewButtonAciton:(UIButton *)sender {
    
    
    sender.selected = YES;
    
    for (UIButton *button in _btnArray) {
        if (button == sender) {
            //button.backgroundColor = __TestOColor;
            continue;
        } else{
            button.selected = NO;
            //button.backgroundColor = [UIColor clearColor];
        }
        
        
    }
    
    
    
    switch (sender.tag - 120) {
        case 0:
        {
            NSLog(@"选择商品简介");
            [UIView animateWithDuration:0.2 animations:^{
                _btnBackView.frame = CGRectMake(CGRectX(sender)+11, CGRectH(_naviView)/2+13, 28, 2);
            }];
            
            commonGoodsIntroductionView.hidden = NO;
            commonGoodsDetailView.hidden = YES;
            commonGoodsJudgeView.hidden =YES;
            
        }
            break;
            
        case 1:
        {
            NSLog(@"选择商品详情");
            [UIView animateWithDuration:0.2 animations:^{
                //_btnBackView.frame = CGRectMake(CGRectW(_naviView)/3+10+11, CGRectH(_naviView)/2+13, 28, 2);
                _btnBackView.frame = CGRectMake(CGRectX(sender)+11, CGRectH(_naviView)/2+13, 28, 2);
                
            }];
            
            commonGoodsIntroductionView.hidden = YES;
            commonGoodsDetailView.hidden = NO;
            commonGoodsJudgeView.hidden =YES;
            
        }
            break;
            
        case 2:
        {
            NSLog(@"选择商品评价");
            [UIView animateWithDuration:0.2 animations:^{
                _btnBackView.frame = CGRectMake(CGRectX(sender)+11, CGRectH(_naviView)/2+13, 28, 2);
            }];
            
            commonGoodsIntroductionView.hidden = YES;
            commonGoodsDetailView.hidden = YES;
            commonGoodsJudgeView.hidden = NO;
            
        }
            break;
            
        default:
            break;
    }
    
    
}

//底部按钮触发方法
- (void)bottmBtnAction:(UIButton *)sender {
    ZGoodsDeal *goodsDeal = _goodsInfoArrays[0];
    
    switch (sender.tag - 240) {
        case 0:
        {
            NSLog(@"普通商品页面 返回首页");
            [self returnHomePage];
        }
            
            break;
            
        case 1:{
            NSLog(@"普通商品页面 进入购物车页面");
            //判断用户是否登录
            if (IsNilString([UdStorage getObjectforKey:Userid])) {
                [self loginAction];
            }else{
                self.navigationController.tabBarController.hidesBottomBarWhenPushed = NO;
                //0表示返回到tabor第一个按钮的主页面，依此类推
                self.navigationController.tabBarController.selectedIndex = 2;
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
        }
            
            break;
            
            
        case 2:
        {
            NSLog(@"普通商品页面 进入会员资格页面");
            //判断用户是否登录
            if (IsNilString([UdStorage getObjectforKey:Userid])) {
                [self loginAction];
            }else {
                ZCenterMemberCertificateViewController *vc = [[ZCenterMemberCertificateViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
            
            break;
            
        case 3:
        {
            NSLog(@"普通商品页面 加入购物车");
            //判断用户是否登录
            if (IsNilString([UdStorage getObjectforKey:Userid])) {
                [self loginAction];
            }else {
                [self goodsAddShoppingCartRequest:goodsDeal];
            }

        }
            
            
            break;
            
        case 4:
        {
            NSLog(@"立即购买 普通商品跳转至支付页面");
            if (IsNilString([UdStorage getObjectforKey:Userid])) {
                [self loginAction];
            }else {
                NSDictionary *paraDic = @{@"token":[UdStorage getObjectforKey:Userid],
                                          @"goods_id":goodsDeal.goodsID,
                                          @"goods_num":_goodsCounts};
                
                [ZHttpRequestService POSTGoods:@"Order/buyNowSettlement" withParameters:paraDic success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
                    if (succe) {
                        
                        NSLog(@"%@", jsonDic[@"data"]);
                        
                        ZPayViewController *vc = [[ZPayViewController alloc] initWithArray:jsonDic[@"data"]];
                        
                        [self.navigationController pushViewController:vc animated:YES];
                        
                    }else{
                        NSString *message = jsonDic[@"message"];
                        
                        [SXLoadingView showAlertHUD:message duration:2];
                    }
                } failure:^(NSError *error) {
                    
                } animated:YES withView:nil];
            }
            
            
        }
            
            break;
            
        default:
            break;
    }

    
}

#pragma mark - 进入登录页面
-(void)loginAction{
    
//    ZLoginViewController *vc = [[ZLoginViewController alloc] init];
    ZYSLoginViewController *vc = [[ZYSLoginViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}




#pragma mark - 加入购物车请求
- (void)goodsAddShoppingCartRequest:(ZGoodsDeal *)deal {
    //115.159.146.202/api/goods.php?	action=add_goods_cart&goods_id=387&user_id=10001338&taocan= &fanli_jifen=4&shoper_id=387&price_new=49.00&format=array
//    NSDictionary *params = @{@"goods_id":deal.goodsID,
//                             @"user_id":[UdStorage getObjectforKey:Userid],
//                             @"pic_url":deal.goodsPicUrl,
//                             @"goods_title":deal.goodsTitle,
//                             @"taocan":deal.goodsPackage,
//                             @"fanli_jifen":deal.goodsPoints,
//                             @"shoper_id":deal.goodsShoperID,
//                             @"price_new":deal.goodsNowPrice,
//                             @"num":_goodsCounts};
    NSDictionary *params = @{@"goods_id":deal.goodsID,@"num":_goodsCounts,@"token":[UdStorage getObjectforKey:Userid]};
    [ZHttpRequestService POSTGoods:@"Cart/addCart" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [SXLoadingView showAlertHUD:@"添加成功" duration:2];
        }else{
            NSString *message = jsonDic[@"message"];
            [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
        }
        
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
        
    } animated:NO withView:nil];
    
    
    
    
    
}


#pragma mark -- 普通商品视图 简介页面 Delegate

- (void)senderChooseGoodsCounts:(NSInteger)goodsCounts {
    NSLog(@"选择数量:%d(个)",goodsCounts);
    _goodsCounts = [NSString stringWithFormat:@"%d",goodsCounts];
}



- (void)showCommonGoodsJudgeView:(id)sender {
    //*** 到目前为止，传过来的sender并没有使用，未放置后续变化，暂时保留
    
    
    
    NSLog(@"选择普通商品 评价");
    UIButton *IntroductionBtn = _btnArray[0];
    UIButton *JudgeBtn = _btnArray[2];
    JudgeBtn.selected = YES;
    IntroductionBtn.selected = NO;
    [UIView animateWithDuration:0.2 animations:^{
        _btnBackView.frame = CGRectMake(CGRectX(JudgeBtn)+11, CGRectH(_naviView)/2+13, 28, 2);
    }];
    
    commonGoodsIntroductionView.hidden = YES;
    commonGoodsDetailView.hidden = YES;
    commonGoodsJudgeView.hidden = NO;
    
  
}

//显示普通商品详情页面
- (void)showCommonGoodsDetailView {
    NSLog(@"选择普通商品 详情");
    UIButton *IntroductionBtn = _btnArray[0];
    UIButton *DetailBtn = _btnArray[1];
    DetailBtn.selected = YES;
    IntroductionBtn.selected = NO;
    [UIView animateWithDuration:0.2 animations:^{
        _btnBackView.frame = CGRectMake(CGRectX(DetailBtn)+11, CGRectH(_naviView)/2+13, 28, 2);
    }];
    
    commonGoodsIntroductionView.hidden = YES;
    commonGoodsDetailView.hidden = NO;
    commonGoodsJudgeView.hidden = YES;
}


//分享 收藏
- (void)commonGoodsShareAndColloctBtnAciton:(UIButton *)sender {
    if (sender.tag == 2410) {
        NSLog(@"商品收藏");
        [self addGoodsCollectRequest];
        
    }else{
        NSLog(@"商品分享");
        [self goodsShareAciton];
        
    }
}

#pragma mark - 分享
- (void)goodsShareAciton {
    ZGoodsDeal *goodsDeal = _goodsInfoArrays[0];
    NSString *titleStr = goodsDeal.goodsTitle;
    NSString *urlStr= [NSString stringWithFormat:@"%@id=%@",GoodsDetailUrl, goodsDeal.goodsID];
    NSLog(@"goodsDetailUrl:%@", urlStr);
//    [UMSocialData defaultData].extConfig.title = @"QQ分享标题";
//    [UMSocialData defaultData].extConfig.wechatSessionData.title = @"微信好友title";
//    [UMSocialData defaultData].extConfig.wechatTimelineData.title = @"微信朋友圈title";
//    
    
    
//    [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeApp;

    
    //微信好友 朋友圈分享
    [UMSocialData defaultData].extConfig.wechatSessionData.url = urlStr;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = urlStr;
    
    //qq分享 qq空间分享
    [UMSocialData defaultData].extConfig.qqData.url = urlStr;
    [UMSocialData defaultData].extConfig.qzoneData.url = urlStr;
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:urlStr];
    

    
    [UMSocialSnsService presentSnsIconSheetView:self appKey:UMKey shareText:titleStr shareImage:nil shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQQ,UMShareToQzone] delegate:self];
}
- (void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}




#pragma mark - 添加商品收藏请求
- (void)addGoodsCollectRequest {
    ZGoodsDeal *goodsDeal = _goodsInfoArrays[0];
    

//    NSDictionary *params = @{@"goods_id":goodsDeal.goodsID,
//                             @"user_id":[UdStorage getObjectforKey:Userid],
//                             @"pic_url":goodsDeal.goodsPicUrl,
//                             @"price_new":goodsDeal.goodsNowPrice,
//                             @"price_old":goodsDeal.goodsOriginalPrice,
//                             @"detail_title":goodsDeal.goodsTitle,
//                             @"is_type":@"1"};
    NSDictionary *params = @{@"goods_id":goodsDeal.goodsID,@"token":[UdStorage getObjectforKey:Userid]};
    [ZHttpRequestService POST:@"Collection/addCollect" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            //
            [SXLoadingView showAlertHUD:@"收藏成功" duration:2];
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
    } animated:NO withView:nil];
    
    
    
    
}




#pragma mark -- 普通商品视图 详情页面 Delegate


#pragma mark -- 普通商品视图 评价页面 Delegate


#pragma mark -- 返回首页
- (void)returnHomePage {
    
    self.navigationController.tabBarController.hidesBottomBarWhenPushed = NO;
    //0表示返回到tabor第一个按钮的主页面，依此类推
    self.navigationController.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:YES];
}



#pragma mark - ===== 获取数据 =====
- (void)getData {
    
    dealArray = [NSMutableArray array];
    
    if (IsNilString(self.comGoodsID)) {
        [SXLoadingView showAlertHUD:@"数据信息不全，请重新加载" duration:SXLoadingTime];
        return;
    }
    
    //请求商品详情 相关数据
    //115.159.146.202/api/goods.php?action=goods_details&id=387&user_id=10001338&format=array
    //NSDictionary *params = @{@"id":self.comGoodsID, @"user_id":[UdStorage getObjectforKey:Userid]};
    NSDictionary *params = @{@"id":self.comGoodsID};
//     NSDictionary *params = @{@"id":@"766"};
    [ZHttpRequestService POSTGoods:@"Goods/goodsDetail" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSArray *data = jsonDic[@"data"];
            if (data.count==0) {
                [SXLoadingView showAlertHUD:@"该类商品已下架" duration:1.5];
                [self.navigationController popViewControllerAnimated:YES];
                return ;
            }
            _goodsInfoArrays = [Parse parseGoodsDetail:jsonDic[@"data"]];
            //NSLog(@"goodsInfo:%@",_goodsInfoArrays[0]);
            
            
            //ZGoodsDeal *deal = _goodsInfoArrays[0];
//            for (int i = 0; i<_goodsInfoArrays.count; i++) {
//                ZGoodsDeal *deal = [[ZGoodsDeal alloc] init];
//                deal = _goodsInfoArrays[i];
//                [dealArray addObject:deal];
//            }
//            NSLog(@"%@",dealArray[0]);
            
            //初始化视图
            [self initSubViews:self.mainMiddleView];
            
            
            
            
        }else{
            NSString *message = jsonDic[@"message"];
            [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
            
        }
        
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
        
    } animated:NO withView:nil];
    
    
    //[self loadDataToSubView:_goodsInfoArrays];
}


//- (void)loadDataToSubView:(NSArray *)array {
//    
//    for (int i = 0; i<array.count; i++) {
//        ZGoodsDeal *deal = [[ZGoodsDeal alloc] init];
//        deal = array[i];
//        [dealArray addObject:deal];
//    }
//    NSLog(@"%@",dealArray[0]);
//}



#pragma mark - ===== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
