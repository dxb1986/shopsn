//
//  ZCommonGoodsIntroductionView.h
//  shopSN
//
//  Created by yisu on 16/8/1.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品信息页面
 *
 *  普通商品简介 视图
 *
 */
#import <UIKit/UIKit.h>

@class ZCommonGoodsIntroductionView;
@protocol ZCommonGoodsIntroductionDelegate <NSObject>

- (void)showCommonGoodsJudgeView:(id)sender;
- (void)showCommonGoodsDetailView;
- (void)commonGoodsShareAndColloctBtnAciton:(UIButton *)sender;
- (void)senderChooseGoodsCounts:(NSInteger)goodsCounts;

@end


@interface ZCommonGoodsIntroductionView : UIView

/** 普通商品简介页面 选中商品 */
@property (strong, nonatomic)ZGoodsDeal *goodsDeal;

/** 普通商品简介页面 代理方法 */
@property (weak, nonatomic) id<ZCommonGoodsIntroductionDelegate>delegate;

@end
