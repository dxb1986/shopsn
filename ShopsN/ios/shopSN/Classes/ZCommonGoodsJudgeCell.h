//
//  ZCommonGoodsJudgeCell.h
//  shopSN
//
//  Created by yisu on 16/8/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品 评价页面
 *
 *   评价 cell
 *
 */
#import "BaseTableViewCell.h"
#import "ZSmallStarView.h"

@interface ZCommonGoodsJudgeCell : BaseTableViewCell


/** 头像 */
@property (nonatomic, strong) UIImageView *iconView;

/** 用户id */
@property (nonatomic, strong) UILabel *userIdLb;

/** 评价发布时间 */
@property (nonatomic, strong) UILabel *timeLb;

/** 用户评价内容 */
@property (nonatomic, strong) UILabel *contentLb;

/** 评价配图 */
@property (nonatomic, strong) UIView *photoView;

/** 星级视图 */
@property (nonatomic, strong) ZSmallStarView *starView;


/** 商品 颜色 */
@property (nonatomic, strong) UILabel *colorLb;

/** 商品 规格 */
@property (nonatomic, strong) UILabel *rankLb;


- (void)loadJudeCellData:(ZGoodsDeal *)deal;

@end
