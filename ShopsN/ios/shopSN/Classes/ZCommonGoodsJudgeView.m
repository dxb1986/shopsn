//
//  ZCommonGoodsJudgeView.m
//  shopSN
//
//  Created by yisu on 16/8/3.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsJudgeView.h"
#import "ZCommonGoodsJudgeCell.h"

@interface ZCommonGoodsJudgeView ()<UITableViewDataSource, UITableViewDelegate>
{
    UIView *bgView;
    UIView *_topView;
    UITableView *_tableView;
    UIView *_bottomView;
    ZBigStarView *bigStarView;
    
    NSArray *_judgeArray;//评价数据
    UILabel *titleLb; //评价标题lb
}

@property (nonatomic, strong) NSMutableArray *btnArray;

@end


@implementation ZCommonGoodsJudgeView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //self.backgroundColor = [UIColor redColor];
        
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:bgView];
        
        [self initSubViews:bgView];
        
        
    }
    return self;
}



#pragma mark- initSubViews
- (void)initSubViews:(UIView *)view {
    
    //顶部视图
//    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 110)];
//    [view addSubview:_topView];
//    //    _topView.backgroundColor = __TestOColor;
//    [self addTopViewSubViews:_topView];
    
    //***修改标题视图
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 40)];
    [view addSubview:_topView];
    //    _topView.backgroundColor = __TestOColor;
    [self addTopViewSubViews:_topView];
    
    
    
    
    
    //中间评价内容 表视图
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectYH(_topView), CGRectW(view), (CGRectH(view)-CGRectH(_topView)-40))];
    [view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    //刷新控件
    [_tableView addHeaderWithTarget:self action:@selector(tableViewHeaderRefreshAction)];
    [_tableView addFooterWithTarget:self action:@selector(tableViewFootLoadAction)];
    
    
    
    
    
    //底部视图
    _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-40, CGRectW(view), 40)];
    [view addSubview:_bottomView];
    _bottomView.backgroundColor = HEXCOLOR(0xdedede);
    //[self addCommonBottomViewSubViews:_bottomView];
    
}

//加载顶部视图 子控件
- (void)addTopViewSubViews:(UIView *)view {
    
    //背景
    UIView *topBGView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, CGRectW(view)-20, CGRectH(view))];
    [view addSubview:topBGView];
    //    topBGView.backgroundColor = __TestOColor;
    
    
    
//    //标题
//    titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 70, 15)];
//    [topBGView addSubview:titleLb];
//    //    titleLb.backgroundColor = __TestGColor;
//    titleLb.font = MFont(14);
//    titleLb.textColor = HEXCOLOR(0x333333);
//    titleLb.text = @"97.5%好评";//test
//    
//    //星级视图
//    
//    bigStarView = [[ZBigStarView alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), 15, 80, 18)];
//    //bigStarView.starImage = MImage(@"xx_l.png");
//    
//    
//    
//    [topBGView addSubview:bigStarView];
//    
//    //test
//    [bigStarView setStar:5];
//    
//    //评价选择 按钮视图
//    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(titleLb)+10, CGRectW(topBGView), 65)];
//    [topBGView addSubview:buttonView];
//    //    buttonView.backgroundColor = __TestOColor;
//    [self buttonViewAddSubView:buttonView];//添加子控件
    
    
    titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, CGRectW(topBGView), 30)];
    [topBGView addSubview:titleLb];
    //    titleLb.backgroundColor = __TestGColor;
    titleLb.font = MFont(14);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.text =  @"暂无评价内容";//默认无评价内容
    
    
    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, CGRectH(view)-1, CGRectW(view)-10, 1)];
    [view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}

//添加按钮视图 子控件
- (void)buttonViewAddSubView:(UIView *)view {
    
    _btnArray = [NSMutableArray array];
    
    //测试 全部(1075) 好评(1048) 中评(8) 差评(9) 有图(28)
    NSArray *btnArray = @[@"全部(1075)", @"好评(1048)", @"中评(8)", @"差评(9)",@"有图(28)"];
    
    for (int i=0; i<btnArray.count; i++) {
        
        NSString *name = btnArray[i];
        static UIButton *recordBtn =  nil;
        //* 改为自定义类型，否则按钮背景尺寸不对
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect rect = [name boundingRectWithSize:CGSizeMake(CGRectW(view)-20, 25) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:btn.titleLabel.font} context:nil];
        if (i == 0)
        {//第一行 第一个按钮
            btn.frame =CGRectMake(0, 0, rect.size.width, rect.size.height+5);//  高度+5 调整宽度 +16取消
            btn.selected = YES;
            //            btn.backgroundColor = __DefaultColor;
            //            btn.layer.borderColor = __DefaultColor.CGColor;
        }
        else{//其他按钮
            CGFloat yuWidth = CGRectW(view) - 25-recordBtn.frame.origin.x -recordBtn.frame.size.width;
            if (yuWidth >= rect.size.width) {//第一排 后面按钮
                btn.frame =CGRectMake(recordBtn.frame.origin.x +recordBtn.frame.size.width + 10, recordBtn.frame.origin.y, rect.size.width, rect.size.height+5);
            }else{//第二排高度调整 +5
                btn.frame =CGRectMake(0, recordBtn.frame.origin.y+recordBtn.frame.size.height+10, rect.size.width, rect.size.height+5);
            }
            
        }
        
        //        NSLog(@"btn%d:w:%f===h:%f", i, btn.frame.size.width, btn.frame.size.height);
        //btn.backgroundColor = [UIColor yellowColor];
        
        [_btnArray addObject:btn];
        
        
        
        btn.layer.borderWidth = 1.2f;
        if (i == 0) {
            btn.backgroundColor = __DefaultColor;
            btn.layer.borderColor = __DefaultColor.CGColor;
        }else{
            btn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
        }
        
        btn.layer.cornerRadius = 5.0f;
        btn.titleLabel.font = MFont(12);
        [btn setTitle:name forState:BtnNormal];
        [btn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        [btn setTitleColor:HEXCOLOR(0xffffff) forState:BtnStateSelected];
        [view addSubview:btn];
        
        recordBtn = btn;
        recordBtn.tag = 170 + i;
        [recordBtn addTarget:self action:@selector(chooseJudgeTypeAction:) forControlEvents:BtnTouchUpInside];
    }
}






#pragma mark - ==== 按钮触发方法 =====

//底部按钮触发方法
//- (void)bottmBtnAction:(UIButton *)sender {
//    [self.delegate commonGoodsJudgeBottomBtnAction:sender];
//}




//选择评价类型 按钮
- (void)chooseJudgeTypeAction:(UIButton *)sender {
    
    //判断按钮是否是点击的那个
    sender.selected = YES;
    
    for (UIButton *button in _btnArray) {
        if (button == sender) {
            button.backgroundColor = __DefaultColor;
            button.layer.borderColor = __DefaultColor.CGColor;
            continue;
        } else{
            button.selected = NO;
            button.backgroundColor = [UIColor clearColor];
            button.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
        }
        
        
    }
    
    
    switch (sender.tag - 170) {
        case 0:
        {
            NSLog(@"选择普通商品评价页面 全部评价");
        }
            break;
            
        case 1:
        {
            NSLog(@"选择普通商品评价页面 好评");
        }
            break;
        case 2:
        {
            NSLog(@"选择普通商品评价页面 中评");
        }
            break;
        case 3:
        {
            NSLog(@"选择普通商品评价页面 差评");
        }
            break;
        case 4:
        {
            NSLog(@"选择普通商品评价页面 有图");
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark - ===== TableView DataSource and Delegate =====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _judgeArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZCommonGoodsJudgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CGJudgeCell"];
    if (!cell) {
        cell = [[ZCommonGoodsJudgeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CGJudgeCell"];
    }
    
    ZGoodsDeal *deal = _judgeArray[indexPath.row];
//    cell.userIdLb.text = deal.goodsJudgeName;
//    cell.timeLb.text   = deal.goodsJudgeTime;
//    cell.contentLb.text = deal.goodsJudgeContent;
//    cell.starView.hidden = YES;
    
    [cell loadJudeCellData:deal];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //return 192+20;
    
    return 110;
}



#pragma mark - 下拉刷新
-(void)tableViewHeaderRefreshAction {
    
    [self refreshTravelGoodsData];
    
}

//刷新页面
- (void)refreshTravelGoodsData {
    NSLog(@"普通商品 评价页面 下拉刷新");
    [_tableView headerEndRefreshing];
}



#pragma mark - 上拉加载更多
- (void)tableViewFootLoadAction {
    [self loadMoreTravelGoodsData];
}


//加载更多
- (void)loadMoreTravelGoodsData {
    NSLog(@"普通商品 评价页面 加载更多");
    [_tableView footerEndRefreshing];
}



- (void)loadCommonJudgeData:(NSArray *)array {
    
    //评价标题
    titleLb.text = [NSString stringWithFormat:@"评价数量：%d条",array.count];
    
    //
    
    _judgeArray = array;
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
