//
//  ZGoodsListViewController.h
//  shopSN
//
//  Created by chang on 16/6/26.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品列表页面
 *
 *  主控制器
 *
 */

#import "BaseViewController.h"

@interface ZGoodsListViewController : BaseViewController

/** 商品列表 项目名称*/
@property (nonatomic, copy) NSString *itemName;


@end
