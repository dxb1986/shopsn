//
//  ZGoodsListViewController.m
//  shopSN
//
//  Created by chang on 16/6/26.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZGoodsListViewController.h"
#import "ZGoodsSearchViewController.h"//商品搜索

//subViews
#import "ZGoodsListSetionView.h"//项目选择功能视图
#import "ZGoodsSiftView.h"//筛选视图
#import "ZGoodsCell.h" //Cell

@interface ZGoodsListViewController ()<ZGoodsListSetionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

/** 主页面交互菜单视图 */
@property (nonatomic ,strong) UIView *topMenuView;

/** 主页面交互菜单 按钮视图 */
@property (nonatomic ,strong) UIView *menuBtnView;

/** 主页面 商品列表主视图 */
@property (nonatomic ,strong) UIView *goodsListMainView;

/** 主页面 商品列表图 */
@property (nonatomic ,strong) UICollectionView *goodsListCollectionView;

/** 主页面交互菜单 筛选视图 */
@property (nonatomic ,strong) ZGoodsSiftView *siftView;



@end

@implementation ZGoodsListViewController

#pragma mark - ==== 页面设置 =====
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    //self.tabBarController.tabBar.hidden =YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    //self.tabBarController.tabBar.hidden =NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavi];// 自定义导航栏
    
    [self initMainView];//初始化主页面视图
    
}


#pragma mark - 自定义导航栏
// 自定义导航栏
- (void)initNavi {
    BaseView *topView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 64)];
    topView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:topView];
    
    
    UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 25, __kWidth, 40)];
    //subView.backgroundColor = __DefaultColor;
    [topView addSubview:subView];
    
    
    
    //左侧分类 按钮
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, 10, 20, 20)];
    //backBtn.backgroundColor = __TestOColor;
    [backBtn setBackgroundImage:MImage(@"fanhui.png") forState:BtnNormal];
    [backBtn addTarget:self action:@selector(backAciton) forControlEvents:BtnTouchUpInside];
    [subView addSubview:backBtn];
    
    
    //右侧交互 按钮
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(__kWidth-15-20, 10, 20, 20)];
    //    rightBtn.backgroundColor = __TestOColor;
    rightBtn.titleLabel.font = MFont(15);
    //    [rightBtn setTitle:@"交互关" forState:BtnNormal];//等待图片
    //    [rightBtn setTitle:@"交互开" forState:BtnStateSelected];
    [rightBtn setImage:MImage(@"yuan.png") forState:BtnNormal];
    [rightBtn setImage:MImage(@"yuan.png") forState:BtnStateSelected];
    //[rightBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 0)];
    [rightBtn setTitleColor:__DefaultColor forState:BtnNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAciton:) forControlEvents:BtnTouchUpInside];
    [subView addSubview:rightBtn];
    
    
    //中间搜索框
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(CGRectXW(backBtn)+20, 5, CGRectW(subView)-CGRectW(backBtn)-CGRectW(rightBtn)-30-40, 30)];
    searchView.layer.borderWidth = 1;
    searchView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    searchView.backgroundColor = HEXCOLOR(0xffffff);
    searchView.layer.cornerRadius = 5;
    [subView addSubview:searchView];
    [self createSearchSubView:searchView];
    
    //搜索按钮
    UIButton *searchBtn = [[UIButton alloc] init];
    searchBtn.frame = searchView.frame;
    [searchBtn addTarget:self action:@selector(searchBtnAction) forControlEvents:BtnTouchUpInside];
    [subView addSubview:searchBtn];
    
    
    
}



//自定义 搜索视图 内部控件
- (void)createSearchSubView:(UIView *)btnView {
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7.5, 15, 15)];
    imageView.image = MImage(@"sousuo");
    //imageView.backgroundColor = __TestOColor;
    [btnView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(imageView)+5, 5, 150, CGRectH(btnView)-10)];
    //label.backgroundColor = __TestGColor;
    label.font = MFont(14);
    label.textColor = HEXCOLOR(0x999999);
    label.text = _itemName;
    [btnView addSubview:label];
    
    
}

#pragma mark - 初始化 主视图
//初始化 主视图
- (void)initMainView {
    
    //背景图 color (240 240 240)   (f0f0f0)
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    bgView.backgroundColor = HEXCOLOR(0xf0f0f0);
    [self.view addSubview:bgView];
    
    
    //交互 菜单栏
    _topMenuView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 50)];
    _topMenuView.backgroundColor = __DefaultColor;
    [bgView addSubview:_topMenuView];
    [self addTopMenuSubView:(_topMenuView)];//添加交互菜单视图
    _topMenuView.hidden = YES;
    
    
    
    //商品列表主视图
    _goodsListMainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), CGRectH(bgView))];
    //_goodsListMainView.backgroundColor = __TestOColor;
    [bgView addSubview:_goodsListMainView];
    
    [self addGoodsListSubView:_goodsListMainView];//添加其子视图
    
    
}


//交互菜单视图
- (void)addTopMenuSubView:(UIView *)menuView {
    
    for (int i=0; i<4; i++) {
        _menuBtnView = [[UIView alloc] initWithFrame:CGRectMake(i*(CGRectW(menuView)/4)+1, 0, CGRectW(menuView)/4-4, CGRectH(menuView))];
        //_menuBtnView.backgroundColor = [UIColor redColor];
        [menuView addSubview:_menuBtnView];
        
        //1 imageView
        UIImageView *btnIV = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectW(_menuBtnView)-20)/2, 10, 20, 20)];
        //btnIV.backgroundColor = __TestOColor;
        [_menuBtnView addSubview:btnIV];
        
        //2 name
        UILabel *btnLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(btnIV), CGRectW(_menuBtnView), 20)];
        btnLb.font = MFont(10);
        btnLb.textColor = HEXCOLOR(0xffffff);
        btnLb.textAlignment = NSTextAlignmentCenter;
        [_menuBtnView addSubview:btnLb];
        
        switch (i) {
            case 0:
            {
                btnIV.image = MImage(@"home02.png");
                btnLb.text  = @"首页";
            }
                break;
                
            case 1:
            {
                btnIV.image = MImage(@"fbxs02.png");
                btnLb.text  = @"分类搜索";
                _menuBtnView.backgroundColor = HEXCOLOR(0x1d9542);
            }
                break;
                
            case 2:
            {
                btnIV.image = MImage(@"buy02.png");
                btnLb.text  = @"购物车";
            }
                break;
                
            case 3:
            {
                btnIV.image = MImage(@"me02.png");
                btnLb.text  =[NSString stringWithFormat:@"我的%@",simpleTitle];
            }
                break;
                
            default:
                break;
        }
        
        //btn
        UIButton *topBtn = [[UIButton alloc] initWithFrame:_menuBtnView.frame];
        [menuView addSubview:topBtn];
        topBtn.tag = 30 + i;
        //topBtn.backgroundColor = HEXCOLOR(0x1d9542);
        
        [menuView addSubview:topBtn];
        
        [topBtn addTarget:self action:@selector(topMenuBtnAciton:) forControlEvents:BtnTouchUpInside];
        
    }
    
    
    
}


//添加 商品列表子视图
- (void)addGoodsListSubView:(UIView *)mainView {
    
    //1 选择功能视图
    ZGoodsListSetionView *sectionView = [[ZGoodsListSetionView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(mainView), 40)];
    [mainView addSubview:sectionView];
    sectionView.delegate = self;
    
    //2 产品列表视图
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    CGRect frame = CGRectMake(0, CGRectYH(sectionView), CGRectW(mainView), CGRectH(mainView)-40);
    _goodsListCollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:flowLayout];
    _goodsListCollectionView.backgroundColor = HEXCOLOR(0xdedede);
    _goodsListCollectionView.dataSource = self;
    _goodsListCollectionView.delegate   = self;
    [mainView addSubview:_goodsListCollectionView];
    
    //注册cell
    [_goodsListCollectionView registerClass:[ZGoodsCell class] forCellWithReuseIdentifier:@"GoodsCell"];
}


#pragma mark - ===== 按钮方法 =====

//导航栏左侧 返回按钮 触发方法
- (void)backAciton {
    NSLog(@"返回前页面");
    
    if (self.navigationController.viewControllers.count>1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



//导航栏右侧 交互按钮 触发方法
- (void)rightBtnAciton:(UIButton *)btn {
    NSLog(@"选择交互");
    
    if (btn.selected == YES) {
        _topMenuView.hidden = YES;
        
        
        CGRect rect_mc   = _goodsListMainView.frame;
        rect_mc.origin.y -= 50;
        [_goodsListMainView setFrame:rect_mc];
        
        btn.selected = NO;
        
        
    }else {
        _topMenuView.hidden = NO;
        
        
        CGRect rect_mc   = _goodsListMainView.frame;
        rect_mc.origin.y += 50;
        [_goodsListMainView setFrame:rect_mc];
        
        btn.selected = YES;
        
        
        
    }
}



//导航栏中间 搜索按钮 触发方法
- (void)searchBtnAction {
    NSLog(@"跳转至 搜索页面");
    ZGoodsSearchViewController *vc = [[ZGoodsSearchViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}



//交互视图 按钮触发方法
- (void)topMenuBtnAciton:(UIButton *)btn {
    
    switch (btn.tag - 30) {
        case 0:
        {
            NSLog(@"跳转至 首页页面");
            
        }
            break;
            
        case 1:
        {
            NSLog(@"跳转至 分类搜索页面");
            
        }
            break;
            
        case 2:
        {
            NSLog(@"跳转至 购物车页面");
            
        }
            break;
            
        case 3:
        {
            NSLog([NSString stringWithFormat:@"跳转至 我的%@页面",simpleTitle]);
            
        }
            break;
            
        default:
            break;
            
    }
}



#pragma mark - ZGoodsListSectionViewDelegate

- (void)showSiftView:(id)sender {
    [self.siftView setHidden:NO];
    [self siftView];
}



- (ZGoodsSiftView *)siftView {
    if (!_siftView) {

        _siftView = [[ZGoodsSiftView alloc] initWithFrame:CGRectMake(0, 64+40, __kWidth, __kHeight-64-40)];
        //_siftView.g_chooseBtn.titleLabel.text = _itemName;
        //_siftView.gs_categoryName = _itemName; //***获取该值 是在初始化后，已经晚了
        [self.navigationController.view addSubview:_siftView];
        
    }
    return _siftView;
}



#pragma mark - goodsListCollectionView DataSource and Delegate
//每组 多少items
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

//Cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsCell" forIndexPath:indexPath];

//    NSLog(@"x:%f===%f",cell.frame.origin.x, cell.frame.origin.y);
//    NSLog(@"w:%f===h:%f",cell.frame.size.width, cell.frame.size.height);
    
    
    return cell;
}


//内容距离屏幕边缘的距离 参数顺序是top,left,bottom,right
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(5.0f, 0, 5.0f, 0);
    
}

//x 间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

//y 间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}


//item 宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //横向2 纵向3
    return CGSizeMake((CGRectW(_goodsListCollectionView)-2)/2-5/2, CGRectW(_goodsListCollectionView)/2+100);
    
}

//选中触发方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选择的是: 第%ld组 第%ld个",(long)indexPath.section, (long)indexPath.row);
}



#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
