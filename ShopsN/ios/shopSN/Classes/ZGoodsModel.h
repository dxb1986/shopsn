//
//  ZGoodsModel.h
//  shopSN
//
//  Created by yisu on 16/9/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员商品 相关页面
 *
 *  商品模型类
 *
 */

#import <Foundation/Foundation.h>

@interface ZGoodsModel : NSObject

/** 商品模型类 商品id */
@property (copy, nonatomic) NSString *goodsID;

/** 商品模型类 商品标题 */
@property (copy, nonatomic) NSString *goodsTitle;

/** 商品模型类 商品图片地址 */
@property (copy, nonatomic) NSString *goodsPicUrl;

/** 商品模型类 商品原价 */
@property (copy, nonatomic) NSString *goodsOriginalPrice;

/** 商品模型类 商品现价 */
@property (copy, nonatomic) NSString *goodsNowPrice;

/** 商品模型类 商品积分 */
@property (copy, nonatomic) NSString *goodsPoints;


@end
