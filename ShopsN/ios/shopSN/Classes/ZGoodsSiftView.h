//
//  ZGoodsSiftView.h
//  shopSN
//
//  Created by yisu on 16/6/27.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品列表页面
 *
 *  筛选视图
 *
 */
#import "BaseView.h"

@interface ZGoodsSiftView : BaseView

/** 筛选视图 类别名称 */
@property (nonatomic, copy) NSString *gs_categoryName;

@property (nonatomic, strong) UIButton *g_chooseBtn;



//- (void)showSiftView;

@end
