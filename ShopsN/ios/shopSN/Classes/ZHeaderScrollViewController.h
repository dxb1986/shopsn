//
//  ZHeaderScrollViewController.h
//  shopSN
//
//  Created by yisu on 16/6/15.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 Header视图控制器
 *
 *  顶部滚动视图
 *
 */

#import "BaseViewController.h"

@class ZHeaderScrollViewController;
@protocol ZHeaderScrollViewControllerDelegate <NSObject>

- (void)choosePicNum:(NSInteger)picNum;

@end

@interface ZHeaderScrollViewController : BaseViewController

@property (nonatomic) CGSize contentSize;
@property (strong, nonatomic) NSArray *headerImageArray;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (copy, nonatomic)   NSString *picUrl;

@property (weak, nonatomic) id<ZHeaderScrollViewControllerDelegate>delegate;

//- (void)reloadPicData:(NSArray *)array;

- (void)showScrollerViewWithPicURL:(NSString *)url;

@end
