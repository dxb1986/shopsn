//
//  ZHeaderScrollViewController.m
//  shopSN
//
//  Created by yisu on 16/6/15.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHeaderScrollViewController.h"
#import "ZHeadImage.h"
@interface ZHeaderScrollViewController ()<UIScrollViewDelegate>
{
    
}

/** 首页头部滚定视图定时器 */
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ZHeaderScrollViewController

- (void)viewWillAppear:(BOOL)animated {
//    开启定时器
    [_timer setFireDate:[NSDate distantPast]];
}

- (void)viewWillDisappear:(BOOL)animated {
    //关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self showScrollView];
    
    
}

- (void)showScrollView {
    
    
    //添加滚动视图
    self.scrollView = [[UIScrollView alloc] init];
    
    CGRect frame = CGRectZero;
    
    frame.size = self.contentSize;
    self.scrollView.frame = frame;
    [self.view addSubview:self.scrollView];
    
    UIImageView *imageView;
    for (int i=0 ; i<self.headerImageArray.count; i++) {
        ZHeadImage *headImage = self.headerImageArray[i];
        
        
        if (headImage.imageName.length > 10) {
            
            imageView = [[UIImageView alloc] init];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@",_picUrl, headImage.imageName];
            NSLog(@"urlStr:%@",urlStr);
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:MImage(@"banner01.png")];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }else{
            UIImage *image = MImage(headImage.imageName);
            imageView = [[UIImageView alloc] initWithImage:image];
        }
        
        
        
        
        CGRect frame = CGRectZero;
        frame.size = self.contentSize;
        frame.origin.x = frame.size.width *i;
        imageView.frame = frame;
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;

        [self.scrollView addSubview:imageView];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:imageView.frame];
        [self.scrollView addSubview:btn];
        btn.tag = 400 + i;
        [btn addTarget:self action:@selector(choosePicButtonAction:) forControlEvents:BtnTouchUpInside];
    }
    
    
    //因为不需要竖向滚动，所以高度只要小于scrollview的高度就可以
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * self.headerImageArray.count, 0);
    self.scrollView.showsHorizontalScrollIndicator = NO;//不显示横向滚动条
    self.scrollView.showsVerticalScrollIndicator   = NO;//不显示竖向滚动条
    self.scrollView.pagingEnabled = YES;//以翻页形式进行滚动
    
    //添加页面控制器
    self.pageControl = [[UIPageControl alloc] init];
    [self.view addSubview:self.pageControl];
    
    self.pageControl.frame = CGRectMake(0, self.scrollView.frame.size.height - 10, self.scrollView.frame.size.width, 10);//设置坐标位置
    //self.pageControl.backgroundColor = [UIColor orangeColor];
    self.pageControl.numberOfPages = self.headerImageArray.count;//设置圆点的个数
    self.pageControl.pageIndicatorTintColor = HEXCOLOR(0x828282);//设置没有被选中时圆点的颜色
    self.pageControl.currentPageIndicatorTintColor = __DefaultColor;//设置选中时圆点的颜色
    self.pageControl.userInteractionEnabled = NO;//关闭分页控件的用户交互功能
    
    self.scrollView.delegate = self;
    
    
    //开启定时器
    [_timer setFireDate:[NSDate distantPast]];
    
    
    //scrollView动画播放
    _timer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(playingImage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];

}

- (void)showScrollerViewWithPicURL:(NSString *)url {
    //添加滚动视图
    self.scrollView = [[UIScrollView alloc] init];
    
    CGRect frame = CGRectZero;
    
    frame.size = self.contentSize;
    self.scrollView.frame = frame;
    [self.view addSubview:self.scrollView];
    
    UIImageView *imageView;
    for (int i=0 ; i<self.headerImageArray.count; i++) {
        ZHeadImage *headImage = self.headerImageArray[i];
        
        
        if (headImage.imageName.length > 10) {
            
            imageView = [[UIImageView alloc] init];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@",url, headImage.imageName];
            //NSLog(@"urlStr:%@",urlStr);
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:MImage(@"zp_k.png")];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            
        }else{
            UIImage *image = MImage(headImage.imageName);
            imageView = [[UIImageView alloc] initWithImage:image];
        }
        
        
        
        
        CGRect frame = CGRectZero;
        frame.size = self.contentSize;
        frame.origin.x = frame.size.width *i;
        imageView.frame = frame;
        
        
        
        
        [self.scrollView addSubview:imageView];
    }
    
    
    //因为不需要竖向滚动，所以高度只要小于scrollview的高度就可以
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * self.headerImageArray.count, 0);
    self.scrollView.showsHorizontalScrollIndicator = NO;//不显示横向滚动条
    self.scrollView.showsVerticalScrollIndicator   = NO;//不显示竖向滚动条
    self.scrollView.pagingEnabled = YES;//以翻页形式进行滚动
    
    //添加页面控制器
    self.pageControl = [[UIPageControl alloc] init];
    [self.view addSubview:self.pageControl];
    
    self.pageControl.frame = CGRectMake(0, self.scrollView.frame.size.height - 22, self.scrollView.frame.size.width, 30);//设置坐标位置
    //self.pageControl.backgroundColor = [UIColor orangeColor];
    self.pageControl.numberOfPages = self.headerImageArray.count;//设置圆点的个数
    self.pageControl.pageIndicatorTintColor = HEXCOLOR(0x828282);//设置没有被选中时圆点的颜色
    self.pageControl.currentPageIndicatorTintColor = __DefaultColor;//设置选中时圆点的颜色
    self.pageControl.userInteractionEnabled = NO;//关闭分页控件的用户交互功能
    
    self.scrollView.delegate = self;
    
    
    //开启定时器
    [_timer setFireDate:[NSDate distantPast]];
    
    
    //scrollView动画播放
    _timer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(playingImage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

#pragma mark - scrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //self.pageControl.currentPage = self.scrollView.contentOffset.x / CGRectW(self.view);
    CGPoint point = self.scrollView.contentOffset;//取得偏移量
    NSInteger index = round(point.x / self.scrollView.frame.size.width);
    self.pageControl.currentPage = index;//设置分页控制器的当前页面
    
    
    
}

#pragma mark - scrollView 动画播放
- (void)playingImage {
    int i = 1;
    if (self.pageControl.currentPage == self.headerImageArray.count - 1) {
        self.pageControl.currentPage = 0;
        i = 0;
    }

    
    self.scrollView.contentOffset = CGPointMake((self.pageControl.currentPage + i) * self.view.frame.size.width, 0);
    //NSLog(@"index:%d",i);
    //NSLog(@"contentoffset==x:%lf y:%lf",self.scrollView.contentOffset.x, self.scrollView.contentOffset.y);
}

//- (void)reloadPicData:(NSArray *)array {
//    
//
//    
//    
//    self.headerImageArray = array;
//    
//    [self.scrollView removeFromSuperview];
//    [self.pageControl removeFromSuperview];
//    
//        
//    self.scrollView = [[UIScrollView alloc] init];
//    self.scrollView.backgroundColor = __TestOColor;
////    CGRect frame = CGRectZero;
////        
////    frame.size = self.contentSize;
////    self.scrollView.frame = frame;
////    [self.view addSubview:self.scrollView];
//    
//    [self showScrollView];
//    
//}


#pragma mark - 按钮触发方法
- (void)choosePicButtonAction:(UIButton *)button {
    //NSLog(@"tag:%ld",(long)button.tag);
    //ZHeadImage *headImage = self.headerImageArray[button.tag - 400];
    //NSLog(@"%@",headImage.imageName);
    [self.delegate choosePicNum:button.tag-400];
}

#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
