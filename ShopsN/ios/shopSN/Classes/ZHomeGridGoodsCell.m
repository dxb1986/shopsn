//
//  ZHomeGridGoodsCell.m
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGridGoodsCell.h"

@implementation ZHomeGridGoodsCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = HEXCOLOR(0xf1f1f1);
       // BaseView *bgView = [[BaseView alloc] initWithFrame:CGRectMake(1, 1, frame.size.width-2, frame.size.height-2)];
        BaseView *bgView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        //bgView.backgroundColor = __TestGColor;
        [self.contentView addSubview:bgView];
        
        [self initSubViews:bgView];
        
        
        
    }
    return self;
}


- (void)initSubViews:(UIView *)view {
    
    // 商品图片 宽高设计给出 720*460
    //_goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 155)];
    _goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view)-20)];
    [view addSubview:_goodsIV];
    _goodsIV.backgroundColor = __TestOColor;
    
    
    //*** 图片数据包含下面内容
//    // 商品类别
//    _goodsTypeLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(_goodsIV)+5, 150, 20)];
//    [view addSubview:_goodsTypeLb];
////    _goodsTypeLb.backgroundColor = __TestOColor;
//    _goodsTypeLb.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
//    _goodsTypeLb.textColor = HEXCOLOR(0x333333);
////    _goodsTypeLb.textAlignment = NSTextAlignmentCenter;
//    
//    
//    // 商品描述
//    _goodsDetailLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(_goodsTypeLb), 200, 15)];
//    [view addSubview:_goodsDetailLb];
//    //_goodsDetailLb.backgroundColor = __TestGColor;
//    _goodsDetailLb.numberOfLines = 0;
//    _goodsDetailLb.font = MFont(10);
//    _goodsDetailLb.textColor = HEXCOLOR(0x666666);
//    //_goodsDetailLb.textAlignment = NSTextAlignmentCenter;
//    
//    
//    // 商品价格
//    _goodsPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)-70, CGRectYH(_goodsIV)+5, 60, 20)];
//    [view addSubview:_goodsPriceLb];
//    //_goodsPriceLb.backgroundColor = __TestGColor;
//    _goodsPriceLb.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
//    _goodsPriceLb.textColor = __MoneyColor;
//    //_goodsPriceLb.textAlignment = NSTextAlignmentCenter;
//    
//    
//    
//    // 商品积分
//    _goodsPointsLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)-70, CGRectYH(_goodsTypeLb), 60, 15)];
//    [view addSubview:_goodsPointsLb];
//    //_goodsPointsLb.backgroundColor = __TestGColor;
//    _goodsPointsLb.numberOfLines = 0;
//    _goodsPointsLb.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
//    _goodsPointsLb.textColor = __MoneyColor;
//    //_goodsPointsLb.textAlignment = NSTextAlignmentCenter;
//    
//    
    
    //间隔20
    //UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(_goodsDetailLb)+5, CGRectW(view), 10)];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(_goodsIV)+10, CGRectW(view), 10)];
    [view addSubview:lineView];
    lineView.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    //底线
    UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-1, CGRectW(view), 1)];
    linIV.backgroundColor = HEXCOLOR(0xdedede);
    [view addSubview:linIV];
    
    //测试文字 澳洲直送新鲜红酒 保儿加营养素完整均衡营养粉  ￥179
//    _goodsTypeLb.text  = @"澳洲直送新鲜红酒";
//    _goodsDetailLb.text = @"澳大利亚原装进口盒装史密斯葡萄酒";
//    //_goodsPriceLb.text  = @"￥179元";
//    _goodsPointsLb.text = @"30积分返利";
    
    
    //[self setLabeltextAttributes:_goodsPriceLb.text];
    
    
}

// 设置指定字体大小和颜色
//- (void)setLabeltextAttributes:(NSString *)text {
//    
//    NSUInteger length = [text length];
//    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
//    //第一个字符大小
//    //[attri addAttribute:NSFontAttributeName value:MFont(9) range:NSMakeRange(0, 1)];
//    //最后一个字符颜色
//    [attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(length-1, 1)];
//    
//    [_goodsPriceLb setAttributedText:attri];
//    
//}
//
//


@end
