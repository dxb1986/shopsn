//
//  ZHomeGridHeader1.h
//  shopSN
//
//  Created by yisu on 16/8/10.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  滚动广告和专区选择
 *
 */
#import <UIKit/UIKit.h>
#import "YFRollingLabel.h"


@class ZHomeGridHeader1;
@protocol ZHomeGridHeader1Delegate <NSObject>

/** 代理方法 选择专区 */
- (void)choosePrefectureButton:(UIButton *)button withIndex:(NSInteger)index withChooseArray:(NSArray *)array;

- (void)chooseScrollViewPicNum:(NSInteger)picNum;

@end


@interface ZHomeGridHeader1 : UICollectionReusableView

/** 专区标题数组 */
@property (nonatomic, strong) NSArray *headerTitleArray;

/** 头部广告图片数组 */
@property (nonatomic, strong) NSArray *headerImageArray;

@property (nonatomic, assign) BOOL isGoods;

/** 头部滚动公告label */
@property (nonatomic, strong) YFRollingLabel *footerLb;

@property (nonatomic, strong) UIScrollView *scrollView;//滚动公告

//获取数据
- (void)getDataWithTitleArray:(NSArray *)titleArray andImageArray:(NSArray *)headerImageArray;

/**首页页面 secton0 头部视图代理方法 */
@property (nonatomic, weak) id<ZHomeGridHeader1Delegate>delegate;

@end
