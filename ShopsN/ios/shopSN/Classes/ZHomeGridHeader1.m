//
//  ZHomeGridHeader1.m
//  shopSN
//
//  Created by yisu on 16/8/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGridHeader1.h"
#import "ZHeaderScrollViewController.h"
#import "ZHeadImage.h"


@interface ZHomeGridHeader1 ()<ZHeaderScrollViewControllerDelegate>
{
    UIView *bgView;
    UIView *bannerView;
    UIView *chooseView;//专区选择视图
    UIView *chooseBtnView;//区域选择按钮视图
    
    
    NSMutableArray *_btnArr;
    
    NSInteger originalTag;
    NSArray *_chooseHeaderImageArray;
    
    //CGFloat picHight;
    
    NSString *messageStr;
}

/** 首页表格header中的顶部滚动视图 */
@property (nonatomic, strong) ZHeaderScrollViewController *headerScrollViewController;



@property (nonatomic, assign) CGFloat picHight;//图片高度

@property (nonatomic, assign) CGFloat chooseBtnTopSpace;//专区选择按钮上边距

@end

@implementation ZHomeGridHeader1



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:bgView];
        //bgView.backgroundColor = __TestGColor;
        
        _btnArr = [NSMutableArray array];
        
        
        //[self initSubViews:bgView];
        
        //[self getMessageData];
        
        
    }
    return self;
}

//获取数据
- (void)getDataWithTitleArray:(NSArray *)titleArray andImageArray:(NSArray *)headerImageArray  {
    
    //1 获取专区数量和内容
    _headerTitleArray = titleArray;
    
    //2 获取广告图片
    [self addHeaderImage:headerImageArray];
    
    //移除上次加载的 滚动广告视图和专区选择视图
    [bannerView removeFromSuperview];
    [self.headerScrollViewController.view removeFromSuperview];
    [chooseView removeFromSuperview];
    [_scrollView removeFromSuperview];
    [_footerLb removeFromSuperview];
    
    
    //3 确定滚动图片高度
//    if (_isGoods) {
//        if (__kHeight > __k5Height) {
//            _picHight = 235;
//        }else {
//            _picHight = 200;
//        }
//        //_chooseBtnTopSpace = 90;
//        _footerLb.hidden = NO;
//        _scrollView.hidden = NO;
//    }else {
        if (__kHeight > __k5Height) {
            _picHight = 170;
        }else {
            _picHight = 145;
        }
        //_chooseBtnTopSpace = 50;
        _footerLb.hidden = YES;
        _scrollView.hidden = YES;
        
//    }

    
    [self initSubViews:bgView];
    
    //加载滚动公告内容
//    if (IsNilString(messageStr)) {
//        [self getMessageData];
//    }else {
//        [self addScrollLabelWithMessage:messageStr];
//    }

    
}


- (void)getMessageData {
    
    [ZHttpRequestService GETHome:@"gonggao" withParameters:nil success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            NSString *str = jsonDic[@"data"];
            //NSLog(@"%@",str);
            messageStr = str;
            [self addScrollLabelWithMessage:str];
        }
    } failure:^(NSError *error) {
        //
    } animated:NO withView:nil];
}

//加载广告图片
- (void)addHeaderImage:(NSArray *)array {
    
//    NSMutableArray *mutArray = [NSMutableArray array];
//
//    for (int i=0; i<array.count; i++) {
//        
//        
//        ZHeadImage *headImage = [[ZHeadImage alloc] init];
//        headImage.imageName = array[i];
//        [mutArray addObject:headImage];
//    }
//    _chooseHeaderImageArray = mutArray;
    
    
    //*** 重写
    ZHomePicModel *pic = [[ZHomePicModel alloc] init];
    NSMutableArray *mutArray = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        
        
        pic = array[i];
        NSString *imageName = pic.bannerUrl;
        ZHeadImage *headImage = [[ZHeadImage alloc] init];
        headImage.imageName = imageName;
        [mutArray addObject:headImage];
    }
    _chooseHeaderImageArray = mutArray;
    
}



//添加子视图
- (void)initSubViews:(UIView *)view {
    

    
    //滚动广告视图
    bannerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), _picHight)];
    [view addSubview:bannerView];
    //bannerView.backgroundColor = __TestGColor;
    [self addBannerViewSubView:bannerView];
    
    
    
    
    //专区选择 color (143,143,143) (0x8f8f8f)
    //chooseView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(bannerView), CGRectW(view), 30)];
    chooseView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectYH(bannerView)+10, CGRectW(view), 50)];
    [view addSubview:chooseView];
    chooseView.backgroundColor = HEXCOLOR(0x8f8f8f);
    [self addChooseViewSubView:chooseView];
    chooseView.hidden = YES;

    
    //滚动公告
    _scrollView = [[UIScrollView  alloc] initWithFrame:CGRectMake(0, CGRectH(view)-40, CGRectW(view), 40)];
    [view addSubview:_scrollView];
    _scrollView.backgroundColor = [UIColor lightGrayColor];
    _scrollView.contentSize = CGSizeMake(self.frame.size.width, 1000);
    _scrollView.showsVerticalScrollIndicator = NO;//竖向滚动条不显示
    _scrollView.hidden = YES;
    
    //[self getMessageData];//加载滚动公告内容
    
    
}

- (void)addScrollLabelWithMessage:(NSString *)message {
    [_footerLb removeFromSuperview];//防止重复
    
    NSArray *textArray = @[message];
    
    _footerLb = [[YFRollingLabel alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 40) textArray:textArray font:MFont(13) textColor:[UIColor redColor]];
    [_scrollView addSubview:_footerLb];
    //_footerLb.backgroundColor = [UIColor lightGrayColor];
    //_footerLb.backgroundColor = [UIColor orangeColor];
    _footerLb.speed = 1;
    [_footerLb setOrientation:RollingOrientationLeft];
    [_footerLb setInternalWidth:_footerLb.frame.size.width / 3];
    
    //Label Click Event Using Block
    _footerLb.labelClickBlock = ^(NSInteger index){
        NSString *text = [textArray objectAtIndex:index];
        NSLog(@"You Tapped item:%li , and the text is %@",(long)index,text);
    };

}

- (void)addBannerViewSubView:(UIView *)view {
    _headerScrollViewController = [[ZHeaderScrollViewController alloc] init];
    self.headerScrollViewController.headerImageArray = _chooseHeaderImageArray;
    self.headerScrollViewController.picUrl = HomeADUrl;//图片下载地址
    CGFloat width  = __kWidth;
    CGFloat height = _picHight;//调整滚动视图图片高度 150 170
    
    self.headerScrollViewController.contentSize = CGSizeMake(width, height);
    self.headerScrollViewController.view.frame = CGRectMake(GAP_SPACE, GAP_SPACE, width, height);
    self.headerScrollViewController.delegate = self;
    [view addSubview:self.headerScrollViewController.view];
}

//专区选择子控件
- (void)addChooseViewSubView:(UIView *)view {
    
    
    for (int i=0; i<_headerTitleArray.count; i++) {
        
        //按钮视图
        //chooseBtnView = [[UIView alloc] initWithFrame:CGRectMake(i*(CGRectW(view)/4), 0, CGRectW(view)/4, CGRectH(view))];
        //专区数量随数据给出变化
        chooseBtnView = [[UIView alloc] initWithFrame:CGRectMake(i*(CGRectW(view)/_headerTitleArray.count), 0, CGRectW(view)/_headerTitleArray.count, CGRectH(view))];
        [view addSubview:chooseBtnView];
        
       
        //按钮 选中 color (89,89,89) (0x595959)
        UIButton *chooseBtn = [[UIButton alloc] initWithFrame:chooseBtnView.frame];
        [view addSubview:chooseBtn];
         chooseBtn.titleLabel.font = MFont(16);
        [chooseBtn setTitle:_headerTitleArray[i] forState:BtnNormal];
        [_btnArr addObject:chooseBtn];
        

        chooseBtn.tag = 200 + i;

        //判断按钮当前状态
        if (originalTag == 0) {
            //初始状态
            if (chooseBtn.tag - 200 == 0) {
                chooseBtn.selected = YES;
                chooseBtn.backgroundColor = HEXCOLOR(0x595959);
                //originalTag = chooseBtn.tag;
            }
        }else{
            //专区选择后状态
            if (originalTag == chooseBtn.tag) {
                chooseBtn.selected = YES;
                chooseBtn.backgroundColor = HEXCOLOR(0x595959);
            }
        }
        
        
        [chooseBtn addTarget:self action:@selector(choosePrefectureBtnAction:) forControlEvents:BtnTouchUpInside];
        
    }
}

//选择专区按钮触发方法
- (void)choosePrefectureBtnAction:(UIButton *)sender {
    sender.selected = YES;
    NSInteger index = sender.tag - 200;
    for (UIButton *button in _btnArr) {
        if (button == sender) {
            
            
            //添加判断 点击选中按钮不更新数据等操作
            if (button.tag == originalTag) {
                //NSLog(@"按钮没有有变化 ===== 不做响应");
            }else {
                NSLog(@"按钮有变化 ===== 更新广告和代理方法");
                
                
                //添加代理方法
                [self.delegate choosePrefectureButton:sender withIndex:index withChooseArray:_headerTitleArray];
                
                //修改originalTag值
                originalTag = button.tag;
                
            }
            continue;
            
        }else{
            button.selected = NO;
            button.backgroundColor = [UIColor clearColor];
        }
    }
    

    
}


- (void)choosePicNum:(NSInteger)picNum {
    [self.delegate chooseScrollViewPicNum:picNum];
}


@end
