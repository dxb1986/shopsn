//
//  ZHomeRecommendCell.h
//  shopSN
//
//  Created by yisu on 16/8/19.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  中间 推荐项目 cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZHomeRecommendCell : UICollectionViewCell

/** 推荐项目 图片 */
@property (nonatomic, strong) UIImageView *recommendIV;



@end
