//
//  ZHomeRecommendCell.m
//  shopSN
//
//  Created by yisu on 16/8/19.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeRecommendCell.h"

@implementation ZHomeRecommendCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:bgView];
        
        
        //图片
        //_recommendIV = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, CGRectW(bgView)-2, CGRectH(bgView)-2)];
        _recommendIV = [[UIImageView alloc] initWithFrame:bgView.frame];
        //_recommendIV.backgroundColor = __TestGColor;
        //_recommendIV.contentMode = UIViewContentModeScaleAspectFit;
        [bgView addSubview:_recommendIV];
        [bgView bringSubviewToFront:_recommendIV];
        
        
        
        
    }
    return self;
}



@end
