//
//  ZHomeTravelPicModel.h
//  shopSN
//
//  Created by yisu on 16/9/20.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员旅游 首页页面
 *
 *  数据 模型类
 *
 */


#import <Foundation/Foundation.h>

@interface ZHomeTravelPicModel : NSObject

//banner
/** 会员旅游 首页滚动图片 图片url */
@property (copy, nonatomic) NSString *bannerUrl;

/** 会员旅游 首页图片 链接url */
@property (copy, nonatomic) NSString *linkUrl;


//class_page  class
/** 会员旅游 首页类别、分类商品 图片url */
@property (copy, nonatomic) NSString *classUrl;

/** 会员旅游 首页类别、分类商品 分类id */
@property (copy, nonatomic) NSString *classID;

/** 会员旅游 首页类别、分类商品 分类名称 */
@property (copy, nonatomic) NSString *className;


//hot
/** 会员旅游 首页类别小图片 图片url */
@property (copy, nonatomic) NSString *hotUrl;

/** 会员旅游 首页类别小图片 图片url */
@property (copy, nonatomic) NSString *hotID;


//center
/** 会员旅游 center图片 图片url */
@property (copy, nonatomic) NSString *centerFooterUrl;







@end
