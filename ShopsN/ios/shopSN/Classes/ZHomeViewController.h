//
//  ZHomeViewController.h
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面
 *
 *  主视图控制器
 *
 */

#import "BaseViewController.h"

@class ZHomeViewController;
@protocol ZHomeViewControllerDelegate <NSObject>

/** 代理方法 向主控制器传值 */
- (void)sendValueToMainVCWithMainSectionArray:(NSArray *)mainSectionArray andRowArray:(NSArray *)rowArray andIsGoods:(BOOL)isGoods;

@end


@interface ZHomeViewController : BaseViewController

@property (nonatomic, weak) id<ZHomeViewControllerDelegate>delegate;


@end
