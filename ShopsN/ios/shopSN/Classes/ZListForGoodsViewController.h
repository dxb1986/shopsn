//
//  ZListForGoodsViewController.h
//  shopSN
//
//  Created by yisu on 16/9/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品列表页面
 *
 *  会员商品 主控制器
 *
 */
#import "ZBaseViewController.h"

@interface ZListForGoodsViewController : ZBaseViewController

/** 商品列表 会员商品 所选二级分类ID */
@property (copy, nonatomic) NSString *subCategoryID;

/** 商品列表 会员商品 所选二级分类名称 */
@property (copy, nonatomic) NSString *subCategoryName;

/** 商品列表 会员商品 所选一级分类ID */
@property (copy, nonatomic) NSString *mainCategoryID;

@end
