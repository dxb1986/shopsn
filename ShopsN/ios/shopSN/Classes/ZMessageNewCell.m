//
//  ZMessageNewCell.m
//  shopSN
//
//  Created by yisu on 16/10/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZMessageNewCell.h"

@implementation ZMessageNewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = HEXCOLOR(0xffffff);
        
        [self initSubViews];
        
//        //1 消息图片
//        _headIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 44, 44)];
//        [self.contentView addSubview:_headIV];
//        //    _headIV.backgroundColor = __DefaultColor;
//        //    _headIV.layer.cornerRadius = 3;
//        _headIV.image = MImage(@"xxcl.png");
//        
//        //2 标示图片
//        _flagIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)-3, 5, 10, 10)];
//        [self.contentView addSubview:_flagIV];
//        _flagIV.backgroundColor = [UIColor redColor];
//        _flagIV.layer.cornerRadius = 5;
//        
//        //3 标题
//        _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)+10, Cell_SPACE, 150, 20)];
//        [self.contentView addSubview:_titleLb];
//        //    _titleLb.backgroundColor = __TestOColor;
//        _titleLb.font = MFont(14);
//        _titleLb.textColor = HEXCOLOR(0x333333);
//        _titleLb.text = @"通知消息";
//        
//        //4 详情
//        _detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)+10, CGRectYH(_titleLb), 200, 60)];
//        [self.contentView addSubview:_detailLb];
//        //    _detailLb.backgroundColor = __TestOColor;
//        _detailLb.font = MFont(12);
//        _detailLb.textColor = HEXCOLOR(0x999999);
//        //_detailLb.numberOfLines = 0;
//        _detailLb.text = @"您的会员等级已经升级到二级";
//        
//        //5 时间
//        _timeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(self)-120-10, 15, 120, 20)];
//        [self.contentView addSubview:_timeLb];
//        //    _timeLb.backgroundColor = __TestGColor;
//        _timeLb.font = MFont(12);
//        _timeLb.textColor = HEXCOLOR(0x999999);
//        _timeLb.textAlignment = NSTextAlignmentRight;
//        _timeLb.text = @"16/06/10";
//        
//        //6 底线
//        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)+10, CGRectH(self)-1, CGRectW(self)-15-44, 1)];
//        [self.contentView addSubview:lineIV];
//        lineIV.backgroundColor = HEXCOLOR(0xdedede);
        
    }
    
    return self;
}



- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,__kWidth, 110)];
    [self addSubview:bgView];
    
    //1 消息图片
     _headIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 44, 44)];
    [bgView addSubview:_headIV];
    //    _headIV.backgroundColor = __DefaultColor;
    //    _headIV.layer.cornerRadius = 3;
    _headIV.image = MImage(@"xxcl.png");
    
    //2 标示图片
    _flagIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)-3, 5, 10, 10)];
    [bgView addSubview:_flagIV];
    _flagIV.backgroundColor = [UIColor redColor];
    _flagIV.layer.cornerRadius = 5;
    
    //3 标题
    _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)+10, Cell_SPACE, 150, 20)];
    [bgView addSubview:_titleLb];
    //    _titleLb.backgroundColor = __TestOColor;
    _titleLb.font = MFont(14);
    _titleLb.textColor = HEXCOLOR(0x333333);
    _titleLb.text = @"通知消息";
    
    //4 详情
    _detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)+10, CGRectYH(_titleLb)+5, CGRectW(bgView)-CGRectW(_headIV)-20, 60)];
    [bgView addSubview:_detailLb];
    //    _detailLb.backgroundColor = __TestOColor;
    _detailLb.font = MFont(12);
    
    _detailLb.textColor = HEXCOLOR(0x999999);
    _detailLb.numberOfLines = 0;
    _detailLb.text = @"您的会员等级已经升级到二级";
    
    
    //5 时间
    _timeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-140-10, 15, 140, 20)];
    [bgView addSubview:_timeLb];
    //    _timeLb.backgroundColor = __TestGColor;
    _timeLb.font = MFont(12);
    _timeLb.textColor = HEXCOLOR(0x999999);
    _timeLb.textAlignment = NSTextAlignmentRight;
    _timeLb.text = @"16/06/10";
    
    //6 底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectXW(_headIV)+10, CGRectH(bgView)-1, CGRectW(bgView)-15-44, 1)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
