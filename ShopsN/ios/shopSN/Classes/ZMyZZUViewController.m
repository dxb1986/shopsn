//
//  ZMyZZUViewController.m
//  shopSN
//
//  Created by yisu on 16/10/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZMyZZUViewController.h"
#import "ZMyZZUCell.h"

#import "ZCenterAboutOurViewController.h"   //关于我们
#import "ZCenterSuggestViewController.h"    //意见反馈

@interface ZMyZZUViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    NSArray *_titleArray;
}

@end

@implementation ZMyZZUViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = [NSString stringWithFormat:@"我的%@",simpleTitle];
    
    
    
    [self initSubViews:self.mainMiddleView];//初始化中间视图
    
}

#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    _titleArray = @[@"关于我们",@"意见反馈"];
    
    //边线
//    UIImageView *topLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 1)];
//    [view addSubview:topLineIV];
//    topLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    //2表视图
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 1, CGRectW(view), CGRectH(view)-1)];
    [view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
}

#pragma mark - tableView DataSource and Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titleArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ZMyZZUCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyZZUCell"];
    if (!cell) {
        cell = [[ZMyZZUCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyZZUCell"];
        //设置右侧辅助视图
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    cell.textLabel.text = _titleArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0://关于我们页面
        {
            ZCenterAboutOurViewController *vc = [[ZCenterAboutOurViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
            
        case 1://意见反馈页面
        {
            ZCenterSuggestViewController *vc = [[ZCenterSuggestViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}




#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
