//
//  ZOrderModel.h
//  shopSN
//
//  Created by yisu on 16/9/13.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员商品 会员旅游 相关页面
 *
 *  订单模型类
 *
 */

#import <Foundation/Foundation.h>

@interface ZOrderModel : NSObject

/** 商品 id id */
@property (copy, nonatomic) NSString *ID;

/** 商品 订单编号 orders_num */
@property (copy, nonatomic) NSString *orderID;

/** 商品 订单类型 order_type */
@property (copy, nonatomic) NSString *orderType;


/** 商品 订单应付总金额 price_sum */
@property (copy, nonatomic) NSString *orderAllPrice;

/** 商品 订单创建时间 create_time */
@property (copy, nonatomic) NSString *orderCreateTime;

/** 商品 订单状态 orders_status */
@property (copy, nonatomic) NSString *orderStatus;

/** 商品 订单商品数组 goods */
@property (strong, nonatomic) NSArray *goodsArray;

/** 商品 支付状态 pay_status */
@property (copy, nonatomic) NSString *payStatus;


/** 商品 标题 goods_title */
@property (copy, nonatomic) NSString *goodsTitle;

/** 商品_数量 或旅游出发_成人数量 goods_num */
@property (copy, nonatomic) NSString *goodsAdultsCounts;

/** 商品_单价 或旅游出发_成人单价 price_new */
@property (copy, nonatomic) NSString *goodsAdultsNowPrice;

/** 商品 订单ID goods_orders_id */
@property (copy, nonatomic) NSString *goodsOrderID;

/** 商品 ID goods_id */
@property (copy, nonatomic) NSString *goodsID;

/** 商品 图片Url pic_url */
@property (copy, nonatomic) NSString *goodsPicUrl;

/** 旅游出发 日期 chufa_date */
@property (copy, nonatomic) NSString *tourismStartDate;

/** 旅游出发 价格 chufa_price */
@property (copy, nonatomic) NSString *tourismNowPrice;



/** 旅游出发 儿童单价 chufa_price_et*/
@property (copy, nonatomic) NSString *goodsMinorsNowPrice;

/** 旅游出发 儿童数量 goods_mun_et */
@property (copy, nonatomic) NSString *goodsMinorsCounts;

/** 商品 评价内容 pingjia_content */
@property (copy, nonatomic) NSString *goodsJudgeContents;//返回数据暂无该内容

/** 商品 确认收货时间 shouhuo_time */
@property (copy, nonatomic) NSString *goodsReceiveTime;//返回数据暂无该内容

@end
