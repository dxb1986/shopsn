//
//  ZPCMyManagerSubView.h
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的管理 cell
 *
 *  子视图
 *
 */
#import "BaseView.h"

@interface ZPCMyManagerSubView : BaseView

/** 标题图片 */
@property (nonatomic, strong) UIImageView *titleIV;

/** 标题文本 */
@property (nonatomic, strong) UILabel *titleLb;


@end
