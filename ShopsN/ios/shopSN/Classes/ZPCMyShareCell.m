//
//  ZPCMyShareCell.m
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPCMyShareCell.h"



@interface ZPCMyShareCell ()
{
    UIView *_titleView;
}

@property (nonatomic, strong) UIButton *shareButton;


@end

@implementation ZPCMyShareCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = HEXCOLOR(0xdedede);
        //方法2 也无效
                [self addData];
        [self initSubViews];
        


        
    }
    
    return self;
}

- (void)initSubViews {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 70)];
    [self addSubview:bgView];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    
    //3 方法3 没有意义
//    [self addData];
    
    for (int i=0; i<4; i++) {
        
        //1 titleView
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(i*(CGRectW(bgView)-25)/4+4*(i+1), 0, (CGRectW(bgView)-25)/4, CGRectH(bgView))];
        [bgView addSubview:_titleView];
        
//        _titleView.backgroundColor = __TestGColor;
        //2 titleView - subView

        if (i == 3) {//添加 带有图片的subView
            
            _orderSubView = [[ZPCMyOrderSubView alloc] initWithFrame:CGRectMake((CGRectW(_titleView)-55+25)/2, 10, 55, 50)];
            [_titleView addSubview:_orderSubView];
            
            //1 赋值
            _orderSubView.titleIV.image = MImage(@"fx.png");
            _orderSubView.titleLb.text = @"我的分享>";
            
            //2 添加竖线
            UIImageView *leftLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(20, 0, 1, CGRectH(_titleView))];
            [_titleView addSubview:leftLineIV];
            leftLineIV.backgroundColor = HEXCOLOR(0xdedede);
            
            
            
        }else{//添加 没有图片的subView
            _shareSubView = [[ZPCMyShareSubView alloc] initWithFrame:CGRectMake((CGRectW(_titleView)-65)/2, 10, 65, 50)];
            [_titleView addSubview:_shareSubView];
            //_shareSubView.backgroundColor = __TestOColor;
            //积分 优惠券  余额  空 我的分享
            //203 0 0.00 空 fx.png
            switch (i) {
                case 0://
                {
                    _shareSubView.titleLb.text = @"积分";
                    _shareSubView.descLb.text  = _points;
                    
                    
                }
                    break;
                    
                case 1://
                {
                    _shareSubView.titleLb.text = @"优惠券";
                    _shareSubView.descLb.text  = _discount;
                }
                    break;
                    
//                case 2://
//                {
//                    _shareSubView.titleLb.text = @"余额";
//                    _shareSubView.descLb.text  = _balance;
//                }
//                    break;
                    
                default:
                    break;
            }
            
            
        }
        
        //添加同尺寸按钮
        _shareButton = [[UIButton alloc] initWithFrame:_orderSubView.frame];
        [_titleView addSubview:_shareButton];
        _shareButton.tag = 2020+i;
        [_shareButton addTarget:self action:@selector(shareButtonAction:) forControlEvents:BtnTouchUpInside];
    }
    
    
    
    
    
}
-(void)updateCellWith:(NSString *)point{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    _points = point;
    _discount = @"0";
    [self initSubViews];
    
    
}

- (void)addData {
    _points = @"0.0";
    _discount = @"0";
    _balance  = @"0.00";
}

- (void)shareButtonAction:(UIButton *)btn {
    [self.delegate didShareButton:btn];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
