//
//  ZPayConsigneeInfoCell.h
//  shopSN
//
//  Created by chang on 16/7/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 结算页面
 *
 *  收货人信息 cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZPayConsigneeInfoCell : UITableViewCell

/** 收货人 姓名 */
@property (nonatomic, strong) UILabel *consigneeName;

/** 收货人 电话 */
@property (nonatomic, strong) UILabel *consigneePhone;

/** 收货人 地址 */
@property (nonatomic, strong) UILabel *consigneeAddress;


@end
