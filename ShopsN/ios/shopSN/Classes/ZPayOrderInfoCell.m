//
//  ZPayOrderInfoCell.m
//  shopSN
//
//  Created by chang on 16/7/3.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPayOrderInfoCell.h"

@implementation ZPayOrderInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //1 商品图片
        UIImageView *goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 80, 80)];
        [self addSubview:goodsIV];
        self.cellImage = goodsIV;
        //goodsIV.image = MImage(_goodsImageName);
//        goodsIV.layer.borderWidth = 1.0f;
//        goodsIV.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
        goodsIV.contentMode = UIViewContentModeScaleAspectFit;
        
        
        //2 商品描述
        UILabel *detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(goodsIV)+5, 15, __kWidth-35-35-65-50, 35)];
        
//      detailLb.backgroundColor = __TestGColor;
        
        [self addSubview:detailLb];
        self.cellDetail = detailLb;
        detailLb.numberOfLines = 0;
        detailLb.font = MFont(12);
        detailLb.textColor = HEXCOLOR(0x333333);
        //detailLb.text = _goodsDetail;
        
        //套餐名
        UILabel *setName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(detailLb), CGRectYH(detailLb), detailLb.bounds.size.width, 15)];
        setName.text = @"套餐：无";
        setName.font = MFont(12);
        setName.textColor = detailLb.textColor;
        [self addSubview:setName];
        self.cellSetName = setName;
        //送积分
        UILabel *retPoint = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(detailLb), CGRectYH(setName), setName.bounds.size.width, 15)];
        retPoint.text = @"送：0分";
        self.cellRetuPoint = retPoint;
        retPoint.font = MFont(12);
        retPoint.textColor = setName.textColor;
        [self addSubview:retPoint];
        //3 商品现价
        UILabel *priceLb = [[UILabel alloc] initWithFrame:CGRectMake(__kWidth-160, 35, 150, 20)];
//        priceLb.backgroundColor = __TestOColor;
        [self addSubview:priceLb];
        self.cellPrice = priceLb;
        priceLb.font = MFont(12);
        priceLb.textColor = HEXCOLOR(0x333333);
        priceLb.textAlignment = NSTextAlignmentRight;
        
        //4 商品选购数量
        UILabel *numLb = [[UILabel alloc] initWithFrame:CGRectMake(__kWidth-60, CGRectYH(priceLb), 50, 15)];
        [self addSubview:numLb];
        self.cellNumber = numLb;
        numLb.font = MFont(12);
        numLb.textColor = HEXCOLOR(0x999999);
        numLb.textAlignment = NSTextAlignmentRight;
        
        //5 中间线
        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(20, CGRectYH(goodsIV)+10, __kWidth-20, 1)];
        [self addSubview:lineIV];
        lineIV.backgroundColor = HEXCOLOR(0xdedede);
        
        //6 小计 价格
        UILabel *settlePriceLb = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectYH(lineIV)+5, __kWidth-35, 25)];
        [self addSubview:settlePriceLb];
        self.cellAllPrice = settlePriceLb;
        settlePriceLb.font = MFont(14);
        settlePriceLb.textColor = __MoneyColor;
        settlePriceLb.textAlignment = NSTextAlignmentRight;
        
        [self setGoodsImage:goodsIV andDetail:detailLb andPriceLb:priceLb andNumLb:numLb andSettlePriceLb:settlePriceLb];
        
    }
    return self;
}



- (void)setGoodsImage:(UIImageView *)imageView andDetail:(UILabel *)detailLb andPriceLb:(UILabel *)priceLb andNumLb:(UILabel *)numLb andSettlePriceLb:(UILabel *)settlePrciceLb {
    
    //test
    _goodsImageName = @"pic12";
    _goodsDetail    = @"COSME DECORTE 黛珂 AQ珍萃精颜 悦活洁肤霜 150克";
    _goodsPrice     = @"￥589";
    _goodsNum       = @"x1";
    _goodsSettlePrice = @"￥1178.00";
    
    imageView.image = MImage(_goodsImageName);
    detailLb.text = _goodsDetail;
    priceLb.text  = _goodsPrice;
    numLb.text    = _goodsNum;
    settlePrciceLb.text = [NSString stringWithFormat:@"小计: %@",_goodsSettlePrice];
    [self setLabeltextAttributes:settlePrciceLb];
}

- (void)setLabeltextAttributes:(UILabel *)label {
    NSString *text = label.text;
    //NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    [attri addAttribute:NSFontAttributeName value:MFont(9) range:NSMakeRange(0, 3)];
    [attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x999999) range:NSMakeRange(0, 3)];
    
    [label setAttributedText:attri];
}



@end
