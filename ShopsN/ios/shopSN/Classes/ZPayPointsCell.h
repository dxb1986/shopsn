//
//  ZPayPointsCell.h
//  shopSN
//
//  Created by chang on 16/7/4.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 结算页面
 *
 *  使用积分 Cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZPayPointsCell : UITableViewCell


/**A积分*/
@property (nonatomic,strong) UITextField *pointATX;
/**B积分*/
@property (nonatomic,strong) UITextField *pointBTX;

/**A积分*/
@property (nonatomic,strong) UILabel *pointALabel;
/**B积分*/
@property (nonatomic,strong) UILabel *pointBLabel;
/**运费*/
@property (nonatomic,strong) UILabel *yunLabel;




@end
