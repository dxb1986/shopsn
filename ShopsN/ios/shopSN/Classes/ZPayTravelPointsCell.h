//
//  ZPayTravelPointsCell.h
//  shopSN
//
//  Created by 王子豪 on 16/9/28.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZPayTravelPointsCell : UITableViewCell
/**A积分*/
@property (nonatomic,strong) UITextField *pointATX;
/**B积分*/
@property (nonatomic,strong) UITextField *pointBTX;

/**A积分*/
@property (nonatomic,strong) UILabel *pointALabel;
/**B积分*/
@property (nonatomic,strong) UILabel *pointBLabel;
/**运费*/
@property (nonatomic,strong) UILabel *yunLabel;



@end
