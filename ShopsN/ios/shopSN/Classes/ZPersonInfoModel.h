//
//  ZPersonInfoModel.h
//  shopSN
//
//  Created by 王子豪 on 16/9/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPersonInfoModel : NSObject
/**indodic*/
@property (nonatomic,strong) NSArray *infoArr;
+(instancetype)shareInfoModel;
@end
