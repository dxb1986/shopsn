//
//  ZPersonInfoModel.m
//  shopSN
//
//  Created by 王子豪 on 16/9/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPersonInfoModel.h"
ZPersonInfoModel *infoModel = nil;
@implementation ZPersonInfoModel
+(instancetype)shareInfoModel{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        infoModel = [[ZPersonInfoModel alloc] init];
        
    });
    return infoModel;
}
@end
