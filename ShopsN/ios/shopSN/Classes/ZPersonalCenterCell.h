//
//  ZPersonalCenterCell.h
//  shopSN
//
//  Created by chang on 16/7/6.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 页面
 *
 *  我的管理 cell (第三行)
 *
 */
#import "BaseTableViewCell.h"

@class ZPersonalCenterCell;
@protocol ZPersonalCenterCellDelegate <NSObject>

- (void)didManageButton:(UIButton *)sender;

@end

@interface ZPersonalCenterCell : BaseTableViewCell

@property (nonatomic, weak) id<ZPersonalCenterCellDelegate>delegate;

@end
