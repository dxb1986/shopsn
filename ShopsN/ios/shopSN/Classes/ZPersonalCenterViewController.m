//
//  ZPersonalCenterViewController.m
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPersonalCenterViewController.h"
#import "ZCenterMessageViewController.h"//消息
#import "ZCenterMyOrderViewController.h" //我的订单 订单管理
#import "ZCenterShareViewController.h"  //我的分享 shopSN分享
#import "ZCenterAccountViewController.h"//账户管理
#import "ZCenterMyPrintsViewController.h"//我的足迹
#import "ZCenterMyCollectionViewController.h"//我的收藏
#import "ZCenterMemberCertificateViewController.h"//会员资格
#import "ZCenterPersonalInfoViewController.h"    //个人信息
#import "ZMyZZUViewController.h"//我的shopSN
#import "ZCAccountPointsViewController.h"//积分页面
#import "ZCAccountDiscountViewController.h"//优惠劵页面
#import "ZCopyrightViewController.h"

//#import "ZLoginViewController.h"//登录

#import "ZCommonWebViewController.h"//通过web页面-激活账号

#import "ZAccountManagerModel.h"

//subView
#import "ZPersonalCenterCell.h"
#import "ZPCMyOrderCell.h"  //我的订单cell
#import "ZPCMyShareCell.h"  //我的分享cell

@interface ZPersonalCenterViewController ()<UITableViewDataSource, UITableViewDelegate, ZPersonalCenterCellDelegate, ZPCMyOrderCellDelegate, ZPCMyShareCellDelegate>
{
    UITableView *_tableView;    //我的主页面表视图
    UIView *myTopView;//头部视图
    //UIView *menuBtnView;   //交互菜单视图
    
    NSString *tempID;//临时存放用户id
    
}


/** 用户头像 */
@property (nonatomic, strong)UIImageView *headPortraitIV;

/** 用户手机号 */
@property (nonatomic, strong)UILabel *userPhoneLb;

/** 用户id */
@property (nonatomic, strong)UILabel *userIdLb;

/** 会员等级 */
@property (nonatomic, strong)UILabel *userTypeLb;

/** 系统消息 view */
@property (nonatomic, strong) UIView *messageBtnView;

/**个人信息页面的网络data*/
@property (nonatomic,strong) NSDictionary *personDic;


/**
 *  详情数组
 */
@property (nonatomic ,strong)NSArray *detailArray;

@end

@implementation ZPersonalCenterViewController

- (NSArray *)detailArray {
    if (!_detailArray) {
        _detailArray = [NSArray array];
        
        _detailArray = @[@"¥0.0", @"无", @"0分", @"0张"];
    }
    return _detailArray;
}



#pragma mark - ==== 页面设置 =====
- (void)viewWillAppear:(BOOL)animated {
    
   
//    NSLog(@"id:%@===id(2):%@",tempID,[tempID substringWithRange:NSMakeRange(0, 2)]);
    if (![tempID isEqualToString:[UdStorage getObjectforKey:Userid]]) {
        [self userDidChange];
    }
    
//    if ([[tempID substringWithRange:NSMakeRange(1, 2)] isEqualToString:@"19"]) {
//        //用户账号前二位是19时，需要进入网址激活
//        ZCommonWebViewController *vc = [[ZCommonWebViewController alloc] init];
//        vc.titleStr = @"账号激活";
//        vc.urlStr   = @"http://www.zzumall.com/index.php/Mobile/Other/index.html";
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//    

    
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = NO;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.titleLb.text = @"个人中心";
    
    
    
    
    //初始化 中间视图 相关子视图
    [self initSubViews:self.mainMiddleView];
    
    [self getInterfaceData];
    
    tempID = [UdStorage getObjectforKey:Userid];
    NSLog(@"%@",[tempID substringWithRange:NSMakeRange(2, 1)]);
    
}
#pragma mark *** 获取数据 ***
-(void)getInterfaceData{
    
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid]};
    
    [ZHttpRequestService POST:@"User/" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [SXLoadingView hideProgressHUD];
            NSDictionary *dic = jsonDic[@"data"][0];
            self.userPhoneLb.text = dic[@"mobile"];
            self.userIdLb.text = @"******";
            self.userTypeLb.text = dic[@"grade_name"];
            
            self.personDic = dic;
            
            //初始化 中间视图 相关子视图
            //[self initSubViews:self.mainMiddleView];
            
            [_tableView reloadData];
            
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];

    } animated:true withView:nil];
    
    
//    [ZHttpRequestService POST:@"goods_list" withParameters:@{@"id":@195,@"user_id":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
//        if (succe) {
//            NSLog(@"detail----%@", jsonDic[@"data"]);
//        }
//    } failure:^(NSError *error) {
//        
//    } animated:YES withView:nil];
}

#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    //边线
    UIImageView *topLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 1)];
    [view addSubview:topLineIV];
    topLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    //1 头部视图
    myTopView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, CGRectW(view), 119)];
    [view addSubview:myTopView];
//    myTopView.backgroundColor = __TestOColor;
    
    //添加子视图
    [self addSubViewsWithMyTopView:myTopView];
    
    //2表视图
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 120, CGRectW(view), CGRectH(view)-120-50)];
    [view addSubview:_tableView];
    _tableView.backgroundColor = HEXCOLOR(0xdedede);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
    
}

//添加头部视图子视图
- (void)addSubViewsWithMyTopView:(UIView *)view {
    
    //1 底部图片 **坐标要使用绝对值 不能使用topView.frame
    UIImageView *bgIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 119)];
    [view addSubview:bgIV];
    bgIV.image = MImage(@"hybg.png");
    
    
    //2 头像
    _headPortraitIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 30, 60, 60)];
    [view addSubview:_headPortraitIV];
    _headPortraitIV.image = MImage(@"touxiang.png");
    
    //3 手机号
    _userPhoneLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_headPortraitIV)+5, 40, 110, 20)];
    [view addSubview:_userPhoneLb];
//    _userPhoneLb.backgroundColor = __TestOColor;
    _userPhoneLb.font = MFont(12);
    _userPhoneLb.textColor = HEXCOLOR(0xffffff);
    _userPhoneLb.text = @"13789885632";
    
    //4 id
    //标题 color (200 226 23) (0xc8e217)
    UILabel *userIdtitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_headPortraitIV)+5, CGRectYH(_userPhoneLb), 15, 15)];
    [view addSubview:userIdtitle];
//    userIdtitle.backgroundColor = __TestOColor;
    userIdtitle.font = MFont(10);
    userIdtitle.textColor = HEXCOLOR(0xc8e217);
    userIdtitle.text = @"ID:";
    //内容
    _userIdLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(userIdtitle), CGRectYH(_userPhoneLb), 90, 15)];
    [view addSubview:_userIdLb];
//    _userIdLb.backgroundColor = __TestOColor;
    _userIdLb.font = MFont(10);
    _userIdLb.textColor = HEXCOLOR(0xffffff);
    _userIdLb.text = @"689744";
    
    //5 会员等级 color (47 122 40) (0x2f7a28)
    // 视图
    UIView *memberView = [[UIView alloc] initWithFrame:CGRectMake(CGRectXW(_userPhoneLb), 55, 90, 20)];
    [view addSubview:memberView];
    memberView.backgroundColor = LH_RGBCOLOR(195, 94, 104);
    memberView.layer.cornerRadius = 10;
    
    // 标题
    UILabel *memberTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, 45, 18)];
    [memberView addSubview:memberTitle];
//    memberTitle.backgroundColor = __TestGColor;
    memberTitle.font = MFont(9);
    memberTitle.textColor = HEXCOLOR(0xffffff);
    memberTitle.text = @"会员等级:";
    
    // 内容 color (200 226 23) (0xc8e217)
    _userTypeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(memberTitle), 1, CGRectW(memberView)-CGRectW(memberTitle)-15+10, 18)];
    [memberView addSubview:_userTypeLb];
//    _userTypeLb.backgroundColor = __TestOColor;
    _userTypeLb.font = MFont(9);
    _userTypeLb.textColor = HEXCOLOR(0xc8e217);
    _userTypeLb.text = @"游客";
    
    
    //6 消息按钮
    _messageBtnView = [[UIView alloc] initWithFrame:CGRectMake(CGRectW(view)-10-25, 50, 25, 25)];
    [view addSubview:_messageBtnView];
    //_messageBtnView.backgroundColor = __TestOColor;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(messageBtnViewTap:)];
    [_messageBtnView addGestureRecognizer:tap];
    
    UIImageView *messageIV = [[UIImageView alloc] initWithFrame:_messageBtnView.frame];
    messageIV.image = MImage(@"xx.png");
    [view addSubview:messageIV];
    
    UIImageView *roundIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectW(view)-10-8, 48, 10, 10)];
    [view addSubview:roundIV];
    roundIV.backgroundColor = [UIColor redColor];
    roundIV.layer.cornerRadius = 5.0f;
    
    
    
    _userPhoneLb.text = [UdStorage getObjectforKey:UserPhone];
    _userIdLb.text = @"******";
    if (IsNilString([UdStorage getObjectforKey:UserType])) {
        _userTypeLb.text = @"游客";
    }else{
        _userTypeLb.text =[UdStorage getObjectforKey:UserType];
    }
}



#pragma mark-- 消息按钮
- (void)messageBtnViewTap:(UIGestureRecognizer *)gr {
    NSLog(@"进入 系统信息页面");
    [ZHttpRequestService POST:@"LeaveingAMessage/news/" withParameters:@{@"token":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            //跳转消息页面
            ZCenterMessageViewController *vc = [[ZCenterMessageViewController alloc] init];
            vc.messageArray = jsonDic[@"data"];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
        
        
    } failure:^(NSError *error) {
        //
    } animated:NO withView:nil];
    
    
    
}


#pragma mark- ===== tableView DataSource and Delegate =====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        ZPCMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyOrderCell"];
        if (!cell) {
            cell = [[ZPCMyOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyOrderCell"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        return cell;
    }
    
    if (indexPath.row == 1) {
        ZPCMyShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyShareCell"];
        if (!cell) {
            cell = [[ZPCMyShareCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyShareCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
//        //方法1 该赋值方法 无效
//        if (self.personDic) {
//            [cell updateCellWith:self.personDic[@"integral"]];
//        }
//        cell.points = self.personDic[]
//        cell.discount = @"1";
//        cell.balance  = @"0.01";
//        [cell reloadInputViews];
        return cell;
    }
    
    ZPersonalCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
    if (!cell) {
        cell = [[ZPersonalCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        //[cell.managerButton addTarget:self action:@selector(managerButtonAction:) forControlEvents:BtnTouchUpInside];
    }
    
    return cell;
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 2) {
        return 220;
    }
    
    return 80;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    

    
    
}



#pragma mark - ===== 订单行cell 代理方法 =====
- (void)didOrderButton:(UIButton *)sender {
    //判断账户资格
    if ([[UdStorage getObjectforKey:UserType] isEqualToString:@"游客"]) {
        //如果是游客则跳转至会员资格页面
        ZCenterMemberCertificateViewController *vc = [[ZCenterMemberCertificateViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        
        NSInteger chooseNum = 0;
        switch (sender.tag - 2010) {
            case 0:
                NSLog(@"待付款");
                chooseNum = 131;
                break;
                
            case 1:
                NSLog(@"待收货");
                chooseNum = 132;
                break;
                
            case 2:
                NSLog(@"已完成");
                chooseNum = 133;
                break;
            case 3:
            {
                NSLog(@"消息");
                [ZHttpRequestService GETApi:@"news" withParameters:[UdStorage getObjectforKey:Userid] success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
                    if (succe) {
                        //跳转消息页面
                        ZCenterMessageViewController *vc = [[ZCenterMessageViewController alloc] init];
                        vc.messageArray = jsonDic[@"data"];
                        [self.navigationController pushViewController:vc animated:YES];
                    }else{
                        NSString *message = jsonDic[@"message"];
                        
                        [SXLoadingView showAlertHUD:message duration:2];
                    }
                    
                    
                } failure:^(NSError *error) {
                    //
                } animated:NO withView:nil];
                return;
            }
                
                break;
            case 4:
                NSLog(@"我的订单");
                chooseNum = 130;
                break;
                
            default:
                break;
        }
        ZCenterMyOrderViewController *vc = [[ZCenterMyOrderViewController alloc] init];
        vc.chooseNum = chooseNum;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - ===== 分享cell 代理方法 =====
- (void)didShareButton:(UIButton *)sender {
    
    //判断账户资格
//    if ([[UdStorage getObjectforKey:UserType] isEqualToString:@"游客"]) {
//        //如果是游客则跳转至会员资格页面
//        ZCenterMemberCertificateViewController *vc = [[ZCenterMemberCertificateViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
//    }else {
        if (sender.tag == 2023) {
            NSLog(@"我的分享");
            ZCenterShareViewController *vc = [[ZCenterShareViewController alloc] init];
            //*** test
            vc.memberId = [UdStorage getObjectforKey:Userid];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
//            //获取账户管理数据                                    };
//            [ZHttpRequestService POST:@"User/personManage" withParameters:@{@"token":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
//                if (succe) {
//                    
//                    NSDictionary *dic = jsonDic[@"data"];
//                    
//                    [ZAccountManagerModel shareAccountManagerData].accountDic = dic;
//                    
//                    _detailArray = @[dic[@"account_balance"],[dic[@"num"] isEqualToString:@"0"]?@"无":[NSString stringWithFormat:@"%@张",dic[@"num"]],dic[@"integral"],@"0张"];
//                    
//                    //进入积分或优惠劵页面
//                    switch (sender.tag - 2020) {
//                        case 0:
//                        {
//                            NSLog(@"积分");
//                            ZCAccountPointsViewController *vc = [[ZCAccountPointsViewController alloc] init];
//                            vc.myPointsStr = @"1234";
//                            [self.navigationController pushViewController:vc animated:YES];
//                        }
//                            
//                            break;
//                        case 1:
//                        {
//                            NSLog(@"优惠券");
//                            ZCAccountDiscountViewController *vc = [[ZCAccountDiscountViewController alloc] init];
//                            [self.navigationController pushViewController:vc animated:YES];
//                        }
//                            
//                            break;
//                            
//                            
//                        default:
//                            break;
//                    }
//                    
//                    
//                    
//                    
//                }else{
//                    NSString *message = jsonDic[@"message"];
//                    [SXLoadingView showAlertHUD:message duration:2];
//                }
//                
//                
//            } failure:^(NSError *error) {
//                
//                [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
//                
//            } animated:true withView:nil];
        }
        
        
        
//    }

}




#pragma mark - ===== 管理行cell 代理方法 =====
- (void)didManageButton:(UIButton *)sender {
 
//    //判断账户资格
//    if ([[UdStorage getObjectforKey:UserType] isEqualToString:@"游客"]) {
//        //如果是游客则跳转至会员资格页面
//        ZCenterMemberCertificateViewController *vc = [[ZCenterMemberCertificateViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
//    }else {
        switch (sender.tag-100) {
            case 0:
            {
                NSLog(@"进入 个人信息页面");
                ZCenterPersonalInfoViewController *vc = [[ZCenterPersonalInfoViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
                
            case 1:
            {
                NSLog(@"进入 账户管理页面");
                ZCenterAccountViewController *vc = [[ZCenterAccountViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
                
//            case 2:
//            {
//                NSLog(@"进入 会员资格页面");
//                ZCenterMemberCertificateViewController *vc = [[ZCenterMemberCertificateViewController alloc] init];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//                break;

            case 2:
            {
                NSLog(@"进入 订单管理页面");
                ZCenterMyOrderViewController *vc = [[ZCenterMyOrderViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
                
            case 3:
            {
                NSLog(@"进入 我的收藏页面");
                ZCenterMyCollectionViewController *vc = [[ZCenterMyCollectionViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
                
            case 4:
            {
                NSLog(@"进入 我的足迹页面");
                ZCenterMyPrintsViewController *vc = [[ZCenterMyPrintsViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;              
            case 5:
            {
                NSLog(@"进入 授权页面");
                ZCopyrightViewController *vc = [[ZCopyrightViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
//                break;
//                
//            case 7:
//            {
//                NSLog(@"进入 我的页面");
//                ZMyZZUViewController *vc = [[ZMyZZUViewController alloc] init];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//                break;
//                
            default:
                break;
        }

//    }

}



#pragma mark - ===== 用户账号切换 刷新页面=====


- (void)userDidChange {
    NSLog(@"id:%@==name:%@===phone:%@", [UdStorage getObjectforKey:Userid], [UdStorage getObjectforKey:UserType], [UdStorage getObjectforKey:UserPhone]);
    tempID = [UdStorage getObjectforKey:Userid];
    //重写 头部视图
    [myTopView removeFromSuperview];
    
    myTopView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, CGRectW(self.mainMiddleView), 119)];
    [self.mainMiddleView addSubview:myTopView];
    //    myTopView.backgroundColor = __TestOColor;
    
    //添加子视图
    [self addSubViewsWithMyTopView:myTopView];
    
    [self getInterfaceData];
}


#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
