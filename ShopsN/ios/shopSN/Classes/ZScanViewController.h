//
//  ZScanViewController.h
//  shopSN(内测版)
//
//  Created by yisu on 16/7/24.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ZScanViewController : BaseViewController


@property (nonatomic,copy)void (^QRUrlBlock)(NSString *url);

@end
