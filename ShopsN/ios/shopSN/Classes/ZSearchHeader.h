//
//  ZSearchHeader.h
//  shopSN
//
//  Created by yisu on 16/6/29.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品搜索页面
 *
 *  collectionView header
 *
 */
#import <UIKit/UIKit.h>

@interface ZSearchHeader : UICollectionReusableView

/** 搜索异常 顶部提示内容 */
@property (nonatomic, strong) UILabel *topLb;

@end
