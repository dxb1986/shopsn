//
//  ZShoppingCar.m
//  shopSN
//
//  Created by yisu on 16/7/1.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZShoppingCar.h"

@implementation ZShoppingCar

- (instancetype)init {
    
    self = [super init];
    if (self) {
        _shopGoodsArr = [NSMutableArray array];
        _shopCheck    = NO;
    }
    return self;
}


- (void)upDateAllShopGoodsCheck:(BOOL)_check {
    for (ZShopGoods *s_Goods in _shopGoodsArr) {
        s_Goods.goodsCheck = _check;
    }
    _shopCheck = _check;
}

- (void)upDateOneShopGoodsAtIndex:(NSInteger)index withCheck:(BOOL)_check {
    BOOL allCheck = YES;
    for (ZShopGoods *s_Goods in _shopGoodsArr) {
        if (!s_Goods.goodsCheck) {
            allCheck = NO;
        }
    }
    _shopCheck = allCheck;
}

- (void)upDateAllShopGoodsEditHidden:(BOOL)_hidden {
    _goodsDeleHidden = _hidden;
    for (ZShopGoods *s_Goods in _shopGoodsArr) {
        s_Goods.goodsEditHidden = _hidden;
    }
}

- (void)upDateAllShoppingCartEditHidden:(BOOL)_hidden {
    _goodsDeleHidden = _hidden;
    _shopEditButtonHidden = _hidden;
    for (ZShopGoods *s_Goods in _shopGoodsArr) {
        s_Goods.goodsEditHidden = _hidden;
    }
}





@end
