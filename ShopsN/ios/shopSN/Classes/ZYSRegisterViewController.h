//
//  ZYSRegisterViewController.h
//  shopSN
//
//  Created by imac on 2016/10/24.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseViewController.h"

@interface ZYSRegisterViewController : BaseViewController

//是否需要短信验证
@property (nonatomic) BOOL isSms;

@end
